//
//  RegisterViewController.swift
//  octopus-ios
//
//  Created by Maven on 18/04/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import iOSDropDown

class RegisterViewController: UIViewController, RestApiListener, UITextFieldDelegate {
    static let sharedInstance = RegisterViewController()
    
    @IBOutlet weak var txtFieldName: UITextField!
    @IBOutlet weak var txtFieldPhoneNumber: UITextField!
    @IBOutlet weak var txtFieldEmail: UITextField!
    @IBOutlet weak var provinceDropdown: DropDown!
    @IBOutlet weak var cityDropwosn: DropDown!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var buttonAccept: UIButton!
    
    @IBOutlet weak var labelEnterName: UILabel!
    @IBOutlet weak var labelEnterPhone: UILabel!
    @IBOutlet weak var labelEnterEmail: UILabel!
    @IBOutlet weak var labelSelectProvince: UILabel!
    @IBOutlet weak var labelSelectCity: UILabel!
    
    @IBOutlet weak var labelValidTermCondition: UILabel!
    
    //alert
    @IBOutlet weak var wrapperAlert: UIView!
    @IBOutlet weak var buttonOkAlert: UIButton!
    @IBOutlet weak var titleAlert: UILabel!
    @IBOutlet weak var descriptionAlert: UILabel!
    @IBOutlet weak var imageAlert: UIImageView!
    
    var arrProvince : [String] = []
    var province = ""
    
    var arrCity : [String] = []
    var city = ""
    var accept = false
    var phoneValid = false
    var nameValid = false
    var emailValid = false
    var cityValid = false
    var provinceValid = false
    var deviceToken = ""
    
    let authorizationSB: UIStoryboard = UIStoryboard(name: "Authorization", bundle: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        buttonOkAlert.layer.cornerRadius = 2
    
        Service.getProvince(listener: self)
        
        txtFieldName.addTarget(self, action: #selector(textFieldDidChangeName(textField:)), for: .editingChanged)
        txtFieldEmail.addTarget(self, action: #selector(textFieldDidChangeEmail(textField:)), for: .editingChanged)
        txtFieldPhoneNumber.addTarget(self, action: #selector(textFieldDidChangePhone(textField:)), for: .editingChanged)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        wrapperAlert.alpha = 0
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func textFieldDidChangeName(textField: UITextField){
        if textField.text!.count == 0 {
            labelEnterName.isHidden = false
            nameValid = false
        } else {
            labelEnterName.isHidden = true
            nameValid = true
        }
    }
    
    @objc func textFieldDidChangeEmail(textField: UITextField){
        if !textField.text!.isValidEmail {
            labelEnterEmail.isHidden = false
            emailValid = false
            labelEnterEmail.text = "Email Kamu Salah."
        } else if textField.text?.count == 0 {
            labelEnterEmail.isHidden = false
            emailValid = false
            labelEnterEmail.text = "Masukkan Email Kamu."
        } else {
            labelEnterEmail.isHidden = true
            emailValid = true
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var maxLength = 100         // set your need
        
        if textField.tag == 1 {
            maxLength = 14
        }
        
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        
        return newString.length <= maxLength
    }
    
    @objc func textFieldDidChangePhone(textField: UITextField){
        let phone = textField.text!
        
        if phone.count <= 10 || phone.isContainsLetters {
            labelEnterPhone.isHidden = false
            labelEnterPhone.text = "Nomor Telepon Kamu Salah."
            phoneValid = false
        } else if textField.text?.count == 0 {
            phoneValid = false
            labelEnterPhone.isHidden = false
            labelEnterEmail.text = "Masukkan Nomor Telepon Kamu."
        } else if phone.count >= 10 &&  phone[String.Index.init(utf16Offset: 0, in: phone)] != "0" {
            labelEnterPhone.text = "Nomor Telepon dimulai angka 0."
            labelEnterPhone.isHidden = false
            phoneValid = false
        } else {
            labelEnterPhone.isHidden = true
            phoneValid = true
        }
    }
    
    @IBAction func acceptTermsConditionsAction(_ sender: Any) {
        if accept {
            imageView.alpha = 0
            labelValidTermCondition.isHidden = false
            accept = false
        } else {
            imageView.alpha = 1
            labelValidTermCondition.isHidden = true
            accept = true
        }
    }
    
    @IBAction func goToTermsAndCondition(_ sender: Any) {
        self.performSegue(withIdentifier: "termsConditonsSegue", sender: self)
    }
    
    @IBAction func resignKeyboard(_ sender: Any) {
        closeKeyboard()
    }
    
    func closeKeyboard() {
        txtFieldName.resignFirstResponder()
        txtFieldPhoneNumber.resignFirstResponder()
        txtFieldEmail.resignFirstResponder()
        cityDropwosn.resignFirstResponder()
        provinceDropdown.resignFirstResponder()
    }
    
    func checkPhone(phone: String) {
        if phone.count < 10 || phone.isContainsLetters {
            labelEnterPhone.isHidden = false
            labelEnterPhone.text = "Nomor Telepon Kamu Salah."
            phoneValid = false
        } else if phone.count == 0 {
            phoneValid = false
            labelEnterPhone.isHidden = false
            labelEnterEmail.text = "Masukkan Nomor Telepon Kamu."
        } else if phone[String.Index.init(utf16Offset: 0, in: phone)] != "0" {
            labelEnterPhone.text = "Nomor Telepon dimulai angka 0."
            labelEnterPhone.isHidden = false
            phoneValid = false
        } else {
            labelEnterPhone.isHidden = true
            phoneValid = true
        }
    }
    
    @IBAction func registerAction(_ sender: Any) {
        closeKeyboard()
        
        checkPhone(phone: txtFieldPhoneNumber.text!)
        
        if txtFieldName.text == "" || txtFieldPhoneNumber.text == "" || txtFieldEmail.text == "" || provinceDropdown.text == "" || cityDropwosn.text == "" || !accept || !phoneValid || !emailValid {
            
            if txtFieldName.text == "" {
                labelEnterName.isHidden = false
            }
            
            if txtFieldPhoneNumber.text == "" {
                labelEnterPhone.isHidden = false
            }
            
            if txtFieldEmail.text == "" {
                labelEnterName.isHidden = false
            }
            
            if provinceDropdown.text == "" {
                labelSelectProvince.isHidden = false
            }
            
            if cityDropwosn.text == "" {
                labelSelectCity.isHidden = false
            }
            
            if !accept {
                labelValidTermCondition.isHidden = false
            }
            
            if !phoneValid {
                labelEnterPhone.isHidden = false
            }
            
            if !emailValid {
                labelEnterEmail.isHidden = false
            }
            
        } else {
            labelSelectCity.isHidden = true
            labelSelectProvince.isHidden = true
            labelEnterEmail.isHidden = true
            labelEnterPhone.isHidden = true
            labelEnterName.isHidden = true
            labelValidTermCondition.isHidden = true
            
            var request = URLRequest(url: URL(string: "http://dev.api.octopus.co.id/api/user/signup")!)
            request.httpMethod = HTTPMethod.post.rawValue
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")


            let parameters = "{\n\t\"email\":\"\(txtFieldEmail.text!)\",\n\t\"name\":\"\(txtFieldName.text!)\",\n\t\"phone\":\"\(txtFieldPhoneNumber.text!)\",\n\t\"user_type\":\"User\",\n\t\"devic_token\":\"\(deviceToken)\",\n\t\"address\":{\n\t\t\"province\":\"\(province)\",\n\t\t\"city\":\"\(city)\"\n\t}\n}"
            let postData = parameters.data(using: .utf8)

            request.httpBody = postData

            print(postData)

            Alamofire.request(request).responseJSON { (response) in

                switch response.result {
                case .success:
                    print("success register")
                    print(JSON(response.result.value!))

                    let json = JSON(response.result.value!)
                    if json["message"].stringValue == "email_exist" {
                        self.wrapperAlert.alpha = 1
                        self.imageAlert.image = UIImage(named: "alert_email_exist")
                        self.titleAlert.text = "Yah emailmu sudah terdaftar"
                        self.descriptionAlert.text = "Emailmu sudah terdaftar, coba lagi dengan email yang lain ya.."
                    } else if json["message"].stringValue == "phone_exist" {
                        self.wrapperAlert.alpha = 1
                        self.imageAlert.image = UIImage(named: "alert_phone_exist")
                        self.titleAlert.text = "Yah nomormu sudah terdaftar"
                        self.descriptionAlert.text = "Nomor HPmu sudah terdaftar, coba lagi dengan nomor yang lain ya.."
                    } else if json["message"].stringValue == "signup_success" {
                        self.login(phone: self.self.txtFieldPhoneNumber.text!)
                        self.wrapperAlert.alpha = 0
                    }

                    break
                case .failure(let error):

                    print(error)
                }

            }
        }
    }
    
    func login(phone: String) {
        let param: Parameters = [
            "phone": phone
        ]
        
        let jsonParam = JSON(param)
        
        let representation = jsonParam.rawString([.castNilToNSNull: true])
        print(representation!)
        let JsonString = representation! //  Ex.:-  "{\"action\": \"fetch-data\"}"
        var request = URLRequest(url: URL(string: "http://dev.api.octopus.co.id/api/user/signin")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")

        let pjson = JsonString
        let data = (pjson.data(using: .utf8))! as Data

        request.httpBody = data
        
        print(data)

        Alamofire.request(request).responseJSON { (response) in

            switch response.result {
            case .success:
                print("success login")
                print(response)
                let OtpVC = self.authorizationSB.instantiateViewController(withIdentifier: "OtpVC") as! OtpViewController
                OtpVC.phoneNumber = self.txtFieldPhoneNumber.text!
                OtpVC.source = "register"
                self.navigationController?.pushViewController(OtpVC, animated: true)
                
                
                break
            case .failure(let error):

                print(error)
            }

        }
        
    }
    
    @IBAction func closeAlertAction(_ sender: Any) {
        self.wrapperAlert.alpha = 0
    }
    
    static func stringify(json: Any, prettyPrinted: Bool = false) -> String {
        var options: JSONSerialization.WritingOptions = []
        if prettyPrinted {
          options = JSONSerialization.WritingOptions.prettyPrinted
        }

        do {
          let data = try JSONSerialization.data(withJSONObject: json, options: options)
          if let string = String(data: data, encoding: String.Encoding.utf8) {
            return string
          }
        } catch {
          print(error)
        }

        return ""
    }
    
    func successResponse(call: RequestCall, response: DataResponse<Any>) {
        print("successResponse")
        if (call == .getProvince) {
            let res = response.result.value
            if (res != nil) {
                let json = JSON(res!)
                arrProvince = []
                for (index, _) in json.enumerated() {
                    let name = json[index]["name"].stringValue
                    arrProvince.append(name)
                }
                DispatchQueue.main.async {
                    self.provinceDropdown.optionArray = self.arrProvince
                    self.provinceDropdown.isSearchEnable = false
//                    self.province = self.arrProvince[0]
//                    self.provinceDropdown.selectedIndex = 0
//                    self.provinceDropdown.text = self.arrProvince[0]
                    self.provinceDropdown.didSelect { (selectedText, index, id) in
                        self.province = selectedText
                        self.getFilteredCity(selectedProvince: self.province)
                    }
                }
            }
        }
        
        if (call == .getCity) {
            let res = response.result.value
            if (res != nil) {
                let json = JSON(res!)
                arrCity = []
                for (index, _) in json.enumerated() {
                    let name = json[index]["name"].stringValue
                    arrCity.append(name)
                }
                DispatchQueue.main.async {
                    self.cityDropwosn.optionArray = self.arrCity
                    self.cityDropwosn.isSearchEnable = false
                    self.cityDropwosn.placeholder = "Pilih Kota"
                    self.cityDropwosn.didSelect { (selectedText, index, id) in
                        self.city = selectedText
                    }
                }
            }
        }
        
        if (call == .registerReguest) {
            print("success register")
            let OtpVC = self.authorizationSB.instantiateViewController(withIdentifier: "OtpVC") as! OtpViewController
            OtpVC.phoneNumber = self.txtFieldPhoneNumber.text!
            self.navigationController?.pushViewController(OtpVC, animated: true)
        }
    }
    
    func getFilteredCity(selectedProvince: String){
        Service.getCity(name: selectedProvince, listener: self)
    }
    
    func failureResponse(call: RequestCall, response: DataResponse<Any>) {
        print("failureResponse")
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension String {
    var isValidEmail: Bool {
        NSPredicate(format: "SELF MATCHES %@", "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}").evaluate(with: self)
    }
}
