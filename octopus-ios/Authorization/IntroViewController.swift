//
//  IntroViewController.swift
//  octopus-ios
//
//  Created by Maven on 24/04/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class IntroViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, RestApiListener {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var introList : [IntroModel] = []
    var token = UserDefaults.standard.string(forKey: "token")
    var phoneNumber : String?
    var window: UIWindow?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        var introModel : IntroModel
        
        introList = []
        
        introModel = IntroModel()
        introModel.icon = "intro1"
        introModel.title = "CALL SCAVENGER"
        introModel.subTitle = "Untuk mulai menjual sampah, klik tombol Call Scavenger"
        introList.append(introModel)
        
        introModel = IntroModel()
        introModel.icon = "intro2"
        introModel.title = "MASUKKAN JUMLAH SAMPAH"
        introModel.subTitle = "Masukkan jenis dan jumlah sampah yang akan kamu jual"
        introList.append(introModel)
        
        introModel = IntroModel()
        introModel.icon = "intro3"
        introModel.title = "PILIH LOKASI JEMPUT"
        introModel.subTitle = "Masukkan lokasi yang menjadi titik jemput sampah"
        introList.append(introModel)
        
        introModel = IntroModel()
        introModel.icon = "intro4"
        introModel.title = "TUNGGU SCAVENGER KAMU"
        introModel.subTitle = "Silahkan tunggu hingga Octopus menemukan Scavenger untukmu"
        introList.append(introModel)
        // Do any additional setup after loading the view.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return introList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : UICollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        
        let introModel : IntroModel = introList[indexPath.row]
        let imageView : UIImageView = cell.viewWithTag(1) as! UIImageView
        let labelTitle : UILabel = cell.viewWithTag(2) as! UILabel
        let labelSubTitle : UILabel = cell.viewWithTag(3) as! UILabel
        let buttonNext : UIButton = cell.viewWithTag(4) as! UIButton
        let buttonSkip : UIButton = cell.viewWithTag(5) as! UIButton
        
        buttonNext.layer.cornerRadius = 23
        
        labelTitle.text = introModel.title
        labelSubTitle.text = introModel.subTitle
        imageView.image = UIImage.init(named: introModel.icon)
        buttonSkip.setTitle("Lewati", for: .normal)
        
        if indexPath.row == 3 {
            buttonNext.setTitle("OK, MENGERTI", for: .normal)
            buttonSkip.alpha = 0.0
            buttonNext.addTarget(self, action: #selector(buttonSkipAction), for: .touchUpInside )
        } else if indexPath.row == 1 || indexPath.row == 0 || indexPath.row == 2 {
            buttonNext.setTitle("SELANJUTNYA", for: .normal)
            buttonSkip.alpha = 1.0
            buttonNext.addTarget(self, action: #selector(buttonNextAction), for: .touchUpInside)
        }
        
        buttonSkip.addTarget(self, action: #selector(buttonSkipAction), for: .touchUpInside )
        
        return cell
    }
    
    var index : Int = 0
    
    @objc func buttonNextAction() {
        if index != 3 {
            index = index + 1
            self.collectionView.scrollToItem(at:IndexPath(item: index, section: 0), at: .right, animated: true)
        }
    }
    
    @objc func buttonSkipAction() {
        Service.checkPin(token: token!, listener: self)
    }
    
    func successResponse(call: RequestCall, response: DataResponse<Any>) {
        if (call == .checkPin) {
            
            let mainSB: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let homeTab = mainSB.instantiateViewController(withIdentifier: "homeNav") as! UINavigationController
            UIApplication.shared.keyWindow?.rootViewController = homeTab
        }
    }
    
    func failureResponse(call: RequestCall, response: DataResponse<Any>) {
        if (call == .checkPin) {
            let authSB: UIStoryboard = UIStoryboard(name: "Authorization", bundle: nil)
            let pinVC = authSB.instantiateViewController(withIdentifier: "pinVC") as! PinViewController
            pinVC.phoneNumber = phoneNumber
            self.navigationController?.pushViewController(pinVC, animated: true)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        for cell in collectionView.visibleCells {
            let indexPath = collectionView.indexPath(for: cell)
            index = indexPath!.row
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellWidth = collectionView.frame.size.width
        let cellHeight = collectionView.frame.size.height
        let size : CGSize = CGSize(width: cellWidth, height: cellHeight)
        
        return size;
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
