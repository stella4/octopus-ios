//
//  LoginViewController.swift
//  octopus-ios
//
//  Created by Maven on 17/04/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class LoginViewController: UIViewController, RestApiListener, UITextFieldDelegate {
    static let sharedInstance = LoginViewController()
    
    @IBOutlet weak var txtFieldPhoneNumber: UITextField!
    @IBOutlet weak var logInButton: UIButton!
    @IBOutlet weak var labelValid: UILabel!
    @IBOutlet weak var wrapperAlert: UIView!
    @IBOutlet weak var buttonAlert: UIButton!
    
    var deviceToken : String = ""
    
    let authorizationSB: UIStoryboard = UIStoryboard(name: "Authorization", bundle: nil)
    
    var phoneValid = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        buttonAlert.layer.cornerRadius = 2
        
        txtFieldPhoneNumber.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)

        // Do any additional setup after loading the view.
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 14         // set your need
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    @objc func textFieldDidChange(textField: UITextField){
        let phone = textField.text!
        
        if phone.count < 10 || phone.isContainsLetters {
            labelValid.alpha = 1
            labelValid.text = "Nomor Telepon Kamu Salah."
            phoneValid = 1
        } else {
            labelValid.alpha = 0
            phoneValid = 0
        }
        
        if phone.count >= 10 &&  phone[String.Index.init(utf16Offset: 0, in: phone)] != "0" {
            labelValid.text = "Nomor Telepon dimulai angka 0."
            labelValid.alpha = 1
            phoneValid = 1
        } else {
           phoneValid = 0
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        wrapperAlert.alpha = 0
    }
    
    @IBAction func btnLogInAction(_ sender: Any) {
        txtFieldPhoneNumber.resignFirstResponder()
        
        if txtFieldPhoneNumber.text!.isEmpty {
            
        } else if phoneValid == 0 {
            let param: Parameters = [
                "phone": txtFieldPhoneNumber.text!
            ]
            
            let jsonParam = JSON(param)
            
            let representation = jsonParam.rawString([.castNilToNSNull: true])
            let JsonString = representation! //  Ex.:-  "{\"action\": \"fetch-data\"}"
            var request = URLRequest(url: URL(string: "http://dev.api.octopus.co.id/api/user/signin")!)
            request.httpMethod = HTTPMethod.post.rawValue
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")

            let pjson = JsonString
            let data = (pjson.data(using: .utf8))! as Data

            request.httpBody = data

            Alamofire.request(request).responseJSON { (response) in

                switch response.result {
                case .success:
                    print(response)
                    //callScavangerVC
                    let json = JSON(response.result.value!)
                    if json["code"].intValue == 3 {
                        self.wrapperAlert.alpha = 1
                    } else {
                        let OtpVC = self.authorizationSB.instantiateViewController(withIdentifier: "OtpVC") as! OtpViewController
                        OtpVC.phoneNumber = self.txtFieldPhoneNumber.text!
                        OtpVC.source = "login"
                        self.navigationController?.pushViewController(OtpVC, animated: true)
                    }
                    
                    break
                case .failure(let error):

                    print(error)
                }

            }
        }
    }
    
    @IBAction func closeAlertAction(_ sender: Any) {
        wrapperAlert.alpha = 0
    }
    
    @IBAction func resignKeyboard(_ sender: Any) {
        txtFieldPhoneNumber.resignFirstResponder()
    }
    
    func successResponse(call: RequestCall, response: DataResponse<Any>) {
        let OtpVC = authorizationSB.instantiateViewController(withIdentifier: "OtpVC") as! OtpViewController
        OtpVC.phoneNumber = txtFieldPhoneNumber.text!
        self.navigationController?.pushViewController(OtpVC, animated: true)

    }
    
    func failureResponse(call: RequestCall, response: DataResponse<Any>) {
        print("failureResponse")
        buildAlert(title: "", message: "", completion: {
            self.logInButton.isEnabled = true
        }, handler: nil)
    }
    
    @IBAction func btnSignUpAction(_ sender: Any) {
        let registerVC = authorizationSB.instantiateViewController(withIdentifier: "registerVC") as! RegisterViewController
        self.navigationController?.pushViewController(registerVC, animated: true)
    }
    
    func buildAlert(title: String, message: String, completion: (() -> Void)?, handler: ((UIAlertAction) -> Void)?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: handler)
        alert.addAction(ok)
        self.present(alert, animated: true, completion: completion)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension String{
    var isContainsLetters : Bool{
        let letters = CharacterSet.letters
        return self.rangeOfCharacter(from: letters) != nil
    }
}

extension UITextField{
   @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
        }
    }
}

extension UIViewController {
    func showToast(message : String) {

        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 125, y: self.view.frame.size.height - 100, width: 250, height: 35))
        toastLabel.backgroundColor = UIColor.init(hexString: "#F5F5F5")
        toastLabel.textColor = UIColor.black
        toastLabel.textAlignment = .center;
        toastLabel.adjustsFontSizeToFitWidth = true
//        toastLabel.font = UIFont(name: "poppins_regular.ttf", size: 10.0)
        toastLabel.text = message
        toastLabel.numberOfLines = 1
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 8;
        toastLabel.clipsToBounds = true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 3.0, delay: 0.1, options: .curveEaseIn, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
}
