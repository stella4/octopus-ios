//
//  PinViewController.swift
//  octopus-ios
//
//  Created by Maven on 30/04/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import UIKit
import Alamofire
import  SwiftyJSON

class PinViewController: UIViewController, UITextFieldDelegate, RestApiListener {

    @IBOutlet weak var txtCode1: UITextField!
    @IBOutlet weak var txtCode2: UITextField!
    @IBOutlet weak var txtCode3: UITextField!
    @IBOutlet weak var txtCode4: UITextField!
    @IBOutlet weak var txtCode5: UITextField!
    @IBOutlet weak var txtCode6: UITextField!
    
    @IBOutlet weak var wrapperView: UIView!
    @IBOutlet weak var view1: UIView!
    
    var token = UserDefaults.standard.string(forKey: "token")
    var phoneNumber : String?
    var pin : String?
    var unwind = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        wrapperView.alpha = 0
        view1.layer.cornerRadius = 4
        
        txtCode1.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        txtCode2.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        txtCode3.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        txtCode4.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        txtCode5.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        txtCode6.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        // Do any additional setup after loading the view.
    }
    
    @objc func textFieldDidChange(textField: UITextField){
        let text = textField.text
        if text?.count == 1 {
            switch textField{
            case txtCode1:
                txtCode2.becomeFirstResponder()
            case txtCode2:
                txtCode3.becomeFirstResponder()
            case txtCode3:
                txtCode4.becomeFirstResponder()
            case txtCode4:
                txtCode5.becomeFirstResponder()
            case txtCode5:
                txtCode6.becomeFirstResponder()
            case txtCode6:
                txtCode6.resignFirstResponder()
                wrapperView.alpha = 1
            default:
                break
            }
        }
        if text?.count == 0 {
            switch textField{
            case txtCode1:
                txtCode1.becomeFirstResponder()
            case txtCode2:
                txtCode1.becomeFirstResponder()
            case txtCode3:
                txtCode2.becomeFirstResponder()
            case txtCode4:
                txtCode3.becomeFirstResponder()
            case txtCode5:
                txtCode4.becomeFirstResponder()
            case txtCode6:
                txtCode5.becomeFirstResponder()
            default:
                break
            }
        }
    }
    
    @IBAction func yesAction(_ sender: Any) {
        let code1 = txtCode1.text!
        let code2 = txtCode2.text!
        let code3 = txtCode3.text!
        let code4 = txtCode4.text!
        let code5 = txtCode5.text!
        let code6 = txtCode6.text!
        let code = code1+code2+code3+code4+code5+code6
        pin = code
        
        Service.sendPin(token: token!, pin: code, listener: self)
    }
    
    func successResponse(call: RequestCall, response: DataResponse<Any>) {
        if (call == .sendPin) {
            let authorizationSB: UIStoryboard = UIStoryboard(name: "Authorization", bundle: nil)
            let OtpVC = authorizationSB.instantiateViewController(withIdentifier: "OtpVC") as! OtpViewController
            OtpVC.phoneNumber = phoneNumber
            OtpVC.code = pin
            OtpVC.source = "pin"
            OtpVC.unwind = unwind
            
            self.navigationController?.pushViewController(OtpVC, animated: true)
        }
    }
    
    func failureResponse(call: RequestCall, response: DataResponse<Any>) {
        
    }
    
    @IBAction func noAction(_ sender: Any) {
        wrapperView.alpha = 0
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
