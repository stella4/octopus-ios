//
//  TermsConditionViewController.swift
//  octopus-ios
//
//  Created by Maven on 24/04/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import WebKit

class TermsConditionViewController: UIViewController, RestApiListener, WKNavigationDelegate {
    func successResponse(call: RequestCall, response: DataResponse<Any>) {
        let json = JSON(response.result.value!)
        let description = (json["description"].stringValue).replacingOccurrences(of: "\n", with: "<br/>")
        
        labelTitle.text = json["title"].stringValue
        
        self.webView.navigationDelegate = self
        let htmlString = """
        <style>
        @font-face
        {
            font-family: 'poppins_regular';
            font-weight: normal;
            src: url(poppins_regular.ttf);
        }
        </style>
        <span style="font-family: 'poppins_regular'; font-weight: normal; font-size: 42; text-align: left; color: #000000">\(description)</span>
        """
        self.webView.loadHTMLString(htmlString, baseURL: Bundle.main.bundleURL)
        self.webView.backgroundColor = UIColor.clear
        self.heightConstantWkWebView.constant = 40
        self.webView.scrollView.isScrollEnabled = false;
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        webView.scrollView.isScrollEnabled = false;
        heightConstantWkWebView.constant = webView.scrollView.contentSize.height
        self.webView.frame.size.height = webView.scrollView.contentSize.height
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.heightConstantWkWebView.constant = webView.scrollView.contentSize.height
        }
    }
    
    func failureResponse(call: RequestCall, response: DataResponse<Any>) {
        
    }
    

    @IBOutlet weak var heightConstantWkWebView: NSLayoutConstraint!
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var labelTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        Service.termsConditions(listener: self)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
