//
//  IntroModel.swift
//  octopus-ios
//
//  Created by Maven on 24/04/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import Foundation

class IntroModel: Decodable {
    var icon : String = ""
    var title : String = ""
    var subTitle : String = ""
}
