//
//  OtpViewController.swift
//  octopus-ios
//
//  Created by Maven on 17/04/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class OtpViewController: UIViewController, UITextFieldDelegate, RestApiListener {
    static let sharedInstance = OtpViewController()
    
    @IBOutlet weak var txtCode1: UITextField!
    @IBOutlet weak var txtCode2: UITextField!
    @IBOutlet weak var txtCode3: UITextField!
    @IBOutlet weak var txtCode4: UITextField!
    @IBOutlet weak var lblResendTime: UILabel!
    @IBOutlet weak var buttonResendOtp: UIButton!
    
    var seconds = 0
    var code: String?
    var timer = Timer()
    var isTimerRunning = false
    var phoneNumber : String?
    let mainSB: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    var source: String?
    var deviceToken : String = ""
    var unwind = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        txtCode1.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        txtCode2.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        txtCode3.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        txtCode4.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        
        txtBorder(textfield: txtCode1)
        txtBorder(textfield: txtCode2)
        txtBorder(textfield: txtCode3)
        txtBorder(textfield: txtCode4)
        
        runTimer()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    func runTimer() {
        seconds = 120
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(updateTimer)),
                                     userInfo: nil, repeats: true)
        isTimerRunning = true
    }
    
    @objc func updateTimer(){
        if seconds < 1 {
            timer.invalidate()
            buttonResendOtp.alpha = 1
            //Send alert to indicate time's up.
        } else {
            seconds -= 1
            lblResendTime.text = (timeString(time: TimeInterval(seconds)))
            buttonResendOtp.alpha = 0
        }
    }
    
    func timeString(time:TimeInterval) -> String {
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i",minutes,seconds)
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func resendOtpAction(_ sender: Any) {
        Service.resendOtp(phone: phoneNumber!, listener: self)
    }
    
    @IBAction func verificationOtpAction(_ sender: Any) {
        txtCode1.resignFirstResponder()
        txtCode2.resignFirstResponder()
        txtCode3.resignFirstResponder()
        txtCode4.resignFirstResponder()
        
        if txtCode1.text! == "" || txtCode2.text! == "" || txtCode3.text! == "" || txtCode4.text! == "" {
            
        } else {
            let code1 = txtCode1.text!
            let code2 = txtCode2.text!
            let code3 = txtCode3.text!
            let code4 = txtCode4.text!
            let otp = code1+code2+code3+code4
            if source == "pin" {
                let token = UserDefaults.standard.string(forKey: "token")
                Service.pinReguest(token: token!, pin: code!, otp: otp, listener: self)
            } else {
                Service.verficationOtp(phone: phoneNumber!, otp: otp, listener: self)
            }
        }
    }
    
    func successResponse(call: RequestCall, response: DataResponse<Any>) {
        if (call == .verificationOtp) {
            let json = JSON(response.result.value!)
            UserDefaults.standard.set(json["token"].stringValue, forKey: "token")
            
            let authorizationSB: UIStoryboard = UIStoryboard(name: "Authorization", bundle: nil)
            let introVC = authorizationSB.instantiateViewController(withIdentifier: "introVC") as! IntroViewController
            introVC.phoneNumber = phoneNumber
            self.navigationController?.pushViewController(introVC, animated: true)
        }
        
        if (call == .pinRequest) {
            if unwind {
                unwind = false
                self.performSegue(withIdentifier: "unwindToHomeSegue", sender: self)
            }
            source = ""
            let mainSB: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let homeTab = mainSB.instantiateViewController(withIdentifier: "homeNav") as! UINavigationController
            UIApplication.shared.keyWindow?.rootViewController = homeTab
        }
        
    }
    
    func failureResponse(call: RequestCall, response: DataResponse<Any>) {
        print("failureResponse")
        self.showToast(message: "Invalid OTP")
    }
    
    func txtBorder(textfield: UITextField) {
        let myColor = UIColor.white
        textfield.layer.borderColor = myColor.cgColor
        textfield.layer.borderWidth = 1.0
        textfield.layer.cornerRadius = 3.0
    }
    
    @objc func textFieldDidChange(textField: UITextField){
        let text = textField.text
        if  text?.count == 1 {
            switch textField{
            case txtCode1:
                txtCode2.becomeFirstResponder()
            case txtCode2:
                txtCode3.becomeFirstResponder()
            case txtCode3:
                txtCode4.becomeFirstResponder()
            case txtCode4:
                txtCode4.resignFirstResponder()
            default:
                break
            }
        }
        if  text?.count == 0 {
            switch textField{
            case txtCode1:
                txtCode1.becomeFirstResponder()
            case txtCode2:
                txtCode1.becomeFirstResponder()
            case txtCode3:
                txtCode2.becomeFirstResponder()
            case txtCode4:
                txtCode3.becomeFirstResponder()
            default:
                break
            }
        }
        else{
            
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
