//
//  Service.swift
//  octopus-ios
//
//  Created by Maven on 16/04/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

let baseURL = "http://dev.api.octopus.co.id/api/"
//let baseURL = "http://prod.api.octopus.co.id/api/"

protocol RestApiListener {
    func successResponse(call: RequestCall, response: DataResponse<Any>)
    func failureResponse(call: RequestCall, response: DataResponse<Any>)
}

enum RequestCall: String {

    // account & authorization
    case loginUser = "loginUser"
    case resendOtp = "resendOtp"
    case verificationOtp = "verificationOtp"
    case getProvince = "getProvince"
    case getCity = "getCity"
    case registerReguest = "registerReguest"
    case termsConditions = "termsConditions"
    case profile = "profile"
    case news = "news"
    case vouchers = "vouchers"
    case pickPack = "pickpack"
    case addToCart = "addToCart"
    case callScavanger = "callScavanger"
    case activeVoucher = "activeVoucher"
    case pastVoucher = "pastVoucher"
    case detailVoucher = "detailVoucher"
    case pinRequest = "pinRequest"
    case checkPin = "checkPin"
    case checkUserPin = "checkUserPin"
    case redeemVoucher = "redeemVoucher"
    case onGoingOrder = "onGoingOrder"
    case historyOrder = "historyOrder"
    case orderDetail = "orderDetail"
    case postChat = "postChat"
    case getChat = "getChat"
    case accountType = "accountType"
    case transactionHistory = "transactionHistory"
    case useCoupon = "useCoupon"
    case verificationId = "verificationId"
    case submitVerification = "submitVerification"
    case requestPayout = "requestPayout"
    case getPayoutSummary = "getPayoutSummary"
    case sendPin = "sendPin"
    case cancelOrder = "cancelOrder"
    case logout = "logout"
    case updateProfile = "updateProfile"
    case uploadImage = "uploadImage"
    case forgotPin = "forgotPin"
}

class Service {

    var deviceToken = ""
    
    /// show network activity indicator
    static func showNetworkActivityIndicator() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }

    /// hide network activity indicator
    static func hideNetworkActivityIndicator() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }

    /// base alamofire func
    static func callAlamofire(endpoint: String, method: HTTPMethod, header: HTTPHeaders?, param: Parameters?, call: RequestCall, listener: RestApiListener) {
        showNetworkActivityIndicator()
//        print("header - ",header!.values)
        let url = baseURL + endpoint
        let urlwithPercentEscapes = url.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        print(urlwithPercentEscapes!)
        sessionManager.request(urlwithPercentEscapes!, method: method, parameters: param, encoding: JSONEncoding.default, headers: header).validate().responseJSON { (response) in
//            print("response - ",response.result.value!)
            switch response.result {
            case .success:
                print("response success - ",response.result.value!)
                listener.successResponse(call: call, response: response)
                hideNetworkActivityIndicator()
            case .failure:
                print("failure - ",response.debugDescription)
                listener.failureResponse(call: call, response: response)
                hideNetworkActivityIndicator()
            }
        }
    }
    
    static func loginRequest(phone: String, listener: RestApiListener) {
        let param: Parameters = [
            "phone": phone
        ]
        print("param - ",param)
        callAlamofire(endpoint: "user/signin", method: .post, header: nil, param: param, call: RequestCall.loginUser, listener: listener)
    }
    
    static func resendOtp(phone: String, listener: RestApiListener) {
        let param: Parameters = [
            "phone": phone
        ]
        print("param - ",param)
        callAlamofire(endpoint: "user/re-send-otp", method: .post, header: nil, param: param, call: RequestCall.resendOtp, listener: listener)
    }
    
    static func verficationOtp(phone: String, otp: String, listener: RestApiListener) {
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        let bundleVersion = Bundle.main.infoDictionary?["CFBundleVersion"] as? String
        let systemVersion = UIDevice.current.systemVersion
        let deviceToken = UserDefaults.standard.string(forKey: "dtoken")!
        
        let param: Parameters = [
            "phone": phone,
            "otp": otp,
            "platform_type": "ios",
            "platform_version": systemVersion,
            "build_number": appVersion!+"."+bundleVersion!,
            "device_token": deviceToken
        ]
        print("param - ",param)
        callAlamofire(endpoint: "user/v2/otp-verification", method: .post, header: nil, param: param, call: RequestCall.verificationOtp, listener: listener)
    }
    
    static func getProvince(listener: RestApiListener) {
        callAlamofire(endpoint: "user/get-province", method: .get, header: nil, param: nil, call: RequestCall.getProvince, listener: listener)
    }
    
    static func getCity(name: String, listener: RestApiListener) {
        callAlamofire(endpoint: "user/get-city", method: .post, header: nil, param: ["name":name], call: RequestCall.getCity, listener: listener)
    }
    
    static func termsConditions(listener: RestApiListener) {
        callAlamofire(endpoint: "static-content/get-terms-and-conditions", method: .get, header: nil, param: nil, call: RequestCall.termsConditions, listener: listener)
    }

    static func registerRequest(email: String, name: String, phone: String, user_type: String, devic_token: String, province: String, city: String, listener: RestApiListener) {
        let paramsArray : [String:Any] = [
            "province": province,
            "city": city
        ]
        
        let json = JSON(paramsArray)
        let rawValue = stringify(json: paramsArray)
        print(rawValue)
        
        let param: Parameters = [
            "email": email,
            "name": name,
            "phone": phone,
            "user_type": user_type,
            "devic_token": devic_token,
            "address": rawValue
        ]
        print("param - ",param)
        callAlamofire(endpoint: "user/signup", method: .post, header: nil, param: param, call: RequestCall.registerReguest, listener: listener)
    }
    
    static func stringify(json: Any, prettyPrinted: Bool = false) -> String {
        var options: JSONSerialization.WritingOptions = []
        if prettyPrinted {
          options = JSONSerialization.WritingOptions.prettyPrinted
        }

        do {
          let data = try JSONSerialization.data(withJSONObject: json, options: options)
          if let string = String(data: data, encoding: String.Encoding.utf8) {
            return string
          }
        } catch {
          print(error)
        }

        return ""
    }
    
    static func getProfile(token: String, listener: RestApiListener) {
        callAlamofire(endpoint: "user/profile", method: .get, header: ["Authorization": "JWT "+token], param: nil, call: RequestCall.profile, listener: listener)
    }
    
    static func logout(token: String, listener: RestApiListener) {
        callAlamofire(endpoint: "user/logout", method: .get, header: ["Authorization": "JWT "+token], param: nil, call: RequestCall.logout, listener: listener)
    }
    
    static func getNews(token: String, listener: RestApiListener) {
        callAlamofire(endpoint: "user/get-home-tiles", method: .get, header: ["Authorization": "JWT "+token], param: nil, call: RequestCall.news, listener: listener)
    }
    
    static func getVouchers(token: String, listener: RestApiListener) {
        callAlamofire(endpoint: "user/get-voucher-list", method: .get, header: ["Authorization": "JWT "+token], param: nil, call: RequestCall.vouchers, listener: listener)
    }
    
    static func getPickPack(token: String, listener: RestApiListener) {
        callAlamofire(endpoint: "user/pick-pack", method: .get, header: ["Authorization": "JWT "+token], param: nil, call: RequestCall.pickPack, listener: listener)
    }
    
    static func addToCart(token: String, dataCart:[CartModel], listener: RestApiListener) {
        var items = [[String: Any]]()
        for ol in dataCart {
            items.append([
                "id": ol.id,
                "qty": ol.qty
                ])
        }
        
        let param : [String:Any] = ["item":items]
        let json = JSON(param)
        
        let representation = json.rawString([.castNilToNSNull: true])
        print(representation!)

        let pjson = representation!
        let data = (pjson.data(using: .utf8))! as Data
        callAlamofire(endpoint: "user/add-to-cart/", method: .post, header: ["Authorization": "JWT "+token], param: param, call: RequestCall.addToCart, listener: listener)
    }
    
    static func callScavanger(token: String, adress: String, optional_address: String, listener: RestApiListener) {
        print(token,optional_address,adress)
        let param: Parameters = [
            "address": adress,
            "optional_address": optional_address
        ]
        print("param order - ",param)
        callAlamofire(endpoint: "user/place-order", method: .post, header: ["Authorization": "JWT "+token], param: param, call: RequestCall.callScavanger, listener: listener)
    }
    
    static func getOnGoingOrder(token: String, listener: RestApiListener) {
        callAlamofire(endpoint: "user/get-ongoing-order", method: .get, header: ["Authorization": "JWT "+token], param: nil, call: RequestCall.onGoingOrder, listener: listener)
    }
    
    static func getHistoryOrder(token: String, listener: RestApiListener) {
        callAlamofire(endpoint: "user/order-history", method: .get, header: ["Authorization": "JWT "+token], param: nil, call: RequestCall.historyOrder, listener: listener)
    }
    
    static func getParticularOnGoingOrder(token: String, id: String, listener: RestApiListener) {
        callAlamofire(endpoint: "user/get-particular-ongoing-order/\(id)", method: .get, header: ["Authorization": "JWT "+token], param: nil, call: RequestCall.orderDetail, listener: listener)
    }
    
    static func cancelOrder(token: String, id: String, listener: RestApiListener) {
        callAlamofire(endpoint: "user/cancel-order/\(id)", method: .get, header: ["Authorization": "JWT "+token], param: nil, call: RequestCall.cancelOrder, listener: listener)
    }
    
    static func postChat(token: String, message: String, sender_id: String, order_id: String, receiver_id: String, listener: RestApiListener) {
        let param: Parameters = [
            "message": message,
            "sender_id": sender_id,
            "order_id": order_id,
            "receiver_id": receiver_id
        ]
        
        callAlamofire(endpoint: "user/chat", method: .post, header: ["Authorization": "JWT "+token], param: param, call: RequestCall.postChat, listener: listener)
    }
    
    static func getChat(token: String, order_id: String, listener: RestApiListener) {
        callAlamofire(endpoint: "user/chat-list/\(order_id)", method: .get, header: ["Authorization": "JWT "+token], param: nil, call: RequestCall.getChat, listener: listener)
    }
    
    static func getActiveVouchers(token: String, listener: RestApiListener) {
        callAlamofire(endpoint: "user/get-user-voucher-list-active", method: .get, header: ["Authorization": "JWT "+token], param: nil, call: RequestCall.activeVoucher, listener: listener)
    }
    
    static func getPastVouchers(token: String, listener: RestApiListener) {
        callAlamofire(endpoint: "user/get-user-voucher-list-past", method: .get, header: ["Authorization": "JWT "+token], param: nil, call: RequestCall.pastVoucher, listener: listener)
    }
    
    static func getDetailVouchers(token: String, id: Int, listener: RestApiListener) {
        callAlamofire(endpoint: "user/detail-voucher/\(id)", method: .post, header: ["Authorization": "JWT "+token], param: nil, call: RequestCall.detailVoucher, listener: listener)
    }
    
    static func redeemVouchers(token: String, id: Int, listener: RestApiListener) {
        let param: Parameters = [
            "voucher_id": id
        ]
        
        callAlamofire(endpoint: "user/redeem-voucher", method: .post, header: ["Authorization": "JWT "+token], param: param, call: .redeemVoucher, listener: listener)
    }
    
    static func pinReguest(token: String, pin: String, otp: String, listener: RestApiListener) {
        let param: Parameters = [
            "pin": pin,
            "otp": otp
        ]
        callAlamofire(endpoint: "user/change-user-pin", method: .post, header: ["Authorization": "JWT "+token], param: param, call: RequestCall.pinRequest, listener: listener)
    }
    
    static func forgotPin(token: String, pin: String, otp: String, listener: RestApiListener) {
        let param: Parameters = [
            "pin": pin,
            "otp": otp
        ]
        callAlamofire(endpoint: "user/change-user-pin", method: .post, header: ["Authorization": "JWT "+token], param: param, call: RequestCall.pinRequest, listener: listener)
    }
    
    static func sendPin(token: String, pin: String, listener: RestApiListener) {
        let param: Parameters = [
            "pin": pin
        ]
        callAlamofire(endpoint: "user/user-pin", method: .post, header: ["Authorization": "JWT "+token], param: param, call: RequestCall.sendPin, listener: listener)
    }
    
    static func checkPin(token: String, listener: RestApiListener) {
        callAlamofire(endpoint: "user/user-pin", method: .get, header: ["Authorization": "JWT "+token], param: nil, call: RequestCall.checkPin, listener: listener)
    }
    
    static func checkUserPin(token: String, pin: String, listener: RestApiListener) {
        let param: Parameters = [
            "pin": pin
        ]
        callAlamofire(endpoint: "user/check-user-pin", method: .post, header: ["Authorization": "JWT "+token], param: param, call: RequestCall.checkUserPin, listener: listener)
    }
    
    static func chackAccountType(token: String, listener: RestApiListener) {

        callAlamofire(endpoint: "user/account_type", method: .get, header: ["Authorization": "JWT "+token], param: nil, call: RequestCall.accountType, listener: listener)
    }
    
    static func getTransactionHistory(token: String, listener: RestApiListener) {

        callAlamofire(endpoint: "user/get-transaction-history", method: .get, header: ["Authorization": "JWT "+token], param: nil, call: RequestCall.transactionHistory, listener: listener)
    }
    
    static func useCoupon(token: String, id: Int, listener: RestApiListener) {
        let param: Parameters = [
            "voucher_code_id": id
        ]
        
        callAlamofire(endpoint: "user/use-voucher", method: .post, header: ["Authorization": "JWT "+token], param: param, call: .useCoupon, listener: listener)
    }
    
    static func getPayoutSummary(token: String, dtbm_points: String, bank_name: String, account_no: String, bank_account_name: String, listener: RestApiListener) {

        callAlamofire(endpoint: "user/request-payout-summary?dtbm_points=\(dtbm_points)&bank_name=\(bank_name)&account_no=\(account_no)&bank_account_name=\(bank_account_name)", method: .get, header: ["Authorization": "JWT "+token], param: nil, call: RequestCall.getPayoutSummary, listener: listener)
    }
    
    static func requestPayout(token: String, dtbm_points: Int, bank_name: String, account_no: String, bank_account_name: String, pin: String, listener: RestApiListener) {
           let param: Parameters = [
               "dtbm_points": dtbm_points,
               "bank_name": bank_name,
               "account_no": account_no,
               "bank_account_name": bank_account_name,
               "pin": pin
           ]
        
            print("param - ",param)
           
           callAlamofire(endpoint: "user/request-payout", method: .post, header: ["Authorization": "JWT "+token], param: param, call: RequestCall.requestPayout, listener: listener)
       }
    
    static func idVerification( imageUrl: NSURL, photo: UIImage, token: String, listener: RestApiListener) {
        
        let url = URL(string: baseURL + "user/id_verification/fetch_citizen_card_data")
        
        let filename = imageUrl.absoluteString
        let imageData = photo.jpegData(compressionQuality: 0.5)!
        
        
        Alamofire.upload(multipartFormData: { (multipart) in
            multipart.append(imageData, withName: "citizen_card_image", fileName: filename!, mimeType: "image/jpeg")
        },
                         usingThreshold: UInt64.init(),
                         to: url!,
                         method: .post,
                         headers: ["Authorization": "JWT "+token],
                  
                         encodingCompletion: { encodingResult in
                            switch encodingResult {
                            case .success(let upload, _, _):
                                upload.response{ (resp) in
                                    print(resp)
                                }
                                upload.responseJSON { (JSON) in
                                    print(JSON)
                                }
                            case .failure(let encodingError):
                                print(encodingError)
                            }
        })
    }
    
    static func verifyId(token: String, nik: String, name: String, birthdate: String, birthplace: String, gender: String, address: String, rtrw: String, village: String, districts: String, religion: String, marriage_status: String, occupation: String, income: String, is_completed: Bool, listener: RestApiListener) {
        let param: Parameters = [
            "nik" : nik,
            "name" : name,
            "birthdate" : birthdate,
            "birthplace" : birthplace,
            "gender" : gender,
            "address" : address,
            "rtrw" : rtrw,
            "village" : village,
            "districts" : districts,
            "religion" : religion,
            "marriage_status" : marriage_status,
            "occupation" : occupation,
            "income" : income,
            "is_completed" : is_completed
        ]
     
         print("param - ",param)
        
        callAlamofire(endpoint: "id_verification/confirm", method: .post, header: ["Authorization": "JWT "+token], param: param, call: RequestCall.submitVerification, listener: listener)
    }
    
    static func updateProfile(token: String, name: String, email: String, province: String, city: String, phone: String, listener: RestApiListener) {
        let param: Parameters = [
            "name" : name,
            "province" : province,
            "email" : email,
            "city" : city,
            "phone" : phone
        ]
     
         print("param - ",param)
        
        callAlamofire(endpoint: "user/profile", method: .post, header: ["Authorization": "JWT "+token], param: param, call: RequestCall.updateProfile, listener: listener)
    }
}

