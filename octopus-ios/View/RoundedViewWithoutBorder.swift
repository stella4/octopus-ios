//
//  RoundedViewWithoutBorder.swift
//  octopus-ios
//
//  Created by Maven on 28/04/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import UIKit

class RoundedViewWithoutBorder: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    required init(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)!
        self.setup()
    }
    override init(frame:CGRect) {
        super.init(frame:frame)
        self.setup()
    }
    override  func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    func setup() {
        var layer:CALayer;
        layer = self.layer;
        layer.cornerRadius = 4.0;
        layer.borderWidth = 0;
//        layer.masksToBounds = false
//        layer.shadowColor = UIColor.darkGray.cgColor
//        layer.shadowOpacity = 1
//        layer.shadowRadius = 4
//        layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
    }
}
