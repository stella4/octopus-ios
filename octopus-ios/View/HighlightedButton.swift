//
//  HighlightedButton.swift
//  octopus-ios
//
//  Created by Maven on 11/06/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import UIKit

class HighlightedButton: UIButton {

    let blueOctopus = UIColor(hexString: "3F97E1")
    let highlightedColor = UIColor(hexString: "3889CC")
    
    override var isHighlighted: Bool {
        didSet {
            backgroundColor = isHighlighted ? highlightedColor : blueOctopus
            
        }
    }

}
