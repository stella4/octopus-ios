//
//  CircleView.swift
//  Trunkey
//
//  Created by Unicode on 31/10/17.
//  Copyright © 2017 Unicode. All rights reserved.
//

import UIKit

class CircleView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)!
        self.setup()
    }
    override init(frame:CGRect) {
        super.init(frame:frame)
        self.setup()
    }
    override  func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    func setup() {
        var layer:CALayer;
        layer = self.layer;
        layer.cornerRadius = self.frame.width / 2;
        layer.borderColor = UIColor.init(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 0.0).cgColor;
        layer.borderWidth = CGFloat(0.0);
    }

}
