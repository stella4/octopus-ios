//
//  RoundedView.swift
//  octopus-ios
//
//  Created by Maven on 16/04/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import UIKit

class RoundedView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    required init(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)!
        self.setup()
    }
    override init(frame:CGRect) {
        super.init(frame:frame)
        self.setup()
    }
    override  func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    func setup() {
        var layer:CALayer;
        layer = self.layer;
        layer.cornerRadius = 2.0;
        layer.borderColor = UIColor.init(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor;
        layer.borderWidth = CGFloat(1.0);
    }
}
