//
//  PayoutModel.swift
//  octopus-ios
//
//  Created by Maven on 13/05/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import Foundation

class PayoutModel: Decodable {
    var account_name = ""
    var account_no = ""
    var bank_name = ""
    var amount = ""
    var fee = ""
    var total = ""
    var dtbm = 0
}
