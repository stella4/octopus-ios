//
//  PayoutSummaryViewController.swift
//  octopus-ios
//
//  Created by Maven on 13/05/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import UIKit
import Alamofire

class PayoutSummaryViewController: UIViewController, RestApiListener {
    
    @IBOutlet weak var labelPoints: UILabel!
    @IBOutlet weak var labelAmount: UILabel!
    @IBOutlet weak var labelFee: UILabel!
    @IBOutlet weak var labelTotal: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelBankAndNumber: UILabel!
    
    @IBOutlet weak var wrapperAlert: UIView!
    
    var token = UserDefaults.standard.string(forKey: "token")
    let dtbmSB: UIStoryboard = UIStoryboard(name: "Dtbm", bundle: nil)
    var tempPayout : PayoutModel = PayoutModel()
    var dtbmPoints = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        wrapperAlert.alpha = 0
        
        labelPoints.text = tempPayout.dtbm.withCommas()
        labelAmount.text = "- Rp. "+tempPayout.amount
        labelFee.text = "- Rp. "+tempPayout.fee
        labelTotal.text = "- Rp. "+tempPayout.total
        labelName.text = tempPayout.bank_name
        labelBankAndNumber.text = "(\(tempPayout.account_name)) \(tempPayout.account_no)"
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func confirmAction(_ sender: Any) {
        let checkPinVC = dtbmSB.instantiateViewController(withIdentifier: "checkPinVC") as! CheckPinViewController
        checkPinVC.source = "payout"
        checkPinVC.tempPayout = tempPayout
        
        self.navigationController?.pushViewController(checkPinVC, animated: true)
    }
    
    @IBAction func okAction(_ sender: Any) {
        
    }
    
    func successResponse(call: RequestCall, response: DataResponse<Any>) {
        let historyTransactionVC = dtbmSB.instantiateViewController(withIdentifier: "historyTransactionVC") as! TransactionHistoryViewController
        
        self.navigationController?.pushViewController(historyTransactionVC, animated: true)
    }
    
    func failureResponse(call: RequestCall, response: DataResponse<Any>) {
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
