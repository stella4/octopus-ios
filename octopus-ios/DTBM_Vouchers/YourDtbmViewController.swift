//
//  YourDtbmViewController.swift
//  octopus-ios
//
//  Created by Maven on 24/04/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Kingfisher

class YourDtbmViewController: UIViewController, RestApiListener, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    static let sharedInstance = YourDtbmViewController()
    
    @IBOutlet weak var voucherCollectionView: UICollectionView!
    @IBOutlet weak var heighConstantVoucherCV: NSLayoutConstraint!
    @IBOutlet weak var labelYourDtbm: UILabel!
    
    var token = UserDefaults.standard.string(forKey: "token")
    let dtbmSB: UIStoryboard = UIStoryboard(name: "Dtbm", bundle: nil)
    var arrVouchers : [VoucherModel] = []
    var yourDTBM : String = ""
    var account_type = ""
    var myDtbmInt = 0
    var unwind = 0
    var showToastSuccess = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelYourDtbm.text = yourDTBM
        Service.getVouchers(token: token!, listener: self)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if YourDtbmViewController.sharedInstance.showToastSuccess {
            
            YourDtbmViewController.sharedInstance.showToastSuccess = false
            self.showToast(message: "Berhasil Mendaftar Premium Account")
        }
        Service.chackAccountType(token: token!, listener: self)
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.performSegue(withIdentifier: "unwindToHomeSegue", sender: self)
    }
    
    @IBAction func unwindToYourDtbm(segue: UIStoryboardSegue) {
        
    }
    
    @IBAction func goToMyVoucherAction(_ sender: Any) {
        let myVouchersVC = dtbmSB.instantiateViewController(withIdentifier: "myVouchersVC") as! MyVouchersViewController
        myVouchersVC.push = 0
        
        self.navigationController?.pushViewController(myVouchersVC, animated: true)
    }

    @IBAction func goToHistoryTransactionAction(_ sender: Any) {
        let historyTransactionVC = dtbmSB.instantiateViewController(withIdentifier: "historyTransactionVC") as! TransactionHistoryViewController
        
        self.navigationController?.pushViewController(historyTransactionVC, animated: true)
    }
    
    @IBAction func goToRequestPayoutAction(_ sender: Any) {
        if account_type == "basic" {
            let premiumVC = dtbmSB.instantiateViewController(withIdentifier: "premiumVC") as! ApplyPremiumViewController

            self.navigationController?.pushViewController(premiumVC, animated: true)
        } else {
            let requestVC = dtbmSB.instantiateViewController(withIdentifier: "requestVC") as! RequestPayoutViewController

            self.navigationController?.pushViewController(requestVC, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrVouchers.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellIdentifier: String = "cell"
        let cell = self.voucherCollectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath)

        let imageView : UIImageView = cell.viewWithTag(2) as! UIImageView
        let labelTitle : UILabel = cell.viewWithTag(3) as! UILabel
        let labelDetail : UILabel = cell.viewWithTag(4) as! UILabel
        let labelAmount : UILabel = cell.viewWithTag(5) as! UILabel
        let imageLogo : UIImageView = cell.viewWithTag(6) as! UIImageView
        let labelRibbon : UILabel = cell.viewWithTag(7) as! UILabel
        let ribbon : UIView = cell.viewWithTag(9)!
        
        let bannerModel : VoucherModel = arrVouchers[indexPath.row]
        
        ribbon.backgroundColor = UIColor.init(hexString: bannerModel.ribbon_color)
        labelTitle.text = bannerModel.name
        labelDetail.text = bannerModel.merchant_name
        labelAmount.text = bannerModel.dtbm_cost
        labelRibbon.text = bannerModel.ribbon_text
        
        imageView.kf.indicatorType = .activity
        imageView.kf.setImage(with: URL(string: bannerModel.cover_image))

        imageLogo.kf.indicatorType = .activity
        imageLogo.kf.setImage(with: URL(string: bannerModel.merchant_logo))

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let detailVoucherVC = dtbmSB.instantiateViewController(withIdentifier: "detailVoucherVC") as! DetailVoucherViewController
        detailVoucherVC.idVoucher = arrVouchers[indexPath.row].id
        detailVoucherVC.myDTBM = yourDTBM
        detailVoucherVC.myDtbmInt = myDtbmInt
        self.navigationController?.pushViewController(detailVoucherVC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.backgroundColor = UIColor.init(hexString: "EBEBEB")
    }
    
    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.backgroundColor = UIColor.init(hexString: "F5F5F5")
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenRect : CGRect = UIScreen.main.bounds
        let screenWidth : CGFloat = screenRect.size.width
        let cellWidth : Double = Double(screenWidth * 0.44)
        let cellHeight : Double = Double(cellWidth * 1.32)
        
        let size : CGSize = CGSize(width: cellWidth, height: cellHeight)

        return size;
    }
    
    func successResponse(call: RequestCall, response: DataResponse<Any>) {
        if (call == .accountType) {
            print("account type success")
            let res = response.result.value
            if (res != nil) {
                let json = JSON(res!)
                let type = json["data"]["account_type"].stringValue
                account_type = type
            }
        }
        
        if (call == .vouchers) {
            let res = response.result.value
            if (res != nil) {
                let json = JSON(res!)
                var result: VoucherModel
                arrVouchers = []
                for (index, _) in json.enumerated() {
                    result = VoucherModel()
                    result.id = json[index]["id"].intValue
                    result.name = json[index]["name"].stringValue
                    result.detail = json[index]["details"].stringValue
                    result.merchant_name = json[index]["merchant"]["name"].stringValue
                    result.dtbm_cost = json[index]["dtbm_cost"].stringValue
                    result.cover_image = json[index]["cover_image"].stringValue
                    result.merchant_logo = json[index]["merchant"]["logo"].stringValue
                    result.ribbon_text = json[index]["ribbon_text"].stringValue
                    result.ribbon_color = json[index]["ribbon_color"].stringValue
                    arrVouchers.append(result)
                }
                
                DispatchQueue.main.async {
                    if(self.arrVouchers.count == 0){
                        self.voucherCollectionView.isHidden = true
                    }else{
                        self.voucherCollectionView.isHidden = false
                    }

                    self.voucherCollectionView.reloadData()
                    let height = self.voucherCollectionView.collectionViewLayout.collectionViewContentSize.height;
                    self.heighConstantVoucherCV.constant = height
                }
            }
        }
        
    }
    
    func failureResponse(call: RequestCall, response: DataResponse<Any>) {
        print("failureResponse")
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
