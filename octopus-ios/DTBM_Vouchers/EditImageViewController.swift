//
//  EditImageViewController.swift
//  octopus-ios
//
//  Created by Maven on 12/05/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class EditImageViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var viewImage: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var guideView: UIView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var selected = 1
    var image : UIImage?
    var theImage: UIImage?
    var imageURL: NSURL?
    
    var token = UserDefaults.standard.string(forKey: "token")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageView.image = image
        
        scrollView.minimumZoomScale = 0.1
        scrollView.maximumZoomScale = 4.0
        scrollView.zoomScale = 1.0
        scrollView.delegate = self
        
        slider.value = 1
        slider.minimumValue = 0.1
        slider.maximumValue = 2.0
        
        guideView.layer.borderColor = UIColor.white.cgColor
        guideView.layer.borderWidth = 2.0
        guideView.translatesAutoresizingMaskIntoConstraints = false
        
        slider.addTarget(self, action: #selector(self.valueChanged(sender:)), for: .valueChanged)
        
        // Do any additional setup after loading the view.
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        let offsetX = max((scrollView.bounds.size.width - scrollView.contentSize.width) * 0.5, 0.0)
        let offsetY = max((scrollView.bounds.size.height - scrollView.contentSize.height) * 0.5, 0.0)

        scrollView.contentInset = UIEdgeInsets(top: offsetY, left: offsetX, bottom: offsetY, right: offsetX);
    }
    
    @objc func valueChanged(sender: UISlider) {
        
        if selected == 0 {
            imageView.transform = CGAffineTransform(rotationAngle: CGFloat(slider.value * 2 * Float(Double.pi) / slider.maximumValue))
        } else {
            scrollView.setZoomScale(CGFloat(slider!.value), animated: true)
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.performSegue(withIdentifier: "unwindToEktp", sender: self)
    }
    
    @IBAction func segmentedControlChange(_ sender: Any) {
        switch segmentedControl.selectedSegmentIndex {
            case 0:
                slider.minimumValue = -180
                slider.maximumValue = 180
                selected = 0
//                slider.value = 0
            case 1:
                slider.minimumValue = 0.1
                slider.maximumValue = 2
                selected = 1
//                slider.value = 1
            default: break
        }
    }
    
    @IBAction func okAction(_ sender: Any) {
//        let alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
//
//        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
//        loadingIndicator.hidesWhenStopped = true
//        loadingIndicator.style = UIActivityIndicatorView.Style.gray
//        loadingIndicator.startAnimating();
//
//        alert.view.addSubview(loadingIndicator)
//        present(alert, animated: true, completion: nil)
        
        cropImage()
    }
    
    func cropImage() {
        let newImage = image!.croppedInRect(rect: CGRect(x: self.guideView.frame.origin.x, y: self.guideView.frame.origin.y, width: self.guideView.layer.frame.width, height: self.guideView.layer.frame.height))
        
        let now = Date()
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "MMdd_HHmmssSSS"
        let dateString = formatter.string(from: now)
        
        let imgName = "region_\(dateString).jpeg"
        print(imgName)
        
        let documentDirectory = NSTemporaryDirectory()
        let localPath = documentDirectory.appending(imgName)
        
        let data = newImage!.jpegData(compressionQuality: 0.3)! as NSData
        data.write(toFile: localPath, atomically: true)
        self.imageURL = URL.init(fileURLWithPath: localPath) as NSURL
        self.theImage = newImage
                
        UIImageWriteToSavedPhotosAlbum(image!, nil, nil, nil)
        UIImageWriteToSavedPhotosAlbum(newImage!, nil, nil, nil)
        
//        self.submitId()
    }
    
    func submitId() {
        let url = "http://dev.api.octopus.co.id/api/user/id_verification/fetch_citizen_card_data"
        
        let filename = self.imageURL?.absoluteString
        let imageData = self.theImage!.jpegData(compressionQuality: 0.5)!
        
        
        Alamofire.upload(multipartFormData: { (multipart) in
            multipart.append(imageData, withName: "citizen_card_image", fileName: filename!, mimeType: "image/jpeg")
        },
             usingThreshold: UInt64.init(),
             to: url,
             method: .post,
             headers: ["Authorization": "JWT "+token!],
      
             encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    
                    let tempData : VerificationModel = VerificationModel()
                    
                    upload.responseJSON { (response) in
                        print(response)
                        
                        let json = JSON(response.result.value)
                        tempData.is_completed = json["data"]["attribute"]["is_completed"].boolValue
                        tempData.nik = json["data"]["attribute"]["nik"].stringValue
                        tempData.name = json["data"]["attribute"]["name"].stringValue
                        tempData.birthdate = json["data"]["attribute"]["birthdate"].stringValue
                        tempData.birthplace = json["data"]["attribute"]["birthplace"].stringValue
                        tempData.gender = json["data"]["attribute"]["gender"].stringValue
                        tempData.address = json["data"]["attribute"]["address"].stringValue
                        tempData.rtrw = json["data"]["attribute"]["rtrw"].stringValue
                        tempData.village = json["data"]["attribute"]["village"].stringValue
                        tempData.districts = json["data"]["attribute"]["districts"].stringValue
                        tempData.religion = json["data"]["attribute"]["religion"].stringValue
                        tempData.marriage_status = json["data"]["attribute"]["marriage_status"].stringValue
                        tempData.occupation = json["data"]["attribute"]["occupation"].stringValue
                        tempData.income = json["data"]["attribute"]["income"].stringValue
                        
                        self.dismiss(animated: true, completion: nil)
                        
                        let dtbmSB: UIStoryboard = UIStoryboard(name: "Dtbm", bundle: nil)
                        let verifyVC = dtbmSB.instantiateViewController(withIdentifier: "verifyIdVC") as! VerifyIdViewController
                        verifyVC.tempData = tempData
                        self.navigationController?.pushViewController(verifyVC, animated: true)
                    }
                    
                case .failure(let encodingError):
                    print(encodingError)
//                    self.activityIndicator.stopAnimating()
                }
        })
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//extension UIImage {
//    func rotate1(degrees: CGFloat) {
//
//        let degreesToRadians: (CGFloat) -> CGFloat = { (degrees: CGFloat) in
//            return degrees / 180.0 * CGFloat.pi
//        }
//        self.transform =  CGAffineTransform(rotationAngle: degreesToRadians(degrees))
//
//        // If you like to use layer you can uncomment the following line
//        //layer.transform = CATransform3DMakeRotation(degreesToRadians(degrees), 0.0, 0.0, 1.0)
//    }
//}

extension UIImage {
    func rotate(radians: Float) -> UIImage? {
        var newSize = CGRect(origin: CGPoint.zero, size: self.size).applying(CGAffineTransform(rotationAngle: CGFloat(radians))).size
        // Trim off the extremely small float value to prevent core graphics from rounding it up
        newSize.width = floor(newSize.width)
        newSize.height = floor(newSize.height)

        UIGraphicsBeginImageContextWithOptions(newSize, false, self.scale)
        let context = UIGraphicsGetCurrentContext()!

        // Move origin to middle
        context.translateBy(x: newSize.width/2, y: newSize.height/2)
        // Rotate around middle
        context.rotate(by: CGFloat(radians))
        // Draw the image at its center
        self.draw(in: CGRect(x: -self.size.width/2, y: -self.size.height/2, width: self.size.width, height: self.size.height))

        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage
    }
    
    func imageRotatedByDegrees(oldImage: UIImage, deg degrees: CGFloat) -> UIImage {
        let size = oldImage.size

        UIGraphicsBeginImageContext(size)

        let bitmap: CGContext = UIGraphicsGetCurrentContext()!
        //Move the origin to the middle of the image so we will rotate and scale around the center.
        bitmap.translateBy(x: size.width / 2, y: size.height / 2)
        //Rotate the image context
        bitmap.rotate(by: (degrees * CGFloat(M_PI / 180)))
        //Now, draw the rotated/scaled image into the context
        bitmap.scaleBy(x: 1.0, y: -1.0)

        let origin = CGPoint(x: -size.width / 2, y: -size.width / 2)

        bitmap.draw(oldImage.cgImage!, in: CGRect(origin: origin, size: size))

        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
}
