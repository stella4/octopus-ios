//
//  MyVoucherTableViewCell.swift
//  octopus-ios
//
//  Created by Maven on 14/05/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import UIKit
import Kingfisher

protocol myvoucherscellviewcontrollerdelegate {
    func refreshViewVoucher()
}

class MyVoucherTableViewCell: UITableViewCell {

    @IBOutlet weak var imageViewLogo: UIImageView!
    @IBOutlet weak var labelVoucherName: UILabel!
    @IBOutlet weak var labelVoucherCategory: UILabel!
    @IBOutlet weak var labelVoucherAmount: UILabel!
    @IBOutlet weak var labelVoucherDescription: UILabel!
    @IBOutlet weak var labelVoucherValid: UILabel!
    @IBOutlet weak var labelStatus: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    
    var endValidDate = ""
    var usedDate = "0"
    var statusTab = 0
    
    var timer = Timer()
    
    var delegate : myvoucherscellviewcontrollerdelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func showCellSkeleton() {
        imageViewLogo?.showAnimatedGradientSkeleton()
        labelVoucherName.showAnimatedGradientSkeleton()
        labelVoucherCategory.showAnimatedGradientSkeleton()
        labelVoucherAmount.showAnimatedGradientSkeleton()
        labelVoucherDescription.showAnimatedGradientSkeleton()
        labelVoucherValid.showAnimatedGradientSkeleton()
        stackView.alpha = 0
        labelStatus.alpha = 0
    }
    
    func bind(data: VoucherModel, status: Int) {
        imageViewLogo?.hideSkeleton()
        labelVoucherName.hideSkeleton()
        labelVoucherCategory.hideSkeleton()
        labelVoucherAmount.hideSkeleton()
        labelVoucherDescription.hideSkeleton()
        labelVoucherValid.hideSkeleton()
        stackView.alpha = 1
        labelStatus.alpha = 1
        
        imageViewLogo.kf.indicatorType = .activity
        imageViewLogo.kf.setImage(with: URL(string: data.merchant_logo))

        labelVoucherName.text = data.merchant_name
        labelVoucherCategory.text = data.merchant_category
        labelVoucherAmount.text = data.amount
        labelVoucherDescription.text = data.detail
        
        endValidDate = ""
        statusTab = status
        
        if status == 1 {
            if data.used_at != "" {
                usedDate = "1"
                endValidDate = data.used_at
                timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(countdownValid), userInfo: nil, repeats: true)
                timer.fire()
                labelStatus.text = "Hangus pada"
            } else {
                usedDate = "0"
                endValidDate = data.end_valid_date
                timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(countdownDate), userInfo: nil, repeats: true)
                timer.fire()
                labelStatus.text = "Berlaku sampai"
            }
        } else {
            let dateFormatter = DateFormatter()
            
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'"
            dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
            guard let date = dateFormatter.date(from: data.used_at) else {
                fatalError()
            }
            
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            dateFormatter.timeZone = TimeZone.current
            
            let newDate = dateFormatter.string(from: date)
            
            labelStatus.text = "Dipakai pada "
            labelVoucherValid.text = newDate
        }
        
    }
    
    @objc func countdownDate() {
        if usedDate == "0" && statusTab == 1 {
            let releaseDate = endValidDate
            let futureDateFormatter = DateFormatter()
            futureDateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
            let date: Date = futureDateFormatter.date(from: releaseDate)!

            let currentDate = Date()
            let currentFormatter = DateFormatter()
            currentFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
            currentFormatter.timeZone = TimeZone.current

            let calendar = Calendar.current

            let CompetitionDayDifference = calendar.dateComponents([.day, .hour, .minute, .second], from: currentDate, to: date)

            let daysLeft = CompetitionDayDifference.day!
            let hoursLeft = CompetitionDayDifference.hour!
            let minutesLeft = CompetitionDayDifference.minute!
            let secondLeft = CompetitionDayDifference.second!

            let validDate = releaseDate.convertDateTable(dateString: releaseDate)

            let timeLeft = " ( \(String(describing: daysLeft)) Day, \(String(describing: hoursLeft)) : \(String(describing: minutesLeft)) : \(String(describing: secondLeft)) )"

            labelVoucherValid.text = validDate! + timeLeft
        }
    }
    
    @objc func countdownValid() {
        if endValidDate != "" && statusTab == 1 {
            let releaseDate = endValidDate
            let futureDateFormatter = DateFormatter()
            futureDateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'"
            futureDateFormatter.timeZone = TimeZone(abbreviation: "UTC")
            
            let date: Date = futureDateFormatter.date(from: releaseDate)!
            let dateAdded = date.adding(minutes: 10)

            let currentDateTime = Date()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'"
            let dateStr =  dateFormatter.string(from: currentDateTime)
            let dateNow : Date = dateFormatter.date(from: dateStr)!

            let CompetitionDayDifference = Calendar.current.dateComponents([.minute, .second], from: dateNow, to: dateAdded)

            let minutesLeft = CompetitionDayDifference.minute!
            let secondLeft = CompetitionDayDifference.second!

            futureDateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            futureDateFormatter.timeZone = TimeZone.current
            
            let newDate = futureDateFormatter.string(from: dateAdded)

            let timeLeft = " ( \(String(describing: minutesLeft)) : \(String(describing: secondLeft)) )"

            labelVoucherValid.text = newDate + timeLeft
            
            if minutesLeft <= 0 && secondLeft <= 0 {
                timer.invalidate()
                self.delegate.refreshViewVoucher()
            }
        }
    }
    
}
