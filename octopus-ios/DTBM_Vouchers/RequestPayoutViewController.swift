//
//  RequestPayoutViewController.swift
//  octopus-ios
//
//  Created by Maven on 13/05/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class RequestPayoutViewController: UIViewController, RestApiListener {

    @IBOutlet weak var labelPoints: UILabel!
    @IBOutlet weak var textFieldAmount: UITextField!
    @IBOutlet weak var alertAmount: UILabel!
    @IBOutlet weak var textFieldBankName: UITextField!
    @IBOutlet weak var alertBankName: UILabel!
    @IBOutlet weak var textFieldAccountNumber: UITextField!
    @IBOutlet weak var alertAccountNumber: UILabel!
    @IBOutlet weak var textFieldAccountName: UITextField!
    @IBOutlet weak var alertAccountName: UILabel!
    
    @IBOutlet weak var wrapperView: UIView!
    @IBOutlet weak var imageAlert: UIImageView!
    @IBOutlet weak var labelTitleAlert: UILabel!
    @IBOutlet weak var labelDescriptionAlert: UILabel!
    
    var token = UserDefaults.standard.string(forKey: "token")
    let dtbmSB: UIStoryboard = UIStoryboard(name: "Dtbm", bundle: nil)
    let payoutTemp : PayoutModel = PayoutModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        wrapperView.alpha = 0
        labelPoints.showAnimatedGradientSkeleton()
        textFieldAmount.addTarget(self, action: #selector(textFieldDidChange(_:)),
        for: .editingChanged)
        Service.getProfile(token: token!, listener: self)
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= 120
            }
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
//        if textField.text! != "" {
//            textFieldAmount.text = Int(textField.text!)?.withCommas()
//        } else {
//            textFieldAmount.text = "0"
//        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func closeWrapperAction(_ sender: Any) {
        wrapperView.alpha = 0
    }
    
    @IBAction func resignKeyboard(_ sender: Any) {
        textFieldAmount.resignFirstResponder()
        textFieldAccountName.resignFirstResponder()
        textFieldAccountNumber.resignFirstResponder()
        textFieldBankName.resignFirstResponder()
    }
    
    @IBAction func submitAction(_ sender: Any) {
        textFieldAmount.resignFirstResponder()
        textFieldAccountName.resignFirstResponder()
        textFieldAccountNumber.resignFirstResponder()
        textFieldBankName.resignFirstResponder()
        
        if textFieldAmount.text == "" || textFieldBankName.text! == "" || textFieldAccountNumber.text == "" || textFieldAccountName.text == "" {
            if textFieldAmount.text == "" {
                alertAmount.isHidden = false
            } else {
                alertAmount.isHidden = true
            }
            
            if textFieldBankName.text! == "" {
                alertBankName.isHidden = false
            } else {
                alertBankName.isHidden = true
            }
            
            if textFieldAccountNumber.text! == "" {
                alertAccountNumber.isHidden = false
            } else {
                alertAccountNumber.isHidden = true
            }
            
            if textFieldAccountName.text! == "" {
                alertAccountName.isHidden = false
            } else {
                alertAccountName.isHidden = true
            }
        } else {
            let amount = Int(textFieldAmount.text!)
            if amount! < 10000 {
                wrapperView.alpha = 1
                imageAlert.image = UIImage(named: "min_dtbm")
                labelTitleAlert.text = "Yah jumlah uangmu kurang"
                labelDescriptionAlert.text = "Jumlah uang yang kamu masukkan kurang, penarikan uang minimal harus 10.000 ya.."
            } else if amount! > payoutTemp.dtbm {
                wrapperView.alpha = 1
                imageAlert.image = UIImage(named: "point_not_enough")
                labelTitleAlert.text = "Yah poin DTBMmu belum cukup"
                labelDescriptionAlert.text = "Poin DTBMmu belum cukup untuk melakukan penarikan. Yuk, order dulu untuk tambah poin."
            } else {
                wrapperView.alpha = 0
                Service.getPayoutSummary(token: token!, dtbm_points: textFieldAmount.text!, bank_name: textFieldBankName.text!, account_no: textFieldAccountNumber.text!, bank_account_name: textFieldAccountName.text!, listener: self)
            }
        }
    }
    
    func successResponse(call: RequestCall, response: DataResponse<Any>) {
        
        if (call == .profile) {
            labelPoints.hideSkeleton()
            let res = response.result.value
            if (res != nil) {
                let json = JSON(res!)
                let points = json["wallet_user"]["balance"].intValue
                payoutTemp.dtbm = points
                labelPoints.text = points.withCommas()
            }
        }
        
        if (call == .getPayoutSummary) {
            let res = response.result.value
            
            if (res != nil) {
                let json = JSON(res!)
                payoutTemp.account_name = json["account_name"].stringValue
                payoutTemp.account_no = json["account_no"].stringValue
                payoutTemp.bank_name = json["bank_name"].stringValue
                let amount = json["amount"].stringValue
                payoutTemp.amount = amount
                let fee = json["fee"].stringValue
                payoutTemp.fee = fee
                let total = json["total"].stringValue
                payoutTemp.total = total
                
                let payoutSummaryVC = dtbmSB.instantiateViewController(withIdentifier: "payoutSummaryVC") as! PayoutSummaryViewController
                payoutSummaryVC.tempPayout = payoutTemp
                self.navigationController?.pushViewController(payoutSummaryVC, animated: true)
            }
        }
    }
    
    func failureResponse(call: RequestCall, response: DataResponse<Any>) {
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

struct Number {
    static let withSeparator: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.groupingSeparator = "." // or possibly "." / ","
//        formatter.numberStyle = .decimal
        return formatter
    }()
}
extension Int {
    var stringWithSepator: String {
        return Number.withSeparator.string(from: NSNumber(value: hashValue)) ?? ""
    }
}
