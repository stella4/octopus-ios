//
//  DetailVoucherViewController.swift
//  octopus-ios
//
//  Created by Maven on 29/04/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import  Kingfisher

class DetailVoucherViewController: UIViewController, RestApiListener {
    static let sharedInstance = DetailVoucherViewController()
    
    func successResponse(call: RequestCall, response: DataResponse<Any>) {
        
        if (call == .detailVoucher) {
            imageView.hideSkeleton()
            labelMerchant.hideSkeleton()
            logoMerchant.hideSkeleton()
            labelDetail.hideSkeleton()
            labelDetails.hideSkeleton()
            labelSyaratDanKetentuan.hideSkeleton()
            labelTermsCondition.hideSkeleton()
            labelCarUntukMenukarkan.hideSkeleton()
            labelHowToRedeem.hideSkeleton()
            labelAmount.hideSkeleton()
            labelMyDtbm.hideSkeleton()
            viewButton.hideSkeleton()
            imageViewCoins.hideSkeleton()
            
            let res = response.result.value
            if (res != nil) {
                let json = JSON(res!)
                
                idVoucher = json["data"]["id"].intValue
                
                let image = json["data"]["cover_image"].stringValue
                imageView.kf.indicatorType = .activity
                imageView.kf.setImage(with: URL(string: image))
                
                name = json["data"]["name"].stringValue
                labelMerchant.text = name
                
                let logo = json["data"]["merchant"]["logo"].stringValue
                logoMerchant.kf.indicatorType = .activity
                logoMerchant.kf.setImage(with: URL(string: logo))
                
                labelDetail.text = json["data"]["details"].stringValue
                labelTermsCondition.text = json["data"]["term_and_conditions"].stringValue
                labelHowToRedeem.text = json["data"]["how_to_use_instruction"].stringValue
                dtbmCost = json["data"]["dtbm_cost"].intValue
                
                labelAmount.text = "\(dtbmCost) DTBM"
                
                
            }
        }
    }
    
    func failureResponse(call: RequestCall, response: DataResponse<Any>) {
        
    }
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var labelMerchant: UILabel!
    @IBOutlet weak var logoMerchant: UIImageView!
    @IBOutlet weak var labelSyaratDanKetentuan: UILabel!
    @IBOutlet weak var labelTermsCondition: UILabel!
    @IBOutlet weak var labelCarUntukMenukarkan: UILabel!
    @IBOutlet weak var labelHowToRedeem: UILabel!
    @IBOutlet weak var labelAmount: UILabel!
    @IBOutlet weak var labelMyDtbm: UILabel!
    @IBOutlet weak var imageViewCoins: UIImageView!
    @IBOutlet weak var viewButton: RoundedViewWithoutBorder!
    @IBOutlet weak var labelDetails: UILabel!
    @IBOutlet weak var labelDetail: UILabel!
    
    @IBOutlet weak var wrapperView: UIView!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var labelRedeem: UILabel!
    
    @IBOutlet weak var wrapperSuccessRedeem: UIView!
    @IBOutlet weak var view2: UIView!
    
    @IBOutlet weak var wrapperFailed: UIView!
    
    let dtbmSB: UIStoryboard = UIStoryboard(name: "Dtbm", bundle: nil)
    var token = UserDefaults.standard.string(forKey: "token")
    var idVoucher : Int = 0
    var myDTBM : String = ""
    var showWrapperSuccessRedeem = false
    var dtbmCost : Int = 0
    var name : String = ""
    var myDtbmInt = 0 
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view1.layer.cornerRadius = 4
        view2.layer.cornerRadius = 4
        
        labelMyDtbm.text = "My DTBM "+myDTBM
        
        Service.getDetailVouchers(token: token!, id: idVoucher, listener: self)
        
        imageView.showAnimatedGradientSkeleton()
        labelMerchant.showAnimatedGradientSkeleton()
        logoMerchant.showAnimatedGradientSkeleton()
        labelDetails.showAnimatedGradientSkeleton()
        labelDetail.showAnimatedGradientSkeleton()
        labelSyaratDanKetentuan.showAnimatedGradientSkeleton()
        labelTermsCondition.showAnimatedGradientSkeleton()
        labelCarUntukMenukarkan.showAnimatedGradientSkeleton()
        labelHowToRedeem.showAnimatedGradientSkeleton()
        labelAmount.showAnimatedGradientSkeleton()
        labelMyDtbm.showAnimatedGradientSkeleton()
        viewButton.showAnimatedGradientSkeleton()
        imageViewCoins.showAnimatedGradientSkeleton()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        wrapperView.alpha = 0
        wrapperSuccessRedeem.alpha = 0
        if DetailVoucherViewController.sharedInstance.showWrapperSuccessRedeem {
            wrapperSuccessRedeem.alpha = 1
        }
    }
    
    @IBAction func okMengertiAction(_ sender: Any) {
        wrapperFailed.alpha = 0
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func getCouponAction(_ sender: Any) {
        if myDtbmInt < dtbmCost {
            wrapperFailed.alpha = 1
        } else {
            labelRedeem.text = "Anda akan menukar \(dtbmCost) DTBM dengan "+name
            wrapperView.alpha = 1
        }
    }
    
    @IBAction func redeemAction(_ sender: Any) {
        let checkPinVC = dtbmSB.instantiateViewController(withIdentifier: "checkPinVC") as! CheckPinViewController
        checkPinVC.voucherId = idVoucher
        checkPinVC.source = "voucher"
        self.navigationController?.pushViewController(checkPinVC, animated: true)
        
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        wrapperView.alpha = 0
    }
    
    @IBAction func okAction(_ sender: Any) {
        DetailVoucherViewController.sharedInstance.showWrapperSuccessRedeem = false
        let myVouchersVC = dtbmSB.instantiateViewController(withIdentifier: "myVouchersVC") as! MyVouchersViewController
        myVouchersVC.push = 1
        
        self.navigationController?.pushViewController(myVouchersVC, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
