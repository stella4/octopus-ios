//
//  VoucherModel.swift
//  octopus-ios
//
//  Created by Maven on 29/04/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import Foundation

class VoucherModel : Decodable {
//    var abbreviation = ""
    var amount = ""
//    var code = ""
    var cover_image = ""
    var current_redeems_count = ""
    var used_at = ""
    var detail = ""
    var dtbm_cost = ""
    var end_valid_date = ""
    var how_to_use_instruction = ""
    var id = 0
    var is_active = ""
    var is_digital = ""
    var max_redeems = ""
    var merchant_id = 0
    var merchant_logo = ""
    var merchant_category = ""
    var merchant_name = ""
    var merchant_group = ""
    var name = ""
    var voucher_name = ""
    var voucher_code = ""
    var quantity = 0
    var ribbon_text = ""
    var ribbon_color = ""
    var start_valid_date = ""
    var term_and_conditions = ""
}
