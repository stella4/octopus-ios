//
//  VerificationModel.swift
//  octopus-ios
//
//  Created by Maven on 13/05/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import Foundation

class VerificationModel: Decodable {
    var is_completed = false
    var nik = ""
    var name = ""
    var birthdate = ""
    var birthplace = ""
    var gender = ""
    var address = ""
    var rtrw = ""
    var village = ""
    var districts = ""
    var religion = ""
    var marriage_status = ""
    var occupation = ""
    var income = ""
}
