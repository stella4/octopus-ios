//
//  MyVouchersViewController.swift
//  octopus-ios
//
//  Created by Maven on 29/04/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Kingfisher

protocol myvouchersviewcontrollerdelegate {
    func refreshViewVoucher()
}

class MyVouchersViewController: UIViewController, RestApiListener, UITableViewDataSource, UITableViewDelegate, myvoucherscellviewcontrollerdelegate {
    
    let dtbmSB: UIStoryboard = UIStoryboard(name: "Dtbm", bundle: nil)
    var token = UserDefaults.standard.string(forKey: "token")
    var arrVouchers : [VoucherModel] = []
    var showSkeleton = 1
    var push = 0
    var status = 1
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewLineVoucher: UIView!
    @IBOutlet weak var viewLineHistory: UIView!
    @IBOutlet weak var labelNotif: UILabel!
    
    var delegate : myvouchersviewcontrollerdelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        showSkeleton = 1
        print("show skeleton - ",showSkeleton)
        
        viewLineHistory.alpha = 0
        
        self.registerTableViewCells()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
        
        Service.getActiveVouchers(token: token!, listener: self)
    }
    
    func refreshViewVoucher() {
        viewLineHistory.alpha = 0
        viewLineVoucher.alpha = 1
        showSkeleton = 1
        status = 1
        arrVouchers = []
        tableView.reloadData()
        Service.getActiveVouchers(token: token!, listener: self)
    }
    
    private func registerTableViewCells() {
        let nib = UINib(nibName: "MyVoucherTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "myVoucherTableViewCell")
        tableView.dataSource = self
    }
    
    @IBAction func activeVoucherAction(_ sender: Any) {
        labelNotif.text = "Anda belum mempunyai voucher yang sedang aktif"
        viewLineHistory.alpha = 0
        viewLineVoucher.alpha = 1
        showSkeleton = 1
        status = 1
        arrVouchers = []
        tableView.reloadData()
        Service.getActiveVouchers(token: token!, listener: self)
    }
    
    @IBAction func historyVoucherAction(_ sender: Any) {
        labelNotif.text = "Anda belum mempunyai voucher yang sudah digunakan / sudah hangus"
        viewLineVoucher.alpha = 0
        viewLineHistory.alpha = 1
        showSkeleton = 1
        status = 2
        arrVouchers = []
        tableView.reloadData()
        Service.getPastVouchers(token: token!, listener: self)
    }
    
    @IBAction func backAction(_ sender: Any) {
        if push == 0 {
            self.navigationController?.popViewController(animated: true)
        } else {
            let yourDtbmVC = dtbmSB.instantiateViewController(withIdentifier: "yourDtbmVC") as! YourDtbmViewController
            yourDtbmVC.unwind = 1
            self.navigationController?.pushViewController(yourDtbmVC, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if showSkeleton == 1 {
            return 8
        }
        return arrVouchers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myVoucherTableViewCell", for: indexPath) as! MyVoucherTableViewCell
        cell.delegate = self
        
        if showSkeleton == 1 {
            cell.showCellSkeleton()
        } else {
            cell.bind(data: arrVouchers[indexPath.row], status: status)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if viewLineVoucher.alpha == 1.0 {
            let detailCouponVC = dtbmSB.instantiateViewController(withIdentifier: "detailCouponVC") as! DetailCouponViewController
            
            detailCouponVC.couponSelected = arrVouchers[indexPath.row]
            
            self.navigationController?.pushViewController(detailCouponVC, animated: true)
        }
    }
    
    func failureResponse(call: RequestCall, response: DataResponse<Any>) {
        tableView.alpha = 0
    }
    
    func successResponse(call: RequestCall, response: DataResponse<Any>) {
        showSkeleton = 0
        
        if (call == .activeVoucher) {
            let res = response.result.value
            if (res != nil) {
                let json = JSON(res!)
                let arrayJSON = json["data"].arrayValue
                var result: VoucherModel
                arrVouchers = []
                for (index, _) in arrayJSON.enumerated() {
                    result = VoucherModel()
                    
                    result.used_at = arrayJSON[index]["used_at"].stringValue
                    result.merchant_name = arrayJSON[index]["voucher_code"]["voucher"]["merchant"]["name"].stringValue
                    result.merchant_logo = arrayJSON[index]["voucher_code"]["voucher"]["merchant"]["logo"].stringValue
                    result.voucher_code = arrayJSON[index]["voucher_code"]["voucher"]["code"].stringValue
                    result.voucher_name = arrayJSON[index]["voucher_code"]["voucher"]["name"].stringValue
                    result.merchant_category = arrayJSON[index]["voucher_code"]["voucher"]["abbreviation"].stringValue
                    result.detail = arrayJSON[index]["voucher_code"]["voucher"]["details"].stringValue
                    let amount = arrayJSON[index]["voucher_code"]["voucher"]["amount"].intValue
                    result.amount = "Rp "+amount.withCommas()+".00"
                    result.cover_image = arrayJSON[index]["voucher_code"]["voucher"]["cover_image"].stringValue
                    result.end_valid_date = arrayJSON[index]["voucher_code"]["voucher"]["end_valid_date"].stringValue
                    result.term_and_conditions = arrayJSON[index]["voucher_code"]["voucher"]["term_and_conditions"].stringValue
                    result.id = arrayJSON[index]["voucher_code"]["id"].intValue
                    
                    arrVouchers.append(result)
                }
                if arrVouchers.count == 0 {
                    tableView.alpha = 0
                } else {
                    tableView.alpha = 1
                }
                tableView.reloadData()
            }
        }
        
        if (call == .pastVoucher) {
            let res = response.result.value
            if (res != nil) {
                let json = JSON(res!)
                let arrayJSON = json["data"].arrayValue
                var result: VoucherModel
                arrVouchers = []
                for (index, _) in arrayJSON.enumerated() {
                    result = VoucherModel()
                    
                    result.merchant_name = arrayJSON[index]["voucher_code"]["voucher"]["merchant"]["name"].stringValue
                    result.voucher_name = arrayJSON[index]["voucher_code"]["voucher"]["name"].stringValue
                    result.merchant_logo = arrayJSON[index]["voucher_code"]["voucher"]["merchant"]["logo"].stringValue
                    result.merchant_category = arrayJSON[index]["voucher_code"]["voucher"]["abbreviation"].stringValue
                    result.used_at = arrayJSON[index]["used_at"].stringValue
                    result.detail = arrayJSON[index]["voucher_code"]["voucher"]["details"].stringValue
                    let amount = arrayJSON[index]["voucher_code"]["voucher"]["amount"].intValue
                    result.amount = "Rp "+amount.withCommas()+".00"
                    
                    arrVouchers.append(result)
                }
                
                if arrVouchers.count == 0 {
                    tableView.alpha = 0
                } else {
                    tableView.alpha = 1
                }
                tableView.reloadData()
            }
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension String {
    func convertDate(dateString: String) -> String? {
        
        let dateFormatter = DateFormatter()
    
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        guard let date = dateFormatter.date(from: dateString) else {
            fatalError()
        }
        
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "E,d/MMM/yyyy"
        
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        timeFormatter.timeZone = TimeZone(abbreviation: "UTC")
        guard let time = timeFormatter.date(from: dateString) else {
            fatalError()
        }
        
        timeFormatter.dateFormat = "HH:mm"
        timeFormatter.timeZone = TimeZone.current
        
        let newDate = dateFormatter.string(from: date)
        let newTime = timeFormatter.string(from: time)
        return newDate + " at " + newTime
    }
    
    func convertDateTable(dateString: String) -> String? {
        let dateFormatter = DateFormatter()
    
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        guard let date = dateFormatter.date(from: dateString) else {
            fatalError()
        }
        
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone.current
        
        let newDate = dateFormatter.string(from: date)
        
        return newDate
    }
    
    func convertDateTableSSSSSS(dateString: String) -> String? {
        let dateFormatter = DateFormatter()
    
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        guard let date = dateFormatter.date(from: dateString) else {
            fatalError()
        }
        
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone.current
        
        let newDate = dateFormatter.string(from: date)
        
        
        return newDate
    }
}
