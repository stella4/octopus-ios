//
//  CameraViewController.swift
//  octopus-ios
//
//  Created by Maven on 07/05/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import UIKit
import AVFoundation
import Alamofire
import SwiftyJSON

class CameraViewController: UIViewController, AVCapturePhotoCaptureDelegate, AVCaptureVideoDataOutputSampleBufferDelegate {
    
    @IBOutlet weak var takePictureButton: UIButton!
    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var guideView: UIView!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var camerImage: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var captureSession = AVCaptureSession()
    var previewLayer: CALayer!
    var captureDevice: AVCaptureDevice!

    /// This will be true when the user clicks on the click photo button.
    var takePhoto = false
    
    var theImage: UIImage?
    var imageURL: NSURL?
    
    let dtbmSB: UIStoryboard = UIStoryboard(name: "Dtbm", bundle: nil)
    var token = UserDefaults.standard.string(forKey: "token")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        guideView.layer.cornerRadius = 8
        guideView.layer.borderColor = UIColor.white.cgColor
        guideView.layer.borderWidth = 2.0
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // Setup your camera here...
        
        captureSession = AVCaptureSession()
        previewLayer = CALayer()
        takePhoto = false
        
        requestAuthorization()
    }

    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /// This function will request authorization, If authorized then start the camera.
    private func requestAuthorization() {
        switch AVCaptureDevice.authorizationStatus(for: AVMediaType.video) {
        case .authorized:
            prepareCamera()

        case .denied, .restricted, .notDetermined:
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted) in
                if !Thread.isMainThread {
                    DispatchQueue.main.async {
                        if granted {
                            self.prepareCamera()
                        } else {
                            let alert = UIAlertController(title: "unable_to_access_the_Camera", message: "to_enable_access_go_to_setting_privacy_camera_and_turn_on_camera_access_for_this_app", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "ok", style: .default, handler: {_ in
                                self.navigationController?.popToRootViewController(animated: true)
                            }))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                } else {
                    if granted {
                        self.prepareCamera()
                    } else {
                        let alert = UIAlertController(title: "unable_to_access_the_Camera", message: "to_enable_access_go_to_setting_privacy_camera_and_turn_on_camera_access_for_this_app", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "ok", style: .default, handler: {_ in
                            self.navigationController?.popToRootViewController(animated: true)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            })
        default: break
            
        }
        
    }

    /// Will see if the primary camera is avilable, If found will call method which will asign the available device to the AVCaptureDevice.
    private func prepareCamera() {
        // Resets the session.
        self.captureSession.sessionPreset = AVCaptureSession.Preset.photo

        if #available(iOS 10.0, *) {
            let availableDevices = AVCaptureDevice.DiscoverySession(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera], mediaType: AVMediaType.video, position: .back).devices
            self.assignCamera(availableDevices)
        } else {
            // Fallback on earlier versions
            // development, need to test this on iOS 8
            if let availableDevices = AVCaptureDevice.default(for: AVMediaType.video) {
                self.assignCamera([availableDevices])
            } else {
                self.showAlert()
            }
        }
    }

    /// Assigns AVCaptureDevice to the respected the variable, will begin the session.
    ///
    /// - Parameter availableDevices: [AVCaptureDevice]
    private func assignCamera(_ availableDevices: [AVCaptureDevice]) {
        if availableDevices.first != nil {
            captureDevice = availableDevices.first
            beginSession()
        } else {
            self.showAlert()
        }
    }

    /// Configures the camera settings and begins the session, this function will be responsible for showing the image on the UI.
    private func beginSession() {
        do {
            let captureDeviceInput = try AVCaptureDeviceInput(device: captureDevice)
            captureSession.addInput(captureDeviceInput)
        } catch {
            print(error.localizedDescription)
        }

        let previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        self.previewLayer = previewLayer
        self.previewView.layer.addSublayer(self.previewLayer)
        self.previewLayer.frame = self.view.layer.frame
        self.previewLayer.frame.origin.y = +self.previewView.frame.origin.y
        (self.previewLayer as! AVCaptureVideoPreviewLayer).videoGravity = AVLayerVideoGravity.resizeAspectFill
        self.previewLayer.masksToBounds = true
        self.previewView.clipsToBounds = true
        captureSession.startRunning()

        self.view.bringSubviewToFront(self.previewView)
        self.view.bringSubviewToFront(self.backImage)
        self.view.bringSubviewToFront(self.backButton)
        self.view.bringSubviewToFront(self.camerImage)
        self.view.bringSubviewToFront(self.guideView)
        self.view.bringSubviewToFront(self.takePictureButton)

        let dataOutput = AVCaptureVideoDataOutput()
        dataOutput.videoSettings = [((kCVPixelBufferPixelFormatTypeKey as NSString) as String):NSNumber(value:kCVPixelFormatType_32BGRA)]

        dataOutput.alwaysDiscardsLateVideoFrames = true

        if captureSession.canAddOutput(dataOutput) {
            captureSession.addOutput(dataOutput)
        }

        captureSession.commitConfiguration()

        let queue = DispatchQueue(label: "com.letsappit.camera")
        dataOutput.setSampleBufferDelegate(self, queue: queue)

    }


    /// Get the UIImage from the given CMSampleBuffer.
    ///
    /// - Parameter buffer: CMSampleBuffer
    /// - Returns: UIImage?
    func getImageFromSampleBuffer(buffer:CMSampleBuffer, orientation: UIImage.Orientation) -> UIImage? {
        if let pixelBuffer = CMSampleBufferGetImageBuffer(buffer) {
            let ciImage = CIImage(cvPixelBuffer: pixelBuffer)
            let context = CIContext()
            let imageRect = CGRect(x: 0, y: 0, width: CVPixelBufferGetWidth(pixelBuffer), height: CVPixelBufferGetHeight(pixelBuffer))

            if let image = context.createCGImage(ciImage, from: imageRect) {
                return UIImage(cgImage: image, scale: UIScreen.main.scale, orientation: orientation)

            }

        }
        return nil
    }

    /// This function will destroy the capture session.
    func stopCaptureSession() {
        self.captureSession.stopRunning()

        if let inputs = captureSession.inputs as? [AVCaptureDeviceInput] {
            for input in inputs {
                self.captureSession.removeInput(input)
            }
        }
    }

    func showAlert() {
        let alert = UIAlertController(title: "Unable to access the camera", message: "It appears that either your device doesn't have camera or its broken", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "cancel", style: .cancel, handler: {_ in
            self.navigationController?.popViewController(animated: true)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func takePictureAction(_ sender: Any) {
//        userinteractionToButton(false)
        let alert = UIAlertController(title: nil, message: "Mengupload Data", preferredStyle: .alert)

        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating();

        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
        
        takePhoto = true
    }
    
    func captureOutput(_ captureOutput: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {

        if connection.isVideoOrientationSupported {
            connection.videoOrientation = .portrait
        }

        if takePhoto {
            takePhoto = false

            // Rotation should be unlocked to work.
            var orientation = UIImage.Orientation.up
            switch UIDevice.current.orientation {
            case .landscapeLeft:
                orientation = .left

            case .landscapeRight:
                orientation = .right

            case .portraitUpsideDown:
                orientation = .down

            default:
                orientation = .up
            }

            if let image = self.getImageFromSampleBuffer(buffer: sampleBuffer, orientation: orientation) {
                DispatchQueue.main.async {
                    
                    let newImage = image.croppedInRect(rect: CGRect(x: self.guideView.frame.origin.x, y: self.guideView.frame.origin.y, width: self.guideView.layer.frame.width, height: self.guideView.layer.frame.height))
                    
                    let now = Date()
                    let formatter = DateFormatter()
                    formatter.timeZone = TimeZone.current
                    formatter.dateFormat = "MMdd_HHmmssSSS"
                    let dateString = formatter.string(from: now)
                    
                    let imgName = "region_\(dateString).jpeg"
                    print(imgName)
                    
                    let documentDirectory = NSTemporaryDirectory()
                    let localPath = documentDirectory.appending(imgName)
                    
                    let data = newImage!.jpegData(compressionQuality: 0.3)! as NSData
                    data.write(toFile: localPath, atomically: true)
                    self.imageURL = URL.init(fileURLWithPath: localPath) as NSURL
                    self.theImage = newImage
                            
                    UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
                    UIImageWriteToSavedPhotosAlbum(newImage!, nil, nil, nil)
                    
                    self.submitId()
                }
            }
        }
    }
    
    func submitId() {
        let url = "http://dev.api.octopus.co.id/api/user/id_verification/fetch_citizen_card_data"
        
        let filename = self.imageURL?.absoluteString
        let imageData = self.theImage!.jpegData(compressionQuality: 0.5)!
        
        
        Alamofire.upload(multipartFormData: { (multipart) in
            multipart.append(imageData, withName: "citizen_card_image", fileName: filename!, mimeType: "image/jpeg")
        },
             usingThreshold: UInt64.init(),
             to: url,
             method: .post,
             headers: ["Authorization": "JWT "+token!],
      
             encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    
                    let tempData : VerificationModel = VerificationModel()
                    
                    upload.responseJSON { (response) in
                        print(response)
                        
                        let json = JSON(response.result.value)
                        tempData.is_completed = json["data"]["attribute"]["is_completed"].boolValue
                        tempData.nik = json["data"]["attribute"]["nik"].stringValue
                        tempData.name = json["data"]["attribute"]["name"].stringValue
                        tempData.birthdate = json["data"]["attribute"]["birthdate"].stringValue
                        tempData.birthplace = json["data"]["attribute"]["birthplace"].stringValue
                        tempData.gender = json["data"]["attribute"]["gender"].stringValue
                        tempData.address = json["data"]["attribute"]["address"].stringValue
                        tempData.rtrw = json["data"]["attribute"]["rtrw"].stringValue
                        tempData.village = json["data"]["attribute"]["village"].stringValue
                        tempData.districts = json["data"]["attribute"]["districts"].stringValue
                        tempData.religion = json["data"]["attribute"]["religion"].stringValue
                        tempData.marriage_status = json["data"]["attribute"]["marriage_status"].stringValue
                        tempData.occupation = json["data"]["attribute"]["occupation"].stringValue
                        tempData.income = json["data"]["attribute"]["income"].stringValue
                        
                        self.dismiss(animated: true, completion: nil)
                        
                        let dtbmSB: UIStoryboard = UIStoryboard(name: "Dtbm", bundle: nil)
                        let verifyVC = dtbmSB.instantiateViewController(withIdentifier: "verifyIdVC") as! VerifyIdViewController
                        verifyVC.tempData = tempData
                        self.navigationController?.pushViewController(verifyVC, animated: true)
                    }
                    
                case .failure(let encodingError):
                    print(encodingError)
                    self.dismiss(animated: true, completion: nil)
                    self.showToast(message: "Gagal memindai foto, silahkan coba lagi.")
                }
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.captureSession.stopRunning()
    }
    
    @IBAction func bacAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UIImage {
    
    func croppedInRect(rect: CGRect) -> UIImage? {
        // Correct rect size based on the device screen scale
        let scaledRect = CGRect(x: rect.origin.x, y: rect.origin.y, width: rect.size.width * self.scale, height: rect.size.height * self.scale);
        // New CGImage reference based on the input image (self) and the specified rect
        let imageRef = self.cgImage!.cropping(to: scaledRect);
        // Gets an UIImage from the CGImage
        let result = UIImage(cgImage: imageRef!, scale: self.scale, orientation: self.imageOrientation)
        // Returns the final image, or NULL on error
        return result;

    }
}
