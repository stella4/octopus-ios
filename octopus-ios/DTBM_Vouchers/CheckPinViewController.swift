//
//  CheckPinViewController.swift
//  octopus-ios
//
//  Created by Maven on 30/04/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class CheckPinViewController: UIViewController, RestApiListener {
    
    @IBOutlet weak var txtCode1: UITextField!
    @IBOutlet weak var txtCode2: UITextField!
    @IBOutlet weak var txtCode3: UITextField!
    @IBOutlet weak var txtCode4: UITextField!
    @IBOutlet weak var txtCode5: UITextField!
    @IBOutlet weak var txtCode6: UITextField!
    
    @IBOutlet weak var imageAlert: UIImageView!
    @IBOutlet weak var wrapperAlert: UIView!
    @IBOutlet weak var labelTitleAlert: UILabel!
    @IBOutlet weak var labelDescriptionAlert: UILabel!
    
    var token = UserDefaults.standard.string(forKey: "token")
    let dtbmSB: UIStoryboard = UIStoryboard(name: "Dtbm", bundle: nil)
    var tempPayout : PayoutModel = PayoutModel()
    var voucherId : Int?
    var source = ""
    var incorrect = false
    var phoneNumber = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        wrapperAlert.alpha = 0
        Service.getProfile(token: token!, listener: self)
        txtCode1.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        txtCode2.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        txtCode3.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        txtCode4.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        txtCode5.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        txtCode6.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        // Do any additional setup after loading the view.
    }
    
    @objc func textFieldDidChange(textField: UITextField){
        let text = textField.text
        if text?.count == 1 {
            switch textField{
            case txtCode1:
                txtCode2.becomeFirstResponder()
            case txtCode2:
                txtCode3.becomeFirstResponder()
            case txtCode3:
                txtCode4.becomeFirstResponder()
            case txtCode4:
                txtCode5.becomeFirstResponder()
            case txtCode5:
                txtCode6.becomeFirstResponder()
            case txtCode6:
                txtCode6.resignFirstResponder()
                let code1 = txtCode1.text!
                let code2 = txtCode2.text!
                let code3 = txtCode3.text!
                let code4 = txtCode4.text!
                let code5 = txtCode5.text!
                let code6 = txtCode6.text!
                let code = code1+code2+code3+code4+code5+code6
                
                let alert = UIAlertController(title: nil, message: "Mohon Tunggu", preferredStyle: .alert)

                let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
                loadingIndicator.hidesWhenStopped = true
                loadingIndicator.style = UIActivityIndicatorView.Style.gray
                loadingIndicator.startAnimating();

                alert.view.addSubview(loadingIndicator)
                present(alert, animated: true, completion: nil)
                
                if source == "voucher" {
                    Service.checkUserPin(token: token!, pin: code, listener: self)
                } else if source == "payout" {
                    requestPayout(pin: code)
                }
                
            default:
                break
            }
        }
        if text?.count == 0 {
            switch textField{
            case txtCode1:
                txtCode1.becomeFirstResponder()
            case txtCode2:
                txtCode1.becomeFirstResponder()
            case txtCode3:
                txtCode2.becomeFirstResponder()
            case txtCode4:
                txtCode3.becomeFirstResponder()
            case txtCode5:
                txtCode4.becomeFirstResponder()
            case txtCode6:
                txtCode5.becomeFirstResponder()
            default:
                break
            }
        }
    }
    
    @IBAction func forgotPasswordAction(_ sender: Any) {
        let authSB: UIStoryboard = UIStoryboard(name: "Authorization", bundle: nil)
        let pinVC = authSB.instantiateViewController(withIdentifier: "pinVC") as! PinViewController
        pinVC.phoneNumber = phoneNumber
        pinVC.unwind = true
        self.navigationController?.pushViewController(pinVC, animated: true)
    }
    
    func requestPayout(pin: String) {
        Service.requestPayout(token: token!, dtbm_points: Int(tempPayout.amount)!, bank_name: tempPayout.bank_name, account_no: tempPayout.account_no, bank_account_name: tempPayout.account_name, pin: pin, listener: self)
//        let param: Parameters = [
//            "dtbm_points": Int(tempPayout.amount)!,
//            "bank_name": tempPayout.bank_name,
//            "account_no": tempPayout.account_no,
//            "bank_account_name": tempPayout.account_name,
//            "pin": pin
//        ]
//
//        let jsonParam = JSON(param)
//
//        let representation = jsonParam.rawString([.castNilToNSNull: true])
//        print(representation!)
//        let JsonString = representation! //  Ex.:-  "{\"action\": \"fetch-data\"}"
//        var request = URLRequest(url: URL(string: "http://dev.api.octopus.co.id/api/user/request-payout")!)
//        request.httpMethod = "POST"
//        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
//        request.addValue("JWT "+token!, forHTTPHeaderField: "Authorization")
//
////        let pjson = JsonString
////        let data = (pjson.data(using: .utf8))! as Data
////
//        let parameters = "{\n\t\"dtbm_points\":\(Int(tempPayout.amount)!),\n\t\"bank_name\":\"\(tempPayout.bank_name)\",\n\t\"account_no\":\"\(tempPayout.account_no)\",\n\t\"bank_account_name\":\"\(tempPayout.account_name)\",\n\t\"pin\": \"\(pin)\"\n}"
//        let postData = parameters.data(using: .utf8)
//
//        request.httpBody = postData
//
//        print(postData)
//
//        Alamofire.request(request).responseJSON { (response) in
//            self.dismiss(animated: false, completion: nil)
//            switch response.result {
//            case .success:
//                print(response.result.value)
//
//                if response.description.contains("Incorrect") {
//                    self.incorrect = true
//                    self.imageAlert.image = UIImage(named: "alert_phone_exist")
//                    self.labelTitleAlert.text = "PIN Salah"
//                    self.labelDescriptionAlert.text = "PIN yang kamu masukkan salah. Silahkan coba lagi."
//                } else {
//                    self.incorrect = false
//                    self.imageAlert.image = UIImage(named: "request_done")
//                    self.labelTitleAlert.text = "Permintaanmu berhasil diproses"
//                    self.labelDescriptionAlert.text = "Yeay, permintaan penarikan uangmu telah berhasil diproses. Cek email ya untuk informasi lebih lanjut."
//                }
//
//                self.wrapperAlert.alpha = 1
//
//                break
//            case .failure(let error):
//
//                print(error.localizedDescription)
//            }
//
//        }
        
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func okAction(_ sender: Any) {
        if incorrect {
            wrapperAlert.alpha = 0
            
            txtCode1.text = ""
            txtCode2.text = ""
            txtCode3.text = ""
            txtCode4.text = ""
            txtCode5.text = ""
            txtCode6.text = ""
        } else {
            let historyTransactionVC = dtbmSB.instantiateViewController(withIdentifier: "historyTransactionVC") as! TransactionHistoryViewController
            
            self.navigationController?.pushViewController(historyTransactionVC, animated: true)
        }
    }
    
    func successResponse(call: RequestCall, response: DataResponse<Any>) {
        self.dismiss(animated: true, completion: nil)
        if (call == .checkUserPin) {
            redeemVoucher(id: voucherId!)
        }
        
        if (call == .profile) {
            let res = response.result.value
            if (res != nil) {
                let json = JSON(res!)
                phoneNumber = json["phone"].stringValue
            }
        }
        
        if (call == .requestPayout) {
            if response.description.contains("Incorrect") {
                self.incorrect = true
                self.imageAlert.image = UIImage(named: "alert_phone_exist")
                self.labelTitleAlert.text = "PIN Salah"
                self.labelDescriptionAlert.text = "PIN yang kamu masukkan salah. Silahkan coba lagi."
            } else {
                self.incorrect = false
                self.imageAlert.image = UIImage(named: "request_done")
                self.labelTitleAlert.text = "Permintaanmu berhasil diproses"
                self.labelDescriptionAlert.text = "Yeay, permintaan penarikan uangmu telah berhasil diproses. Cek email ya untuk informasi lebih lanjut."
            }

            self.wrapperAlert.alpha = 1
        }
    }
    
    func redeemVoucher(id: Int) {
        let param: Parameters = [
            "voucher_id": id
        ]
        
        let jsonParam = JSON(param)
            
        let representation = jsonParam.rawString([.castNilToNSNull: true])
        print(representation!)
        let JsonString = representation! //  Ex.:-  "{\"action\": \"fetch-data\"}"
        var request = URLRequest(url: URL(string: "http://dev.api.octopus.co.id/api/user/redeem-voucher")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("JWT "+token!, forHTTPHeaderField: "Authorization")

        let pjson = JsonString
        let data = (pjson.data(using: .utf8))! as Data

        request.httpBody = data
        
        print(data)

        Alamofire.request(request).responseJSON { (response) in
            self.dismiss(animated: true, completion: nil)
            
            switch response.result {
            case .success:
                print(response.result.value)
//                self.dismiss(animated: true, completion: nil)
                
                if response.description.contains("Incorrect") {
                    self.incorrect = true
                    self.imageAlert.image = UIImage(named: "alert_phone_exist")
                    self.labelTitleAlert.text = "PIN Salah"
                    self.labelDescriptionAlert.text = "PIN yang kamu masukkan salah. Silahkan coba lagi."
                    
                    self.wrapperAlert.alpha = 1
                } else {
                    DetailVoucherViewController.sharedInstance.showWrapperSuccessRedeem = true
                    self.navigationController?.popViewController(animated: true)
                }
                
                break
            case .failure(let error):
//                self.dismiss(animated: true, completion: nil)
                self.showToast(message: "Gagal tukar voucher.")
                print("gagal ",error.localizedDescription)
            }

        }
    }
    
    func failureResponse(call: RequestCall, response: DataResponse<Any>) {
        txtCode1.text = ""
        txtCode2.text = ""
        txtCode3.text = ""
        txtCode4.text = ""
        txtCode5.text = ""
        txtCode6.text = ""
        self.dismiss(animated: true, completion: nil)
        self.showToast(message: "Gagal cek PIN.")
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
