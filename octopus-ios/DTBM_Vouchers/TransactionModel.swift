//
//  TransactionModel.swift
//  octopus-ios
//
//  Created by Maven on 07/05/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import Foundation

class TransactionModel: Decodable {
    var id = 0
    var amount = ""
    var transaction_type = ""
    var description = ""
    var status = ""
    var created_at = ""
}
