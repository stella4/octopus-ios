//
//  VerifyIdViewController.swift
//  octopus-ios
//
//  Created by Maven on 13/05/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import UIKit
import Alamofire
import iOSDropDown
import SwiftyJSON

class VerifyIdViewController: UIViewController, RestApiListener {

    @IBOutlet weak var textFieldKtpId: UITextField!
    @IBOutlet weak var textFieldName: UITextField!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var dropdownIncome: DropDown!
    
    var token = UserDefaults.standard.string(forKey: "token")
    var arrIncome : [String] = []
    var accept = 0
    var tempData : VerificationModel = VerificationModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        textFieldKtpId.text = tempData.nik
        textFieldName.text = tempData.name
        dropdownIncome.isSearchEnable = false
        
        appendArrIncome()
        // Do any additional setup after loading the view.
    }
    
    func appendArrIncome() {
        arrIncome = []
        arrIncome.append("Pilih pendapatan anda")
        arrIncome.append("Rp 0 - Rp 500.000")
        arrIncome.append("Rp 500.000 - Rp 1.500.000")
        arrIncome.append("Rp 1.500.000 - Rp 5.000.000")
        arrIncome.append("Rp 5.000.000 - Rp 7.000.000")
        arrIncome.append("Rp 7.000.000 - Rp 10.000.000")
        arrIncome.append("> Rp 10.000.000")
        
        dropdownIncome.optionArray = self.arrIncome
        dropdownIncome.didSelect{ (selectedText, index, id) in
            self.dropdownIncome.text = selectedText
        }
        dropdownIncome.listWillAppear {
            self.dropdownIncome.listHeight = CGFloat(self.arrIncome.count * 20)
        }
        
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func acceptTermaConditionAction(_ sender: Any) {
        if accept == 0 {
            imageView.alpha = 1
            accept = 1
        } else {
            imageView.alpha = 0
            accept = 0
        }
    }
    
    @IBAction func resignKeyboard(_ sender: Any) {
        textFieldName.resignFirstResponder()
        textFieldKtpId.resignFirstResponder()
    }
    
    @IBAction func termsConditionAction(_ sender: Any) {
        let authorizationSB: UIStoryboard = UIStoryboard(name: "Authorization", bundle: nil)
        let termsVC = authorizationSB.instantiateViewController(withIdentifier: "termsVC") as! TermsConditionViewController
        
        self.navigationController?.pushViewController(termsVC, animated: true)
    }
    
    @IBAction func submitAction(_ sender: Any) {
        if textFieldName.text == "" || textFieldKtpId.text == "" || dropdownIncome.selectedIndex == 0 || accept == 0 {
            
        } else {
            let param: Parameters = [
                "nik" : textFieldKtpId.text!,
                "name" : textFieldName.text!,
                "birthdate" : tempData.birthdate,
                "birthplace" : tempData.birthplace,
                "gender" : tempData.gender,
                "address" : tempData.address,
                "rtrw" : tempData.rtrw,
                "village" : tempData.village,
                "districts" : tempData.districts,
                "religion" : tempData.religion,
                "marriage_status" : tempData.marriage_status,
                "occupation" : tempData.occupation,
                "income" : dropdownIncome.text!,
                "is_completed" :tempData.is_completed
            ]
            
            let jsonParam = JSON(param)
                
            let representation = jsonParam.rawString([.castNilToNSNull: true])
            print(representation!)
            let JsonString = representation! //  Ex.:-  "{\"action\": \"fetch-data\"}"
            var request = URLRequest(url: URL(string: "http://dev.api.octopus.co.id/api/user/id_verification/confirm")!)
            request.httpMethod = HTTPMethod.post.rawValue
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("JWT "+token!, forHTTPHeaderField: "Authorization")

            let pjson = JsonString
            let data = (pjson.data(using: .utf8))! as Data

            request.httpBody = data
            
            print(data)

            Alamofire.request(request).responseJSON { (response) in

                switch response.result {
                case .success:
                    print(response)
                    
                    YourDtbmViewController.sharedInstance.showToastSuccess = true
                    self.performSegue(withIdentifier: "unwindToYourDtbmSegue", sender: self)
                    
                    break
                case .failure(let error):

                    print(error)
                }

            }
//            Service.verifyId(token: token!, nik: textFieldKtpId.text!, name: textFieldName.text!, birthdate: tempData.birthdate, birthplace: tempData.birthplace, gender: tempData.gender, address: tempData.address, rtrw: tempData.rtrw, village: tempData.village, districts: tempData.districts, religion: tempData.religion, marriage_status: tempData.marriage_status, occupation: tempData.occupation, income: dropdownIncome.text!, is_completed: tempData.is_completed, listener: self)
        }
    }
    
    func successResponse(call: RequestCall, response: DataResponse<Any>) {
        
    }
    
    func failureResponse(call: RequestCall, response: DataResponse<Any>) {
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
