//
//  EKtpViewController.swift
//  octopus-ios
//
//  Created by Maven on 02/05/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import UIKit
import Mantis
import Alamofire
import SwiftyJSON

class EKtpViewController: UIViewController, CropViewControllerDelegate, ImagePickerDelegate {

    var token = UserDefaults.standard.string(forKey: "token")
    let dtbmSB: UIStoryboard = UIStoryboard(name: "Dtbm", bundle: nil)
    var theImage: UIImage?
    var imageURL: NSURL?
    var imagePicker: ImagePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func unwindToEktp(segue: UIStoryboardSegue) {
        
    }
    
    @IBAction func selectKtpAction(_ sender: UIButton) {
        self.imagePicker.present(from: sender)
    }
    
    func didSelect(image: UIImage?) {
        guard let image = image else {
            return
        }
        
        self.theImage = image
        
        let config = Mantis.Config()
        
        // Comment out the code below for using preset transformation
//        let transform = Mantis.Transformation(offset: CGPoint(x: 469.0, y: 942.3333333333334), rotation: 0.2806850373744965, scale: 4.157221958778101, manualZoomed: true, maskFrame: CGRect(x: 99.92007104795738, y: 14.0, width: 214.15985790408524, height: 701.0))
//
//        config.presetTransformationType = .presetInfo(info: transform)
        
        let cropViewController = Mantis.cropViewController(image: image,
                                                           config: config)
        cropViewController.modalPresentationStyle = .fullScreen
        cropViewController.config.presetFixedRatioType = .alwaysUsingOnePresetFixedRatio(ratio: 1.586)
        cropViewController.delegate = self
        present(cropViewController, animated: true)
    }
    
    func cropViewControllerDidCrop(_ cropViewController: CropViewController, cropped: UIImage, transformation: Transformation) {
        print(transformation);
        
        let now = Date()
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "MMdd_HHmmssSSS"
        let dateString = formatter.string(from: now)
        
        let imgName = "region_\(dateString).jpeg"
        print(imgName)
        
        let documentDirectory = NSTemporaryDirectory()
        let localPath = documentDirectory.appending(imgName)
        
        let data = cropped.jpegData(compressionQuality: 0.3)! as NSData
        data.write(toFile: localPath, atomically: true)
        self.imageURL = URL.init(fileURLWithPath: localPath) as NSURL
        self.theImage = cropped
    
        UIImageWriteToSavedPhotosAlbum(cropped, nil, nil, nil)
        
        self.dismiss(animated: true)
        addLoading()
    }
    
    func addLoading() {
        let alert = UIAlertController(title: nil, message: "Mohon tunggu", preferredStyle: .alert)

        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating();

        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
        
        submitId()
    }
    
    func cropViewControllerDidCancel(_ cropViewController: CropViewController, original: UIImage) {
        
        let now = Date()
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "MMdd_HHmmssSSS"
        let dateString = formatter.string(from: now)
        
        let imgName = "region_\(dateString).jpeg"
        print(imgName)
        
        let documentDirectory = NSTemporaryDirectory()
        let localPath = documentDirectory.appending(imgName)
        
        let data = original.jpegData(compressionQuality: 0.3)! as NSData
        data.write(toFile: localPath, atomically: true)
    
        UIImageWriteToSavedPhotosAlbum(original, nil, nil, nil)
        
        self.dismiss(animated: true)
    }
    
    @IBAction func applyNowActtion(_ sender: Any) {
        let cameraVC = dtbmSB.instantiateViewController(withIdentifier: "cameraVC") as! CameraViewController
        
        self.navigationController?.pushViewController(cameraVC, animated: true)
    }
    
    func submitId() {
        let url = "http://dev.api.octopus.co.id/api/user/id_verification/fetch_citizen_card_data"
        
        let filename = self.imageURL?.absoluteString
        let imageData = self.theImage!.jpegData(compressionQuality: 0.5)!
        
        
        Alamofire.upload(multipartFormData: { (multipart) in
            multipart.append(imageData, withName: "citizen_card_image", fileName: filename!, mimeType: "image/jpeg")
        },
             usingThreshold: UInt64.init(),
             to: url,
             method: .post,
             headers: ["Authorization": "JWT "+token!],
      
             encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    
                    let tempData : VerificationModel = VerificationModel()
                    
                    upload.responseJSON { (response) in
                        print(response)
                        
                        let json = JSON(response.result.value)
                        tempData.is_completed = json["data"]["attribute"]["is_completed"].boolValue
                        tempData.nik = json["data"]["attribute"]["nik"].stringValue
                        tempData.name = json["data"]["attribute"]["name"].stringValue
                        tempData.birthdate = json["data"]["attribute"]["birthdate"].stringValue
                        tempData.birthplace = json["data"]["attribute"]["birthplace"].stringValue
                        tempData.gender = json["data"]["attribute"]["gender"].stringValue
                        tempData.address = json["data"]["attribute"]["address"].stringValue
                        tempData.rtrw = json["data"]["attribute"]["rtrw"].stringValue
                        tempData.village = json["data"]["attribute"]["village"].stringValue
                        tempData.districts = json["data"]["attribute"]["districts"].stringValue
                        tempData.religion = json["data"]["attribute"]["religion"].stringValue
                        tempData.marriage_status = json["data"]["attribute"]["marriage_status"].stringValue
                        tempData.occupation = json["data"]["attribute"]["occupation"].stringValue
                        tempData.income = json["data"]["attribute"]["income"].stringValue
                        
                        self.dismiss(animated: true, completion: nil)
                        
                        let dtbmSB: UIStoryboard = UIStoryboard(name: "Dtbm", bundle: nil)
                        let verifyVC = dtbmSB.instantiateViewController(withIdentifier: "verifyIdVC") as! VerifyIdViewController
                        verifyVC.tempData = tempData
                        
                        
                        self.navigationController?.pushViewController(verifyVC, animated: true)
                    }
                    
                case .failure(let encodingError):
                    print(encodingError)
                    self.dismiss(animated: true, completion: nil)
                    self.showToast(message: "Gagal memindai foto, silahkan coba lagi.")
                }
        })
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
