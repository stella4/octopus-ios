//
//  DetailCouponViewController.swift
//  octopus-ios
//
//  Created by Maven on 02/05/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class DetailCouponViewController: UIViewController, RestApiListener {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var labelMerchant: UILabel!
    @IBOutlet weak var imageLogo: UIImageView!
    @IBOutlet weak var labelHighlights: UILabel!
    @IBOutlet weak var labelValidDate: UILabel!
    @IBOutlet weak var labelTermsCondition: UILabel!
    @IBOutlet weak var buttonUseNow: UIButton!
    
    @IBOutlet weak var wrapperAlert: UIView!
    @IBOutlet weak var voucherName: UILabel!
    @IBOutlet weak var changeToUsedButton: UIButton!
    
    @IBOutlet weak var wrapperUseCoupon: UIView!
    @IBOutlet weak var labelUseCoupon: UILabel!
    
    @IBOutlet weak var viewRedeemedCoupon: UIView!
    @IBOutlet weak var heighConstantViewRedeemed: NSLayoutConstraint!
    @IBOutlet weak var roundedView: UIView!
    @IBOutlet weak var roundedView2: UIView!
    @IBOutlet weak var labelExpiredIn: UILabel!
    @IBOutlet weak var labelNameCoupon: UILabel!
    @IBOutlet weak var labelDtbmAmount: UILabel!
    @IBOutlet weak var labelValidUntil: UILabel!
    @IBOutlet weak var labelVoucherCode: UILabel!
    
    var couponSelected : VoucherModel = VoucherModel()
    var account_type = ""
    var token = UserDefaults.standard.string(forKey: "token")
    let dtbmSB: UIStoryboard = UIStoryboard(name: "Dtbm", bundle: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        refreshView()
    }
    
    func refreshView() {
        wrapperUseCoupon.alpha = 0
        wrapperAlert.alpha = 0
        viewRedeemedCoupon.alpha = 0
        heighConstantViewRedeemed.constant = 0
        
        Service.chackAccountType(token: token!, listener: self)
        
        imageView.kf.indicatorType = .activity
        imageView.kf.setImage(with: URL(string: couponSelected.cover_image))
        labelMerchant.text = couponSelected.voucher_name
        imageLogo.kf.indicatorType = .activity
        imageLogo.kf.setImage(with: URL(string: couponSelected.merchant_logo))
        labelHighlights.text = couponSelected.detail
        labelValidDate.text = couponSelected.end_valid_date.convertDate(dateString: couponSelected.end_valid_date)
        labelTermsCondition.text = couponSelected.term_and_conditions
        
        if couponSelected.used_at != "" {
            couponUsed()
        }
    }
    
    @objc func countdownDate() {
        let releaseDate = couponSelected.used_at
        let futureDateFormatter = DateFormatter()
        futureDateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        futureDateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dateOri: Date = futureDateFormatter.date(from: releaseDate)!
        
        let dateAdded = dateOri.adding(minutes: 10)

        let currentDateTime = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let dateStr =  dateFormatter.string(from: currentDateTime)
        let dateNow : Date = dateFormatter.date(from: dateStr)!
        
        let one = Calendar.current.dateComponents([.minute, .second], from: dateNow, to: dateAdded)
        let minutesLeft = one.minute!
        let secondLeft = one.second!

        let timeLeft = "(\(String(describing: minutesLeft)):\(String(describing: secondLeft)))"

        labelExpiredIn.text = "Hangus pada "+timeLeft
                
        if minutesLeft <= 0 && secondLeft <= 0 {
            labelExpiredIn.text = "Kupon Anda telah hangus"
        }
    }
    
    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func useCouponAction(_ sender: Any) {
        if account_type == "basic" {
            let premiumVC = dtbmSB.instantiateViewController(withIdentifier: "premiumVC") as! ApplyPremiumViewController
            
            self.navigationController?.pushViewController(premiumVC, animated: true)
        } else {
            wrapperAlert.alpha = 1
            voucherName.text = couponSelected.voucher_name
        }
    }
    
    @IBAction func changeToUseAction(_ sender: Any) {
        labelUseCoupon.text = "Anda yakin akan menggunakan voucher \(couponSelected.name) sekarang?"
        wrapperUseCoupon.alpha = 1
    }
    
    @IBAction func changeCouponAction(_ sender: Any) {
        Service.useCoupon(token: token!, id: couponSelected.id, listener: self)
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        wrapperAlert.alpha = 0
        wrapperUseCoupon.alpha = 0
    }
    
    func successResponse(call: RequestCall, response: DataResponse<Any>) {
        if (call == .accountType) {
            print("account type success")
            let res = response.result.value
            if (res != nil) {
                let json = JSON(res!)
                let type = json["data"]["account_type"].stringValue
                account_type = type
            }
        }
        
        if (call == .useCoupon) {
            let res = response.result.value
            if (res != nil) {
                let json = JSON(res!)
                couponSelected.used_at = json["data"]["used_at"].stringValue
                
                couponUsed()
            }
        }
    }
    
    func couponUsed() {
        labelNameCoupon.text = couponSelected.voucher_name
        labelVoucherCode.text = couponSelected.voucher_code
        labelDtbmAmount.text = couponSelected.amount+" DTBM"
        
        let futureDateFormatter = DateFormatter()
        futureDateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'"
        futureDateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let date: Date = futureDateFormatter.date(from: couponSelected.used_at)!
        let dateAdded = date.adding(minutes: 10)
        
        futureDateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        futureDateFormatter.timeZone = TimeZone.current
        
        let newDate = futureDateFormatter.string(from: dateAdded)
        
        labelValidUntil.text = newDate
        let timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(countdownDate), userInfo: nil, repeats: true)
        timer.fire()
        
        roundedView.layer.cornerRadius = 10
        roundedView.layer.borderColor = UIColor.lightGray.cgColor
        roundedView.layer.borderWidth = 1
        
        roundedView2.layer.cornerRadius = 10
        
        buttonUseNow.backgroundColor = UIColor.lightGray
        buttonUseNow.isUserInteractionEnabled = false
        wrapperUseCoupon.alpha = 0
        wrapperAlert.alpha = 0
        heighConstantViewRedeemed.constant = 275
        viewRedeemedCoupon.alpha = 1
    }
    
    func failureResponse(call: RequestCall, response: DataResponse<Any>) {
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension Date {
    func adding(minutes: Int) -> Date {
        return Calendar.current.date(byAdding: .minute, value: minutes, to: self)!
    }
}
