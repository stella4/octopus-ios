//
//  TransactionHistoryViewController.swift
//  octopus-ios
//
//  Created by Maven on 07/05/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class TransactionHistoryViewController: UIViewController, RestApiListener, UITableViewDelegate, UITableViewDataSource {

    var token = UserDefaults.standard.string(forKey: "token")
    var showSkeleton = 1
    var arrTransaction : [TransactionModel] = []
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.reloadData()
        Service.getTransactionHistory(token: token!, listener: self)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.performSegue(withIdentifier: "unwindToYourDtbm", sender: self)
    }
    
    func successResponse(call: RequestCall, response: DataResponse<Any>) {
        showSkeleton = 0
        
        if (call == .transactionHistory) {
            let res = response.result.value
            
            if (res != nil) {
                let json = JSON(res!)
                let arrayJSON = json["data"].arrayValue
                var result : TransactionModel
                arrTransaction = []
                
                for (index, _) in arrayJSON.enumerated() {
                    result = TransactionModel()
                    result.id = arrayJSON[index]["id"].intValue
                    result.amount = arrayJSON[index]["amount"].stringValue
                    result.description = arrayJSON[index]["description"].stringValue
                    result.status = arrayJSON[index]["status"].stringValue
                    result.transaction_type = arrayJSON[index]["transaction_type"].stringValue
                    let date = arrayJSON[index]["created_at"].stringValue
                    result.created_at = date.convertDateTransaction(dateString: date)!
                    
                    arrTransaction.append(result)
                }
                if arrTransaction.count == 0 {
                    tableView.alpha = 0
                } else {
                    tableView.alpha = 1
                }
                
                tableView.reloadData()
            }
        }
    }
    
    func failureResponse(call: RequestCall, response: DataResponse<Any>) {
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if showSkeleton == 1 {
            return 8
        }
        return arrTransaction.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell_identifier = "cell"
               
        let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: cell_identifier, for: indexPath)
        
        let labelDate : UILabel = cell.viewWithTag(1) as! UILabel
        let labelDtbm : UILabel = cell.viewWithTag(2) as! UILabel
        let labelDescription : UILabel = cell.viewWithTag(3) as! UILabel
        let view1 : UIView = cell.viewWithTag(5)!
        
        if showSkeleton == 1 {
            labelDate.showAnimatedGradientSkeleton()
            labelDtbm.showAnimatedGradientSkeleton()
            labelDescription.showAnimatedGradientSkeleton()
            view1.alpha = 0
        } else {
            labelDate.hideSkeleton()
            labelDtbm.hideSkeleton()
            labelDescription.hideSkeleton()
            view1.alpha = 1
            
            labelDescription.text = arrTransaction[indexPath.row].description
            if arrTransaction[indexPath.row].transaction_type == "payout" {
                labelDescription.text = arrTransaction[indexPath.row].description + " (Status:\(arrTransaction[indexPath.row].status))"
            }
            
            labelDate.text = arrTransaction[indexPath.row].created_at
            
            if arrTransaction[indexPath.row].transaction_type == "pick_and_pack" {
                labelDescription.textColor = UIColor.init(hexString: "##3E99E1")
                labelDtbm.textColor = UIColor.init(hexString: "##3E99E1")
                labelDtbm.text = "+"+arrTransaction[indexPath.row].amount+" DTBM"
            } else {
                labelDescription.textColor = UIColor.black
                labelDtbm.textColor = UIColor.black
                labelDtbm.text = "-"+arrTransaction[indexPath.row].amount+" DTBM"
            }
        }
        
        return cell
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension String {
    func convertDateTransaction(dateString: String) -> String? {
        
        let dateFormatter = DateFormatter()
    
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        guard let date = dateFormatter.date(from: dateString) else {
            fatalError()
        }
        
        dateFormatter.dateFormat = "dd MMM, HH:mm"
        dateFormatter.timeZone = TimeZone.current
        
        let newDate = dateFormatter.string(from: date)
        
        return newDate
    }
}
