//
//  ApplyPremiumViewController.swift
//  octopus-ios
//
//  Created by Maven on 02/05/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import UIKit

class ApplyPremiumViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func applyNowAction(_ sender: Any) {
        let dtbmSB: UIStoryboard = UIStoryboard(name: "Dtbm", bundle: nil)
        let ktpVC = dtbmSB.instantiateViewController(withIdentifier: "ktpVC") as! EKtpViewController
        
        self.navigationController?.pushViewController(ktpVC, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
