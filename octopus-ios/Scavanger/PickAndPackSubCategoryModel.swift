//
//  PickAndPackSubCategoryModel.swift
//  octopus-ios
//
//  Created by Maven on 24/04/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import Foundation

class PickAndPackSubCategoryModel: Decodable {
    var amount = 0
    var description = ""
    var id = ""
    var image = ""
    var name = ""
    var quantity = 0
    var detail : [SubCategoryDetailModel] = []
}
