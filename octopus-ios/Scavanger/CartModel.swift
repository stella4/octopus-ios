//
//  CartModel.swift
//  octopus-ios
//
//  Created by Maven on 24/04/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import Foundation

class CartModel: Decodable {
    var id : String = ""
    var qty : Int = 0
    var dtbm : Int = 0
}
