//
//  OrderViewController.swift
//  octopus-ios
//
//  Created by Maven on 06/05/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class OrderViewController: UIViewController, RestApiListener,UITableViewDelegate, UITableViewDataSource {
    
    var token = UserDefaults.standard.string(forKey: "token")
    var showSkeleton = 1
    var arrOrder : [OrderModel] = []
    var onGoing = 1
    var notif = ""
    var type = ""
    var titleAlert = ""
    var bodyAlert = ""
    
    var open = true
    var openNotifOrder = false
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewLineOnGoing: UIView!
    @IBOutlet weak var viewLineHistory: UIView!
    @IBOutlet weak var wrapperAlert: UIView!
    @IBOutlet weak var imageAlert: UIImageView!
    @IBOutlet weak var alertDescription: UILabel!
    @IBOutlet weak var alertTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleNotification(notification:)), name: NSNotification.Name("PUSHACTION"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        open = true
        wrapperAlert.alpha = 0
        
        if notif == "order_finish" || notif == "order_canceled" {
            getHistoryOrder()
        } else {
            getOnGoingOrder()
        }
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(pullToRefresh(refreshControl:)), for: .valueChanged)
        tableView.addSubview(refreshControl)
    }
    
    @objc func handleNotification(notification:NSNotification) {
        let userinfo = notification.userInfo as! [String : AnyObject]
        print("user info chat - ",userinfo)
        
        type = userinfo["type"] as? String ?? ""
        
        if type == "chat" {
            if open {
                open = false
                let scavangerSB: UIStoryboard = UIStoryboard(name: "Scavanger", bundle: nil)
                let chatVC = scavangerSB.instantiateViewController(withIdentifier: "chatVC") as! ChatViewController
                if let notificationData = userinfo["data"] as? NSString {
                    var dictionary : NSDictionary?
                    if let data = notificationData.data(using: String.Encoding.utf8.rawValue) {
                        do {
                            dictionary = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary
                            if let dict = dictionary {
                                chatVC.order_id = dict["order_id"] as! String
                                
                                if let wd = UIApplication.shared.delegate?.window {
                                    var vc = wd!.rootViewController
                                    if(vc is UINavigationController){
                                        vc = (vc as! UINavigationController).visibleViewController
                                    }

                                    if(vc is ChatViewController) || (vc is CallScavangerViewController){
                                        //your code
                                        print("chat active")
                                    } else {
                                        self.navigationController?.pushViewController(chatVC, animated: true)
                                    }
                                }
                            }
                        } catch let error as NSError {
                            print("Error: \(error)")
                        }
                    }
                }
            }
        } else {
            if let name = userinfo["name"] {
                notif = name as! String
            }
            
            if let title = userinfo["title"] {
                titleAlert = title as! String
            }
            
            if let body = userinfo["message"] {
                bodyAlert = body as! String
            }
            
            if notif == "find_scav" || notif == "scav_found" || notif == "order_finish" || notif == "order_canceled" || notif == "scav_arrived" || notif == "picking_up"  {
                openNotifOrder = true
                
                if notif == "order_finish" || notif == "order_canceled" {
                    getHistoryOrder()
                } else {
                    getOnGoingOrder()
                }
                
                PopUpMessageViewController.sharedInstance.notifType = notif
                PopUpMessageViewController.sharedInstance.titleAlert = titleAlert
                PopUpMessageViewController.sharedInstance.bodyAlert = bodyAlert
                
                notif = ""
                
                if let wd = UIApplication.shared.delegate?.window {
                    var vc = wd!.rootViewController
                    if(vc is UINavigationController){
                        vc = (vc as! UINavigationController).visibleViewController
                    }

                    if(vc is ChatViewController) || (vc is CallScavangerViewController){
                        //your code
                        print("chat active")
                    } else {
                        if openNotifOrder {
                            if PopUpMessageViewController.sharedInstance.notifType != "" {
                                self.performSegue(withIdentifier: "popUpSegue", sender: self)
                            }
                        }
                    }
                }
            }
        }
    }
    
    func checkNotif() {
        if notif == "find_scav" {
            notif = ""
            wrapperAlert.alpha = 1
            imageAlert.image = UIImage(named: "looking_scav")
            alertTitle.text = "Mencari Scavenger"
            alertDescription.text = "Tunggu sebentar ya, kami sedang mencarikan Scav untukmu."
        } else if notif == "scav_arrived" {
            notif = ""
            imageAlert.image = UIImage(named: "scav_arrived")
            wrapperAlert.alpha = 1
            alertTitle.text = "Scavenger telah tiba"
            alertDescription.text = "Yeay, Scavmu sudah tiba. Yuk, serahkan semua sampahmu"
        } else if notif == "scav_found" {
            notif = ""
            imageAlert.image = UIImage(named: "scav_found")
            wrapperAlert.alpha = 1
            alertTitle.text = "Scavenger ditemukan"
            alertDescription.text = "Yeay, Scavmu sudah ditemukan. Hubungi dia sekarang"
        } else if notif == "picking_up" {
            notif = ""
            imageAlert.image = UIImage(named: "picking_up")
            wrapperAlert.alpha = 1
            alertTitle.text = "Sedang menjemput sampahmu"
            alertDescription.text = "Tunggu sebentar ya, Scav sedang menjemput sampahmu"
        } else if notif == "order_finish" {
            notif = ""
            imageAlert.image = UIImage(named: "order_done")
            wrapperAlert.alpha = 1
            alertTitle.text = "Order telah selesai"
            alertDescription.text = "Yeay, orderanmu sudah selesai!"
        } else if notif == "order_canceled" {
            notif = ""
            imageAlert.image = UIImage(named: "cancel_order")
            wrapperAlert.alpha = 1
            alertTitle.text = titleAlert
            alertDescription.text = bodyAlert
        }
    }
    
    @objc func pullToRefresh(refreshControl: UIRefreshControl) {
        // Update your conntent here

        if onGoing == 1 {
            Service.getOnGoingOrder(token: token!, listener: self)
        } else {
            Service.getHistoryOrder(token: token!, listener: self)
        }
        
        refreshControl.endRefreshing()
    }
    
    @IBAction func unwindToOrder(segue: UIStoryboardSegue) {
        
    }
    
    @IBAction func closeAlert(_ sender: Any) {
        wrapperAlert.alpha = 0
        getOnGoingOrder()
    }
    
    @IBAction func OnGoingOrderAction(_ sender: Any) {
        getOnGoingOrder()
   }
    
    func getOnGoingOrder() {
        onGoing = 1
        viewLineHistory.alpha = 0
        viewLineOnGoing.alpha = 1
        showSkeleton = 1
        arrOrder = []
        tableView.reloadData()
        Service.getOnGoingOrder(token: token!, listener: self)
    }
       
   @IBAction func historyOrderAction(_ sender: Any) {
        getHistoryOrder()
   }
    
    func getHistoryOrder() {
        onGoing = 0
        viewLineOnGoing.alpha = 0
        viewLineHistory.alpha = 1
        showSkeleton = 1
        arrOrder = []
        tableView.reloadData()
        Service.getHistoryOrder(token: token!, listener: self)
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.performSegue(withIdentifier: "unwindToHomeSegue", sender: self)
    }
    
    func successResponse(call: RequestCall, response: DataResponse<Any>) {
        showSkeleton = 0
        
        if (call == .onGoingOrder) {
            let res = response.result.value
            
            if (res != nil) {
                let json = JSON(res!)
                arrOrder = []
                
                for (index, _) in json.enumerated() {
                    autoreleasepool {
                        let result : OrderModel = OrderModel()
                        result.address = json[index]["address"].stringValue
                        result.id = json[index]["id"].intValue
                        result.name = json[index]["name"].stringValue
                        result.image = json[index]["image"].stringValue
                        result.order_is_accepted = json[index]["order"]["is_accepted"].boolValue
                        result.order_is_arrived = json[index]["order"]["is_arrived"].boolValue
                        result.order_is_picked = json[index]["order"]["is_picked"].boolValue
                        result.optional_address = json[index]["order"]["optional_address"].stringValue
                        let total = json[index]["order"]["total"].intValue
                        result.order_total = total.withCommas()
                        result.order_id = json[index]["order_id"].stringValue
                        let date = json[index]["updated_at"].stringValue
                        result.date = date.convertDateOrder(dateString: date)!
                        
                        arrOrder.append(result)
                    }
                }
                if arrOrder.count == 0 {
                    tableView.alpha = 0
                } else {
                    tableView.alpha = 1
                }
                tableView.reloadData()
            }
        }
        
        if (call == .historyOrder) {
            let res = response.result.value
            let json = JSON(res!)
            let jsonArray = json["order"].arrayValue
            if (res != nil) {
                arrOrder = []
                for (index, _) in jsonArray.enumerated() {
                    autoreleasepool {
                        let result : OrderModel = OrderModel()
                        result.address = jsonArray[index]["address"].stringValue
                        result.id = jsonArray[index]["id"].intValue
                        result.name = jsonArray[index]["name"].stringValue
                        let total = jsonArray[index]["total"].intValue
                        result.order_total = total.withCommas()
                        result.order_id = jsonArray[index]["order_id"].stringValue
                        let date = jsonArray[index]["updated_at"].stringValue
                        result.date = date.convertDateOrder(dateString: date)!
                        result.is_cancel = jsonArray[index]["is_cancel"].boolValue
                        
                        arrOrder.append(result)
                    }
                }
                if arrOrder.count == 0 {
                    tableView.alpha = 0
                } else {
                    tableView.alpha = 1
                }
                tableView.reloadData()
            }
        }
    }
    
    func failureResponse(call: RequestCall, response: DataResponse<Any>) {
        tableView.alpha = 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if showSkeleton == 1 {
            return 8
        }
        return arrOrder.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell_identifier = "cell"
               
        let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: cell_identifier, for: indexPath)
        
        let labelDtbm : UILabel = cell.viewWithTag(1) as! UILabel
        let labelDate : UILabel = cell.viewWithTag(2) as! UILabel
        let labelStatus : UILabel = cell.viewWithTag(3) as! UILabel
        let labelAddress : UILabel = cell.viewWithTag(4) as! UILabel
        let labelPickingUpAddress : UILabel = cell.viewWithTag(7) as! UILabel
        let view1 : UIView = cell.viewWithTag(5)!
        let view2 : UIView = cell.viewWithTag(6)!
        
        if showSkeleton == 1 {
            labelDate.showAnimatedGradientSkeleton()
            labelDtbm.showAnimatedGradientSkeleton()
            labelStatus.showAnimatedGradientSkeleton()
            labelPickingUpAddress.showAnimatedGradientSkeleton()
            labelAddress.showAnimatedGradientSkeleton()
            view1.alpha = 0
            view2.alpha = 0
        } else {
            labelDate.hideSkeleton()
            labelDtbm.hideSkeleton()
            labelStatus.hideSkeleton()
            labelAddress.hideSkeleton()
            labelPickingUpAddress.hideSkeleton()
            view1.alpha = 1
            view2.alpha = 1

            
            labelDtbm.text = "+"+arrOrder[indexPath.row].order_total+" DTBM"
            labelDate.text = arrOrder[indexPath.row].date
            labelAddress.text = arrOrder[indexPath.row].address
            labelStatus.textColor = UIColor.init(hexString: "#3E99E1")
            
            if onGoing == 1 {
                labelStatus.text = "Mencari"
                
                if arrOrder[indexPath.row].order_is_picked || arrOrder[indexPath.row].order_is_accepted {
                    labelStatus.text = "Menjemput"
                }
                
                if arrOrder[indexPath.row].order_is_arrived {
                    labelStatus.text = "Tiba di Lokasi"
                }
                
            } else {
                if arrOrder[indexPath.row].is_cancel {
                    labelStatus.textColor = UIColor.lightGray
                    labelStatus.text = "Order Dibatalkan"
                } else {
                    labelStatus.text = "Order Selesai"
                }
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if onGoing == 1 {
            let scavangerSB: UIStoryboard = UIStoryboard(name: "Scavanger", bundle: nil)
            let scavangerVC = scavangerSB.instantiateViewController(withIdentifier: "scavangerVC") as! ScavangerViewController
            scavangerVC.id = arrOrder[indexPath.row].order_id
            self.navigationController?.pushViewController(scavangerVC, animated: true)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension String {
    func convertDateOrder(dateString: String) -> String? {
        
        let dateFormatter = DateFormatter()
    
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        guard let date = dateFormatter.date(from: dateString) else {
            fatalError()
        }
        
        dateFormatter.dateFormat = "E,d/MMM/yyyy"
        dateFormatter.timeZone = TimeZone.current
        
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        timeFormatter.timeZone = TimeZone(abbreviation: "UTC")
        guard let time = timeFormatter.date(from: dateString) else {
            fatalError()
        }
        
        timeFormatter.dateFormat = "HH:mm"
        timeFormatter.timeZone = TimeZone.current
        
        let newDate = dateFormatter.string(from: date)
        let newTime = timeFormatter.string(from: time)
        return newDate + " at " + newTime
    }
}
