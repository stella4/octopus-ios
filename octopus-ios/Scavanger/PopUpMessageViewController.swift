//
//  PopUpMessageViewController.swift
//  octopus-ios
//
//  Created by Maven on 14/08/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import UIKit

class PopUpMessageViewController: UIViewController {
    static let sharedInstance = PopUpMessageViewController()
    
    @IBOutlet weak var imageAlert: UIImageView!
    @IBOutlet weak var alertDescription: UILabel!
    @IBOutlet weak var alertTitle: UILabel!
    
    var notif = ""
    var notifType = ""
    var titleAlert = ""
    var bodyAlert = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        notif = PopUpMessageViewController.sharedInstance.notifType
        PopUpMessageViewController.sharedInstance.notifType = ""
        
        // Do any additional setup after loading the view.
        if notif == "find_scav" {
            notif = ""
            imageAlert.image = UIImage(named: "looking_scav")
            alertTitle.text = "Mencari Scavenger"
            alertDescription.text = "Tunggu sebentar ya, kami sedang mencarikan Scav untukmu."
        } else if notif == "scav_arrived" {
            notif = ""
            imageAlert.image = UIImage(named: "scav_arrived")
            alertTitle.text = "Scavenger telah tiba"
            alertDescription.text = "Yeay, Scavmu sudah tiba. Yuk, serahkan semua sampahmu"
        } else if notif == "scav_found" {
            notif = ""
            imageAlert.image = UIImage(named: "scav_found")
            alertTitle.text = "Scavenger ditemukan"
            alertDescription.text = "Yeay, Scavmu sudah ditemukan. Hubungi dia sekarang"
        } else if notif == "picking_up" {
            notif = ""
            imageAlert.image = UIImage(named: "picking_up")
            alertTitle.text = "Sedang menjemput sampahmu"
            alertDescription.text = "Tunggu sebentar ya, Scav sedang menjemput sampahmu"
        } else if notif == "order_finish" {
            notif = ""
            imageAlert.image = UIImage(named: "order_done")
            alertTitle.text = "Order telah selesai"
            alertDescription.text = "Yeay, orderanmu sudah selesai!"
        } else if notif == "order_canceled" {
            notif = ""
            imageAlert.image = UIImage(named: "cancel_order")
            alertTitle.text = PopUpMessageViewController.sharedInstance.titleAlert
            alertDescription.text = PopUpMessageViewController.sharedInstance.bodyAlert
        }
    }
    
    @IBAction func dismissAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
