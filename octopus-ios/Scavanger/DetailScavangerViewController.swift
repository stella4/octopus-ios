//
//  DetailScavangerViewController.swift
//  octopus-ios
//
//  Created by Maven on 05/07/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import UIKit
import PullUpController
import Alamofire
import SwiftyJSON

protocol detailscavangerviewcontrollerdelegate {
    func showButton()
    func hideButton()
    func successCancelOrder()
    func showCancelView()
}

class DetailScavangerViewController: PullUpController, RestApiListener {
    func successResponse(call: RequestCall, response: DataResponse<Any>) {
        if (call == .cancelOrder) {
            self.dismiss(animated: true, completion: nil)
            self.wrapperAlert.alpha = 1
        }
    }
    
    func failureResponse(call: RequestCall, response: DataResponse<Any>) {
        print("failed - ",response.debugDescription)
    }
    

    enum InitialState {
        case contracted
        case expanded
    }
    
    var delegate : detailscavangerviewcontrollerdelegate!
    
    var initialState: InitialState = .contracted
    
    var token = UserDefaults.standard.string(forKey: "token")
    var id  = ""
        
    @IBOutlet private weak var visualEffectView: UIVisualEffectView!
    @IBOutlet private weak var searchBoxContainerView: UIView!
    @IBOutlet private weak var searchSeparatorView: UIView! {
        didSet {
            searchSeparatorView.layer.cornerRadius = searchSeparatorView.frame.height/2
        }
    }
    
    @IBOutlet private weak var firstPreviewView: UIView!
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelAddress: UILabel!
    @IBOutlet weak var labelId: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var wrapperScanager: UIView!
    @IBOutlet weak var heightConstantWrapperScavanger: NSLayoutConstraint!
    @IBOutlet weak var wrapperButtonCancel: UIView!
    @IBOutlet weak var heightConstantWrapperButtonCancel: NSLayoutConstraint!
    @IBOutlet weak var labelHeaderDetail: UILabel!
    @IBOutlet weak var cancelView: UIView!
    
    @IBOutlet weak var wrapperAlert: UIView!
    
    var showDetail = 0
    var orderTemp : OrderModel = OrderModel()
    var stickyPoints = 0
    
    var initialPointOffset: CGFloat {
        switch initialState {
        case .contracted:
            return (searchBoxContainerView?.frame.height ?? 0) + safeAreaAdditionalOffset
        case .expanded:
            return pullUpControllerPreferredSize.height
        }
    }
    
    public var portraitSize: CGSize = .zero
    public var landscapeFrame: CGRect = .zero

    private var safeAreaAdditionalOffset: CGFloat {
        hasSafeArea ? 30 : 0
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        portraitSize = CGSize(width: min(UIScreen.main.bounds.width, UIScreen.main.bounds.height),
                              height: firstPreviewView.frame.maxY)
        landscapeFrame = CGRect(x: 0, y: 50, width: 280, height: 300)
        
        showDetail = 0
        
        if !orderTemp.order_is_accepted {
            wrapperScanager.isHidden = true
            wrapperButtonCancel.isHidden = false
            heightConstantWrapperButtonCancel.constant = 68
            heightConstantWrapperScavanger.constant = 0
        } else {
            wrapperScanager.isHidden = false
            heightConstantWrapperScavanger.constant = 92
            wrapperButtonCancel.isHidden = true
            heightConstantWrapperButtonCancel.constant = 0
        }
        
        labelAddress.text = orderTemp.address
        labelDate.text = orderTemp.date
        labelId.text = String(describing: orderTemp.id)
        labelName.text = orderTemp.name
        
        if orderTemp.image == " null" || orderTemp.image == "null" {
            
        } else {
            self.imageView.kf.indicatorType = .activity
            self.imageView.kf.setImage(with: URL(string: self.orderTemp.image))
        }
        
        if !orderTemp.order_is_accepted {
            searchBoxContainerView.backgroundColor = UIColor.greenSea
            labelHeaderDetail.text = "Mencari Scavenger"
        }
        
        if orderTemp.order_is_picked || orderTemp.order_is_accepted {
            labelHeaderDetail.text = "Scavenger sedang menuju ke rumah kamu"
        }
        
        if orderTemp.order_is_arrived {
            labelHeaderDetail.text = "Scavenger sudah sampai rumah kamu"
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func closeWrapperAction(_ sender: Any) {
        wrapperAlert.alpha = 0
        self.performSegue(withIdentifier: "unwindToOrder", sender: self)
    }
    
    @IBAction func yesAction(_ sender: Any) {
        
        cancelView.alpha = 0
     
        let alert = UIAlertController(title: nil, message: "Mohon tunggu", preferredStyle: .alert)

        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating();

        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
        
        Service.cancelOrder(token: token!, id: id, listener: self)
    }
    
    @IBAction func noAction(_ sender: Any) {
        cancelView.alpha = 0
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        view.layer.cornerRadius = 12
    }
    
    override func pullUpControllerWillMove(to stickyPoint: CGFloat) {
        print("will move to \(stickyPoint)")
        stickyPoints = Int(stickyPoint)
        
        if stickyPoint == initialPointOffset {
            self.delegate.hideButton()
        } else {
            self.delegate.showButton()
        }
    }
    
    override func pullUpControllerDidMove(to stickyPoint: CGFloat) {
        print("did move to \(stickyPoint)")
    }
    
    override func pullUpControllerDidDrag(to point: CGFloat) {
//        print("did drag to \(point)")
    }
    
    override var pullUpControllerPreferredSize: CGSize {
        return portraitSize
    }
    
    override var pullUpControllerPreferredLandscapeFrame: CGRect {
        return landscapeFrame
    }
    
    override var pullUpControllerMiddleStickyPoints: [CGFloat] {
        switch initialState {
        case .contracted:
            return [firstPreviewView.frame.maxY + 0]
        case .expanded:
            return [searchBoxContainerView.frame.maxY + safeAreaAdditionalOffset, firstPreviewView.frame.maxY]
        }
    }
    
    override var pullUpControllerBounceOffset: CGFloat {
        return 30
    }
    
    @IBAction func cancelAction(_ sender: Any) {
//        self.delegate.showCancelView()
        CancelOrderViewController.sharedInstance.id = id
        
        self.performSegue(withIdentifier: "cancelOrderSegue", sender: self)
    }
    
    override func pullUpControllerAnimate(action: PullUpController.Action,
                                          withDuration duration: TimeInterval,
                                          animations: @escaping () -> Void,
                                          completion: ((Bool) -> Void)?) {
        switch action {
        case .move:
            UIView.animate(withDuration: 0.3,
                           delay: 0,
                           usingSpringWithDamping: 0.7,
                           initialSpringVelocity: 0,
                           options: .curveEaseInOut,
                           animations: animations,
                           completion: completion)
        default:
            UIView.animate(withDuration: 0.3,
                           animations: animations,
                           completion: completion)
        }
    }

    @IBAction func callAction(_ sender: Any) {
        print("callAction")
        if let url = URL(string: "tel://\(orderTemp.phone)"),
        UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func chatAction(_ sender: Any) {
        let scavangerSB: UIStoryboard = UIStoryboard(name: "Scavanger", bundle: nil)
        let chatVC = scavangerSB.instantiateViewController(withIdentifier: "chatVC") as! ChatViewController
        chatVC.order_id = orderTemp.order_id
        chatVC.sender_id = orderTemp.sender_id
        chatVC.receiver_id = orderTemp.receiver_id
        self.navigationController?.pushViewController(chatVC, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
