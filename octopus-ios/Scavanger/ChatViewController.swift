//
//  ChatViewController.swift
//  octopus-ios
//
//  Created by Maven on 06/05/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

protocol chatviewcontrollerdelegate {
    func refreshViewChat()
}

class ChatViewController: UIViewController, RestApiListener, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textView: UITextView!
    
    var token = UserDefaults.standard.string(forKey: "token")
    var sender_id = ""
    var order_id = ""
    var receiver_id = ""
    var arrChat : [ChatModel] = []
    var id = 0
    
    var delegate : chatviewcontrollerdelegate!
    
    @IBOutlet weak var buttonResign: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleNotification(notification:)), name: NSNotification.Name("PUSHACTION"), object: nil)
        
        Service.getParticularOnGoingOrder(token: token!, id: order_id, listener: self)
        Service.getChat(token: token!, order_id: order_id, listener: self)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        buttonResign.alpha = 0
    }
    
    func refreshViewChat() {
        Service.getParticularOnGoingOrder(token: token!, id: order_id, listener: self)
        Service.getChat(token: token!, order_id: order_id, listener: self)
    }
    
    @objc func handleNotification(notification:NSNotification) {
        let userInfo = notification.userInfo as! [String : AnyObject]
        print("user info chat - ",userInfo)
        
        let dataType : String = userInfo["type"] as? String ?? ""
        print("data type - ",dataType)
        
        if dataType == "chat" {
            Service.getChat(token: token!, order_id: order_id, listener: self)
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        buttonResign.alpha = 1
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    @IBAction func resignKeyboard(_ sender: Any) {
        textView.resignFirstResponder()
        buttonResign.alpha = 0
    }
    
    @IBAction func sendAction(_ sender: Any) {
        print("sender id - ",sender_id)
        Service.postChat(token: token!, message: textView.text!, sender_id: sender_id, order_id: order_id, receiver_id: receiver_id, listener: self)
        textView.text = ""
        textView.resignFirstResponder()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrChat.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if id == arrChat[indexPath.row].sender {
            
           let cell_identifier = "cellCustomer"
            
            let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: cell_identifier)!
            
            let labelMessage : UILabel = cell.viewWithTag(1) as! UILabel
            
            labelMessage.text = arrChat[indexPath.row].message
            
            return cell
        } else {
            let cell_identifier = "cellScavanger"
            
            let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: cell_identifier)!
            
            let labelMessage : UILabel = cell.viewWithTag(1) as! UILabel
            
            labelMessage.text = arrChat[indexPath.row].message
            
            return cell
        }
    }
    
    func successResponse(call: RequestCall, response: DataResponse<Any>) {
        if (call == .orderDetail) {
        let res = response.result.value
        if (res != nil) {
            let json = JSON(res!)
                sender_id = json["sender_id"].stringValue
                receiver_id = json["receiver_id"].stringValue
            }
        }
        
        if (call == .getChat) {
            let res = response.result.value
            if (res != nil) {
                let json = JSON(res!)
                id = json["user"]["id"].intValue
                
                arrChat.removeAll()
                let chatlist = json["chatlist"].arrayValue
                for (index, _) in chatlist.enumerated() {
                    let tempDetails : ChatModel = ChatModel()
                    tempDetails.id = chatlist[index]["id"].intValue
                    tempDetails.message = chatlist[index]["message"].stringValue
                    tempDetails.receiver = chatlist[index]["receiver"].intValue
                    tempDetails.sender = chatlist[index]["sender"].intValue
                    tempDetails.receiver_image = chatlist[index]["receiver_image"].stringValue
                    tempDetails.sender_image = chatlist[index]["sender_image"].stringValue
                    
                    arrChat.append(tempDetails)
                    tableView.reloadData()
                    let indexPath = NSIndexPath(row: self.arrChat.count-1, section: 0)
                    self.tableView.scrollToRow(at: indexPath as IndexPath, at: .bottom, animated: true)
                }
            }
        }
        
        if (call == .postChat) {
            Service.getChat(token: token!, order_id: order_id, listener: self)
        }
    }
    
    func failureResponse(call: RequestCall, response: DataResponse<Any>) {
        
    }
    
    @IBAction func bacAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
