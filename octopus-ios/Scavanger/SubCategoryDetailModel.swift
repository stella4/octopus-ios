//
//  SubCategoryDetailModel.swift
//  octopus-ios
//
//  Created by Maven on 02/05/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import Foundation

class SubCategoryDetailModel: Decodable {
    var brand_name = ""
    var image = ""
}
