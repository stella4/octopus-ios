//
//  PickAndPackModel.swift
//  octopus-ios
//
//  Created by Maven on 24/04/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import Foundation

class PickAndPackModel: Decodable {
    var category = ""
    var subCategory : [PickAndPackSubCategoryModel] = []
}
