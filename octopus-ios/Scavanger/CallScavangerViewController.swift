//
//  CallScavangerViewController.swift
//  octopus-ios
//
//  Created by Maven on 23/04/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import UIKit
import MapKit
import Alamofire
import SwiftyJSON
import GoogleMaps

protocol callscavviewcontrollerdelegate {
    func showWrapper()
}

class CallScavangerViewController: UIViewController, RestApiListener, UIGestureRecognizerDelegate, homeNotifdelegate {

    func showWrapper() {
        wrapperAlertNotif.alpha = 1
    }
    
    func successResponse(call: RequestCall, response: DataResponse<Any>) {
        self.dismiss(animated: true, completion: nil)
        wrapperAlert.alpha = 1
    }

    func failureResponse(call: RequestCall, response: DataResponse<Any>) {
        
    }
    

    @IBOutlet weak var mkMapView: MKMapView!
    @IBOutlet weak var labelAdress: UILabel!
    @IBOutlet weak var textFieldAdress: UITextField!
    @IBOutlet weak var labelQty: UILabel!
    @IBOutlet weak var labelEstimatedDtbm: UILabel!
    @IBOutlet weak var viewButton: UIView!
    @IBOutlet weak var mapView: GMSMapView!
    
    @IBOutlet weak var wrapperAlert: UIView!
    @IBOutlet weak var wrapperAlertNotif: UIView!
    
    var delegate: callscavviewcontrollerdelegate!
    
    var estimatedDtbm = ""
    var address = ""
    var qty = ""
    var showAddress = false
    
    var type = ""
    var open = true
    var openOrder = true
    
    var token = UserDefaults.standard.string(forKey: "token")
    private var currentLocation: CLLocation?
    var locationManager =  CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        wrapperAlert.alpha = 0
        
        viewButton.layer.cornerRadius = 6
        let greenOctopus = UIColor(hexString: "#16BAA2")
        viewButton.layer.borderColor = greenOctopus.cgColor
        viewButton.layer.borderWidth = CGFloat(1.0);
        
        labelQty.text = qty + " Items"
        labelEstimatedDtbm.text = "+"+estimatedDtbm + " DTBM"
        
        let alert = UIAlertController(title: nil, message: "Mohon Tunggu", preferredStyle: .alert)

        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating();

        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
        
        mapView.delegate = self

        // 1
        locationManager.delegate = self

        // 2
        if CLLocationManager.locationServicesEnabled() {
          // 3
          locationManager.requestLocation()

          // 4
          mapView.isMyLocationEnabled = true
          mapView.settings.myLocationButton = true
        } else {
          // 5
          locationManager.requestWhenInUseAuthorization()
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleNotification(notification:)), name: NSNotification.Name("PUSHACTION"), object: nil)
    }
    
    @objc func handleNotification(notification:NSNotification) {
        let userinfo = notification.userInfo as! [String : AnyObject]
        print("user info call scav - ",userinfo)
        
        type = userinfo["type"] as? String ?? ""
        print("data type - ",type)
        
        if let name = userinfo["name"] {
            if openOrder {
                openOrder = false
                if name as! String == "find_scav" {
                    //your code
                    print("call scav active")
                    showWrapper()
                }
            }
        }
    }
    
    @IBAction func closeAlertNotif(_ sender: Any) {
        wrapperAlertNotif.alpha = 0
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    func reverseGeocode(coordinate: CLLocationCoordinate2D) {
      // 1
      let geocoder = GMSGeocoder()

      // 2
      geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
        guard
          let address = response?.firstResult(),
          let lines = address.lines
          else {
            return
        }
        
        // 3
        self.labelAdress.text = lines.joined(separator: "\n")
        if self.showAddress {
            self.dismiss(animated: true, completion: nil)
            self.labelAdress.alpha = 1
        } else {
            self.labelAdress.alpha = 0
        }
        
        // 4
        UIView.animate(withDuration: 0.25) {
          self.view.layoutIfNeeded()
        }
      }
    }
    
    @IBAction func resignKeyboard(_ sender: Any) {
        textFieldAdress.resignFirstResponder()
    }
    
    @IBAction func callScavangerAction(_ sender: Any) {
        HomeViewController.sharedInstance.callScavActive = true
        
        textFieldAdress.resignFirstResponder()
        let alert = UIAlertController(title: nil, message: "Mohon tunggu", preferredStyle: .alert)

        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating();

        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
        
        var optionalAdress = ""
        optionalAdress = textFieldAdress.text!
        Service.callScavanger(token: token!, adress: labelAdress.text!, optional_address: optionalAdress, listener: self)
    }
    
    @IBAction func buttonOkAction(_ sender: Any) {
        wrapperAlert.alpha = 0
        let scavangerSB: UIStoryboard = UIStoryboard(name: "Scavanger", bundle: nil)
        let orderVC = scavangerSB.instantiateViewController(withIdentifier: "orderVC") as! OrderViewController
        self.navigationController?.pushViewController(orderVC, animated: true)
    }
    
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt64()
        Scanner(string: hex).scanHexInt64(&int)
        let a, r, g, b: UInt64
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}

// MARK: - CLLocationManagerDelegate
//1
extension CallScavangerViewController: CLLocationManagerDelegate {
  // 2
  func locationManager(
    _ manager: CLLocationManager,
    didChangeAuthorization status: CLAuthorizationStatus
  ) {
    // 3
    guard status == .authorizedWhenInUse else {
      return
    }
    // 4
    locationManager.requestLocation()

    //5
    mapView.isMyLocationEnabled = true
    mapView.settings.myLocationButton = true
  }

  // 6
  func locationManager(
    _ manager: CLLocationManager,
    didUpdateLocations locations: [CLLocation]) {
    guard let location = locations.first else {
      return
    }

    // 7
    mapView.camera = GMSCameraPosition(
      target: location.coordinate,
      zoom: 15,
      bearing: 0,
      viewingAngle: 0)
    
    showAddress = true
  }

  // 8
  func locationManager(
    _ manager: CLLocationManager,
    didFailWithError error: Error
  ) {
    print(error)
  }
}


// MARK: - GMSMapViewDelegate
extension CallScavangerViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
      reverseGeocode(coordinate: position.target)
        
    }

//    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
//       mapView.clear()
//       let position = CLLocationCoordinate2D(latitude: coordinate.latitude, longitude: coordinate.longitude)
//       let marker = GMSMarker(position: position)
//       marker.map = mapView
//
//        reverseGeocode(coordinate: position)
//        print("reverse 2")
//   }
}


