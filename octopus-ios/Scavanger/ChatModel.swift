//
//  ChatModel.swift
//  octopus-ios
//
//  Created by Maven on 06/05/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import Foundation

class ChatModel: Decodable {
    var id = 0
    var message = ""
    var sender = 0
    var receiver = 0
    var sender_image = ""
    var receiver_image = ""
}
