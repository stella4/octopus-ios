//
//  PickAndPackViewController.swift
//  octopus-ios
//
//  Created by Maven on 24/04/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Kingfisher
import NVActivityIndicatorView

class PickAndPackViewController: UIViewController, RestApiListener, UITableViewDelegate, UITableViewDataSource, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrDetailItem.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellIdentifier: String = "cell"
        let cell = self.detailCollectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath)
        
        let imageView : UIImageView = cell.viewWithTag(2) as! UIImageView
        let labelName : UILabel = cell.viewWithTag(1) as! UILabel
        
        imageView.kf.indicatorType = .activity
        imageView.kf.setImage(with: URL(string: arrDetailItem[indexPath.row].image))
        
        labelName.text = arrDetailItem[indexPath.row].brand_name
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenWidth : CGFloat = collectionView.frame.size.width
        let screenHeight : CGFloat = collectionView.frame.size.height
        let cellWidth : Double = Double(screenWidth * 0.7)
        let cellHeight : Double = Double(screenHeight)
        
        let size : CGSize = CGSize(width: cellWidth, height: cellHeight)

        return size;
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let cellWidth: CGFloat = flowLayout.itemSize.width
        let cellSpacing: CGFloat = flowLayout.minimumInteritemSpacing
        var cellCount = CGFloat(collectionView.numberOfItems(inSection: section))
        var collectionWidth = collectionView.frame.size.width
        var totalWidth: CGFloat
        if #available(iOS 11.0, *) {
            collectionWidth -= collectionView.safeAreaInsets.left + collectionView.safeAreaInsets.right
        }
        repeat {
            totalWidth = cellWidth * cellCount + cellSpacing * (cellCount - 1)
            cellCount -= 1
        } while totalWidth >= collectionWidth

        if (totalWidth > 0) {
            let edgeInset = (collectionWidth - totalWidth) / 2
            return UIEdgeInsets.init(top: flowLayout.sectionInset.top, left: edgeInset, bottom: flowLayout.sectionInset.bottom, right: edgeInset)
        } else {
            return flowLayout.sectionInset
        }
    }

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var heighConstantTableView: NSLayoutConstraint!
    @IBOutlet weak var heighConstantView: NSLayoutConstraint!
    
    @IBOutlet weak var wrapperDetail: UIView!
    @IBOutlet weak var labelDetailName: UILabel!
    
    @IBOutlet weak var wrapperAlert: UIView!
    @IBOutlet weak var labelTitleAlert: UILabel!
    @IBOutlet weak var labelDetailAlert: UILabel!
    @IBOutlet weak var imageAlert: UIImageView!
    
    @IBOutlet weak var detailCollectionView: UICollectionView!
    
    @IBOutlet weak var buttonConfirm: RoundedViewWithoutBorder!
    @IBOutlet weak var labelQty: UILabel!
    @IBOutlet weak var labelDtbm: UILabel!
    
    @IBOutlet weak var bottomSpace: NSLayoutConstraint!
    
    var token = UserDefaults.standard.string(forKey: "token")
    
    var arrPickAndPack : [PickAndPackModel] = []
    var arrOrder : [CartModel] = []
    var activityIndicator : NVActivityIndicatorView!
    var arrDetailItem : [SubCategoryDetailModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let xAxis = self.view.center.x // or use (view.frame.size.width / 2) // or use (faqWebView.frame.size.width / 2)
        let yAxis = self.view.center.y
        let frame = CGRect(x: (xAxis - 50), y: (yAxis - 50), width: 45, height: 45)
        activityIndicator = NVActivityIndicatorView(frame: frame)
        
        activityIndicator.type = .ballBeat // add your type
        activityIndicator.color = UIColor.lightGray // add your color

        self.view.addSubview(activityIndicator) // or use  webView.addSubview(activityIndicator)

        activityIndicator.startAnimating()
        
        buttonConfirm.alpha = 0
        wrapperDetail.alpha = 0
        wrapperAlert.alpha = 0
        Service.getPickPack(token: token!, listener: self)
        // Do any additional setup after loading the view.
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        tapGesture.cancelsTouchesInView = true
        self.view.addGestureRecognizer(tapGesture)
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        buttonConfirm.alpha = 0
        refreshButtonConfirm()
        tableView.reloadData()
    }
    
    @objc func hideKeyboard()
    {
        self.view.endEditing(true)
        tableView.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrPickAndPack.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPickAndPack[section].subCategory.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell_identifier = "cellHeader"

        let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: cell_identifier)!
        
        let labelCategory : UILabel = cell.viewWithTag(1) as! UILabel
        
        labelCategory.text = arrPickAndPack[section].category
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell_identifier = "cell"
        
        let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: cell_identifier, for: indexPath)
        
        let imageView : UIImageView = cell.viewWithTag(1) as! UIImageView
        let labelName : UILabel = cell.viewWithTag(2) as! UILabel
        let labelAmount : UILabel = cell.viewWithTag(3) as! UILabel
        let buttonDetail : CustomButton = cell.viewWithTag(4) as! CustomButton
        
        let buttonMin : CustomButton = cell.viewWithTag(5) as! CustomButton
        let labelQty : CustomTextField = cell.viewWithTag(6) as! CustomTextField
        let buttonPlus : CustomButton = cell.viewWithTag(7) as! CustomButton
        
        let viewMin : UIView = cell.viewWithTag(8) as! UIView
        let viewQty : UIView = cell.viewWithTag(9) as! UIView
        let viewMax : UIView = cell.viewWithTag(10) as! UIView
        
        viewMin.layer.cornerRadius = viewMin.frame.size.width / 2
        viewMax.layer.cornerRadius = viewMax.frame.size.width / 2
        
        labelQty.object = arrPickAndPack[indexPath.section].subCategory[indexPath.row]
        labelQty.addTarget(self, action: #selector(updateQty(_:)), for: .editingChanged)
        
//        labelQty.tag = indexPath.row
        
        buttonDetail.object = arrPickAndPack[indexPath.section].subCategory[indexPath.row]
        buttonDetail.addTarget(self, action: #selector(showWrapperDetail(_:)), for: .touchUpInside)
        
        buttonPlus.object = arrPickAndPack[indexPath.section].subCategory[indexPath.row]
        buttonPlus.addTarget(self, action: #selector(addQty(_:)), for: .touchUpInside)
        
        buttonMin.object = arrPickAndPack[indexPath.section].subCategory[indexPath.row]
        buttonMin.addTarget(self, action: #selector(minQty(_:)), for: .touchUpInside)
        
        let qty = arrPickAndPack[indexPath.section].subCategory[indexPath.row].quantity
        if qty != 0 {
            viewMin.backgroundColor = UIColor(hexString: "#3F97E1")
            viewMax.backgroundColor = UIColor(hexString: "#3F97E1")
            viewQty.backgroundColor = UIColor(hexString: "#3F97E1")
            
            labelQty.textColor = UIColor.white
            buttonMin.setTitleColor(UIColor.white, for: .normal)
            buttonPlus.setTitleColor(UIColor.white, for: .normal)
        } else {
            viewMin.backgroundColor = UIColor(hexString: "#DFDFDF")
            viewMax.backgroundColor = UIColor(hexString: "#DFDFDF")
            viewQty.backgroundColor = UIColor(hexString: "#DFDFDF")
            
            labelQty.textColor = UIColor.darkGray
            buttonMin.setTitleColor(UIColor.darkGray, for: .normal)
            buttonPlus.setTitleColor(UIColor.darkGray, for: .normal)
        }
        
        labelQty.text = String(describing: qty)
        
        imageView.kf.indicatorType = .activity
        imageView.kf.setImage(with: URL(string: arrPickAndPack[indexPath.section].subCategory[indexPath.row].image))
        labelName.text = arrPickAndPack[indexPath.section].subCategory[indexPath.row].name
        let amount = arrPickAndPack[indexPath.section].subCategory[indexPath.row].amount
        labelAmount.text = "\(amount) DTBM"
        
        return cell
    }
    
    @objc func showWrapperDetail(_ sender: CustomButton) {
        let dataCart : PickAndPackSubCategoryModel = sender.object! as! PickAndPackSubCategoryModel
        
        labelDetailName.text = dataCart.name
        arrDetailItem = dataCart.detail
        
        detailCollectionView.reloadData()
        wrapperDetail.alpha = 1
    }
    
    @IBAction func hideWrapper(_ sender: Any) {
        wrapperDetail.alpha = 0
        wrapperAlert.alpha = 0
    }
    
    @objc func resignKeyboard(_ sender: CustomTextField) {
        
        // this index is equal to the row of the textview
    }
    
    @objc func updateQty(_ sender: CustomTextField) {
        let dataCart : PickAndPackSubCategoryModel = sender.object! as! PickAndPackSubCategoryModel
        
        var indexKetemu = -1
        
        if sender.text != "" {
            for i in 0..<arrOrder.count {
                let arrTemp = arrOrder[i]
                if arrTemp.id == dataCart.id {
                    indexKetemu = 1
                }
            }
            
            if indexKetemu == -1 {
                let arrTemp : CartModel = CartModel()
                arrTemp.id = dataCart.id
                arrTemp.qty = Int(sender.text!)!
                arrTemp.dtbm = dataCart.amount
                arrOrder.append(arrTemp)
            } else {
                if let index = arrOrder.firstIndex(where: {$0.id == dataCart.id}) {
                    arrOrder[index].qty = Int(sender.text!)!
                    arrOrder[index].dtbm = dataCart.amount * arrOrder[index].qty
                }
            }
            
            for i in 0..<arrPickAndPack.count {
                let tempArr = arrPickAndPack[i]
                if let index = tempArr.subCategory.firstIndex(where: {$0.id == dataCart.id}) {
                    arrPickAndPack[i].subCategory[index].quantity = Int(sender.text!)!
                }
            }
        }
        
        
        refreshButtonConfirm()
        
//        tableView.reloadData()
    }
    
    @objc func addQty(_ sender: CustomButton) {
        let dataCart : PickAndPackSubCategoryModel = sender.object! as! PickAndPackSubCategoryModel
        
        var indexKetemu = -1
        
        for i in 0..<arrOrder.count {
            let arrTemp = arrOrder[i]
            if arrTemp.id == dataCart.id {
                indexKetemu = 1
            }
        }
        
        if indexKetemu == -1 {
            let arrTemp : CartModel = CartModel()
            arrTemp.id = dataCart.id
            arrTemp.qty = dataCart.quantity + 1
            arrTemp.dtbm = dataCart.amount
            arrOrder.append(arrTemp)
        } else {
            if let index = arrOrder.firstIndex(where: {$0.id == dataCart.id}) {
                arrOrder[index].qty = dataCart.quantity + 1
                arrOrder[index].dtbm = dataCart.amount * arrOrder[index].qty
            }
        }
        
        for i in 0..<arrPickAndPack.count {
            let tempArr = arrPickAndPack[i]
            if let index = tempArr.subCategory.firstIndex(where: {$0.id == dataCart.id}) {
                arrPickAndPack[i].subCategory[index].quantity = dataCart.quantity + 1
            }
        }
        
        refreshButtonConfirm()
        
        tableView.reloadData()
    }
    
    @objc func minQty(_ sender: CustomButton) {
        let dataCart : PickAndPackSubCategoryModel = sender.object! as! PickAndPackSubCategoryModel
        
        if dataCart.quantity > 0 {
            if let index = arrOrder.firstIndex(where: {$0.id == dataCart.id}) {
                arrOrder[index].qty = dataCart.quantity - 1
                arrOrder[index].dtbm = dataCart.amount * arrOrder[index].qty
            }
            
            for i in 0..<arrPickAndPack.count {
                let tempArr = arrPickAndPack[i]
                if let index = tempArr.subCategory.firstIndex(where: {$0.id == dataCart.id}) {
                    arrPickAndPack[i].subCategory[index].quantity = dataCart.quantity - 1
                }
            }
        }
        
        refreshButtonConfirm()
        
        tableView.reloadData()
    }
    
    func refreshButtonConfirm() {
        if arrOrder.count != 0 {
            buttonConfirm.alpha = 1
            
            var qty = 0
            var dtbm = 0
            for i in 0..<arrOrder.count {
                qty = qty+arrOrder[i].qty
                dtbm = dtbm+arrOrder[i].dtbm
            }
            
            labelQty.text = "\(qty) items"
            labelDtbm.text = "+\(dtbm) DTBM"
            labelQty.alpha = 1
            labelDtbm.alpha = 1
        } else {
            buttonConfirm.alpha = 0
            labelQty.alpha = 0
            labelDtbm.alpha = 0
        }
    }
    
    @IBAction func addToCartAction(_ sender: Any) {
        var qty = 0
        for i in 0..<arrOrder.count {
            qty = qty+arrOrder[i].qty
        }
        
        if qty < 10 {
            if qty == 0 {
                imageAlert.image = UIImage(named: "fillItem")
                labelTitleAlert.text = "Oops, kamu belum mengisi daftar barang"
                labelDetailAlert.text = "Untuk melakukan order, daftar barangmu tidak boleh kosong ya"
            } else {
                imageAlert.image = UIImage(named: "minItem")
                labelTitleAlert.text = "Order kamu masih kurang dari jumlah order minimum yaitu 10"
                labelDetailAlert.text = ""
            }
            wrapperAlert.alpha = 1
        } else {
            wrapperAlert.alpha = 0
            
            var items = [[String: Any]]()
            for ol in arrOrder {
                items.append([
                    "id": ol.id,
                    "qty": ol.qty
                    ])
            }
                        
            let param : [String:Any] = ["item":items]
            let json = JSON(param)
            
            let representation = json.rawString([.castNilToNSNull: true])
            
            let JsonString = representation! //  Ex.:-  "{\"action\": \"fetch-data\"}"
            var request = URLRequest(url: URL(string: "http://dev.api.octopus.co.id/api/user/add-to-cart/")!)
            request.httpMethod = HTTPMethod.post.rawValue
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("JWT "+token!, forHTTPHeaderField: "Authorization")

            let pjson = JsonString
            let data = (pjson.data(using: .utf8))! as Data

            request.httpBody = data
            
            print(data)

            Alamofire.request(request).responseJSON { (response) in

                switch response.result {
                case .success:
                    print(response)
                    //callScavangerVC
                    let json = JSON(response.result.value!)
                    let scavangerSB: UIStoryboard = UIStoryboard(name: "Scavanger", bundle: nil)
                    let callScavangerVC = scavangerSB.instantiateViewController(withIdentifier: "callScavangerVC") as! CallScavangerViewController
                   
                    var qty = 0
                    for i in 0..<self.arrOrder.count {
                        qty = qty+self.arrOrder[i].qty
                    }
                    
                    callScavangerVC.qty = "\(qty)"
                    callScavangerVC.estimatedDtbm = json["estimated_dtbm"].stringValue
                    self.navigationController?.pushViewController(callScavangerVC, animated: true)
                    break
                case .failure(let error):

                    print(error)
                }

            }
        }
    }
    
    func stringify(json: Any, prettyPrinted: Bool = false) -> String {
        var options: JSONSerialization.WritingOptions = []
        if prettyPrinted {
            options = JSONSerialization.WritingOptions.prettyPrinted
        }
        
        do {
            let data = try JSONSerialization.data(withJSONObject: json, options: options)
            if let string = String(data: data, encoding: String.Encoding.utf8) {
                return string
            }
        } catch {
            print(error)
        }
        
        return ""
    }
    
    func successResponse(call: RequestCall, response: DataResponse<Any>) {
        let res = response.result.value
        if (res != nil) {
            let json = JSON(res!)
            var result: PickAndPackModel
            arrPickAndPack = []
            
            for (index, _) in json.enumerated() {
                result = PickAndPackModel()
                result.category = json[index]["name"].stringValue
                var subCategory : PickAndPackSubCategoryModel
                
                let arrSubCategory = json[index]["user_sub_category"].arrayValue
                for (index, _) in arrSubCategory.enumerated() {
                    subCategory = PickAndPackSubCategoryModel()
                    subCategory.amount = arrSubCategory[index]["amount"].intValue
                    subCategory.description = arrSubCategory[index]["description"].stringValue
                    subCategory.id = arrSubCategory[index]["id"].stringValue
                    subCategory.image = arrSubCategory[index]["image"].stringValue
                    subCategory.name = arrSubCategory[index]["name"].stringValue
                    subCategory.quantity = arrSubCategory[index]["quantity"].intValue
                    
                    var detail : SubCategoryDetailModel
                    let arrDetail = arrSubCategory[index]["brand"].arrayValue
                    for (index, _) in arrDetail.enumerated() {
                        detail = SubCategoryDetailModel()
                        detail.brand_name = arrDetail[index]["brand_name"].stringValue
                        detail.image = arrDetail[index]["image"].stringValue
                        subCategory.detail.append(detail)
                    }
                    
                    result.subCategory.append(subCategory)
                }
                
                arrPickAndPack.append(result)
                
                activityIndicator.stopAnimating()
                activityIndicator.removeFromSuperview()
                
                let footerheaderheight = arrPickAndPack.count * 45

                var countDetail = 0
                var cellheight = 0
                for i in 0..<arrPickAndPack.count {
                    let data_array = arrPickAndPack[i]
                    countDetail = countDetail + data_array.subCategory.count
                    cellheight = cellheight + ((data_array.subCategory.count) * 130)
                }
                heighConstantTableView.constant = CGFloat(footerheaderheight + cellheight) + 80
                
                tableView.reloadData()
                
                heighConstantView.constant = CGFloat(footerheaderheight + cellheight) + 60 + 80
            }
        }
    }
    
    func failureResponse(call: RequestCall, response: DataResponse<Any>) {
        
    }
    
    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
