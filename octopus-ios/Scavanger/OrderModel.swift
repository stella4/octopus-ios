//
//  OrderModel.swift
//  octopus-ios
//
//  Created by Maven on 06/05/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import Foundation

class OrderModel : Decodable {
    var address = ""
    var lat = ""
    var long = ""
    var id = 0
    var image = ""
    var name = ""
    var order_is_accepted = false
    var order_is_arrived = false
    var order_is_picked = false
    var optional_address = ""
    var order_total = ""
    var order_id = ""
    var phone = ""
    var qr_code = ""
    var receiver_id = ""
    var scavanger_address_lat = ""
    var scavanger_address_long = ""
    var sender_id = ""
    var total = ""
    var date = ""
    var is_cancel = false
}
