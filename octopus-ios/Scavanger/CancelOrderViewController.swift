//
//  CancelOrderViewController.swift
//  octopus-ios
//
//  Created by Maven on 14/08/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class CancelOrderViewController: UIViewController, RestApiListener {
    static let sharedInstance = CancelOrderViewController()
    
    @IBOutlet weak var wrapperAlert: UIView!
    
    var token = UserDefaults.standard.string(forKey: "token")
    var id = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func yesAction(_ sender: Any) {
        let alert = UIAlertController(title: nil, message: "Mohon tunggu", preferredStyle: .alert)

        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating();

        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
        
        Service.cancelOrder(token: token!, id: CancelOrderViewController.sharedInstance.id, listener: self)
    }
    
    @IBAction func noAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func closeWrapperAction(_ sender: Any) {
        wrapperAlert.alpha = 0
        self.performSegue(withIdentifier: "unwindToOrder", sender: self)
    }
    
    func successResponse(call: RequestCall, response: DataResponse<Any>) {
        if (call == .cancelOrder) {
            self.dismiss(animated: true, completion: nil)
            wrapperAlert.alpha = 1
        }
    }
    
    func failureResponse(call: RequestCall, response: DataResponse<Any>) {
        print("failed - ",response.debugDescription)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
