//
//  ScavangerViewController.swift
//  octopus-ios
//
//  Created by Maven on 06/05/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MapKit
import CoreLocation
import Kingfisher
import NVActivityIndicatorView
import GoogleMaps
import FirebaseDatabase
import Firebase

protocol scavangerviewcontrollerdelegate {
//    buat ambil kuasa
    func showButton()
    func successCancelOrder()
    func hideButton()
    func showCancelView()
}

class ScavangerViewController: UIViewController, RestApiListener, MKMapViewDelegate, CLLocationManagerDelegate, detailscavangerviewcontrollerdelegate {

    @IBOutlet weak var mkMapView: MKMapView!
    @IBOutlet weak var heightConstantViewDetail: NSLayoutConstraint!
    @IBOutlet weak var wrapperDetail: UIView!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelAddress: UILabel!
    @IBOutlet weak var labelId: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var wrapperScanager: UIView!
    @IBOutlet weak var heightConstantWrapperScavanger: NSLayoutConstraint!
    @IBOutlet weak var wrapperButtonCancel: UIView!
    @IBOutlet weak var heightConstantWrapperButtonCancel: NSLayoutConstraint!
    @IBOutlet weak var viewHeaderDetail: UIView!
    @IBOutlet weak var labelHeaderDetail: UILabel!
    @IBOutlet weak var wrapperAlert: UIView!
    @IBOutlet weak var imageAlert: UIImageView!
    @IBOutlet weak var alertTitle: UILabel!
    @IBOutlet weak var alertBody: UILabel!
    @IBOutlet weak var mapView: GMSMapView!
    
    @IBOutlet weak var cancelView: UIView!
    
    var notif = ""
    var titleAlert = ""
    var bodyAlert = ""
    
    var token = UserDefaults.standard.string(forKey: "token")
    var id  = ""
    var showDetail = 0
    var orderTemp : OrderModel = OrderModel()
    var lat : Double?
    var long : Double?
    
    var delegate : scavangerviewcontrollerdelegate!
    
    var ref: DatabaseReference!
    var databaseHandler: DatabaseHandle!
    
    var newPin = MKPointAnnotation()
    private var currentLocation: CLLocation?
    var locationManager =  CLLocationManager()
    
    var activityIndicator : NVActivityIndicatorView!
    
    private var originalPullUpControllerViewSize: CGSize = .zero
    
    private func makeSearchViewControllerIfNeeded() -> DetailScavangerViewController {
        let currentPullUpController = children
            .filter({ $0 is DetailScavangerViewController })
            .first as? DetailScavangerViewController
        let pullUpController: DetailScavangerViewController = currentPullUpController ?? UIStoryboard(name: "Scavanger",bundle: nil).instantiateViewController(withIdentifier: "DetailScavangerViewController") as! DetailScavangerViewController
        pullUpController.initialState = .contracted
        pullUpController.orderTemp = orderTemp
        pullUpController.id = id
        if originalPullUpControllerViewSize == .zero {
            originalPullUpControllerViewSize = pullUpController.view.bounds.size
        }
        pullUpController.delegate = self
        
        return pullUpController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        listenDatabase()
        
        
        
//        if notif == "scav_arrived" {
//            notif = ""
//            imageAlert.image = UIImage(named: "scav_arrived")
//            wrapperAlert.alpha = 1
//            alertTitle.text = "Scavenger telah tiba"
//            alertBody.text = "Yeay, Scavmu sudah tiba. Yuk, serahkan semua sampahmu"
//        } else if notif == "picking_up" {
//            notif = ""
//            imageAlert.image = UIImage(named: "picking_up")
//            wrapperAlert.alpha = 1
//            alertTitle.text = "Sedang menjemput sampahmu"
//            alertBody.text = "Tunggu sebentar ya, Scav sedang menjemput sampahmu"
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        refreshView()
    }
    
    func refreshView() {
        view1.alpha = 0
        showDetail = 0
        wrapperDetail.alpha = 0
        heightConstantWrapperScavanger.constant = 0
        heightConstantViewDetail.constant = 0
        hideButton()
        
        Service.getParticularOnGoingOrder(token: token!, id: id, listener: self)
    }
    
    private func addPullUpController(animated: Bool) {
        let pullUpController = makeSearchViewControllerIfNeeded()
        _ = pullUpController.view // call pullUpController.viewDidLoad()
        addPullUpController(pullUpController,
                            initialStickyPointOffset: pullUpController.initialPointOffset,
                            animated: animated)

    }
    
    func showCancelView() {
        cancelView.alpha = 1
    }
    
    @IBAction func yesAction(_ sender: Any) {
       cancelView.alpha = 0
    
       let alert = UIAlertController(title: nil, message: "Mohon tunggu", preferredStyle: .alert)

       let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
       loadingIndicator.hidesWhenStopped = true
       loadingIndicator.style = UIActivityIndicatorView.Style.gray
       loadingIndicator.startAnimating();

       alert.view.addSubview(loadingIndicator)
       present(alert, animated: true, completion: nil)
       
       Service.cancelOrder(token: token!, id: id, listener: self)
   }
   
   @IBAction func noAction(_ sender: Any) {
       cancelView.alpha = 0
   }
    
    func listenDatabase() {
//        if Auth.auth().currentUser != nil {
//          // User is signed in.
//          print("User is signed in")
//        } else {
//          // No user is signed in.
//          print("No user is signed in")
//        }
    
//        let userID = Auth.auth().currentUser?.uid
//        print("id - ",id," - ",userID!)
//        ref.child("scavenger").child(userID!).observeSingleEvent(of: .value, with: { (snapshot) in
//            let value = snapshot.value as? NSDictionary
//            print("latitude - ",value?["latitude"] as? String ?? "")
//        })
//
//        ref = Database.database().reference().child("scavenger").child(id)
//
//        ref.observeSingleEvent(of: .childAdded, with: { (snapshot) in
//             if let userDict = snapshot.value as? [String:Any] {
//                print("here userDict")
//                  //Do not cast print it directly may be score is Int not string
//                print("here 1 - ",userDict["latitude"] as! String)
//             }
//        })
    }
    
    @IBAction func callAction(_ sender: Any) {
        if let url = URL(string: "tel://\(orderTemp.phone)"),
        UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func chatAction(_ sender: Any) {
        let scavangerSB: UIStoryboard = UIStoryboard(name: "Scavanger", bundle: nil)
        let chatVC = scavangerSB.instantiateViewController(withIdentifier: "chatVC") as! ChatViewController
        chatVC.order_id = orderTemp.order_id
        chatVC.sender_id = orderTemp.sender_id
        chatVC.receiver_id = orderTemp.receiver_id
        self.navigationController?.pushViewController(chatVC, animated: true)
    }
    
    @IBAction func buttonScavangerAction(_ sender: Any) {
        if showDetail == 0 {
            view1.alpha = 1
            showDetail = 1
            wrapperDetail.alpha = 1
            
            if !orderTemp.order_is_accepted {
                heightConstantViewDetail.constant = 200
                wrapperScanager.isHidden = true
                wrapperButtonCancel.isHidden = false
                heightConstantWrapperButtonCancel.constant = 60
                heightConstantWrapperScavanger.constant = 0
            } else {
                wrapperScanager.isHidden = false
                heightConstantWrapperScavanger.constant = 121
                heightConstantViewDetail.constant = 255
                wrapperButtonCancel.isHidden = true
                heightConstantWrapperButtonCancel.constant = 0
            }
        } else {
            view1.alpha = 0
            showDetail = 0
            wrapperDetail.alpha = 0
            heightConstantWrapperScavanger.constant = 0
            heightConstantViewDetail.constant = 0
        }
    }
    
    func showButton() {
        showDetail = 1
        view1.alpha = 1
    }
    
    func hideButton() {
        showDetail = 0
        view1.alpha = 0
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func cancelAction(_ sender: Any) {
        let alert = UIAlertController(title: nil, message: "Mohon tunggu", preferredStyle: .alert)

        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating();

        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
        
        Service.cancelOrder(token: token!, id: id, listener: self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        let pullUpController = makeSearchViewControllerIfNeeded()
        _ = pullUpController.view
        removePullUpController(pullUpController, animated: true)
    }
    
    func successResponse(call: RequestCall, response: DataResponse<Any>) {
        if (call == .orderDetail) {
            let res = response.result.value
            if (res != nil) {
                let json = JSON(res!)
                orderTemp.address = json["address"].stringValue
                orderTemp.lat = json["latitude"].stringValue
                orderTemp.long = json["longitude"].stringValue
                orderTemp.id = json["id"].intValue
                orderTemp.name = json["name"].stringValue
                orderTemp.image = json["image"].stringValue
                orderTemp.phone = json["phone"].stringValue
                orderTemp.order_is_accepted = json["order"]["is_accepted"].boolValue
                orderTemp.order_is_arrived = json["order"]["is_arrived"].boolValue
                orderTemp.order_is_picked = json["order"]["is_picked"].boolValue
                orderTemp.optional_address = json["order"]["optional_address"].stringValue
                orderTemp.order_id = json["order_id"].stringValue
                let date = json["updated_at"].stringValue
                orderTemp.date = date.convertDateOrder(dateString: date)!
                orderTemp.scavanger_address_lat = json["scavanger_address"]["latitude"].stringValue
                orderTemp.scavanger_address_long = json["scavanger_address"]["longitude"].stringValue
                orderTemp.sender_id = json["sender_id"].stringValue
                orderTemp.receiver_id = json["receiver_id"].stringValue
                
                self.lat = Double(orderTemp.scavanger_address_lat)
                self.long = Double(orderTemp.scavanger_address_long)
                
                updateLocation(lat: json["latitude"].doubleValue, long: json["longitude"].doubleValue)
                
                addPullUpController(animated: true)
            }
        }
        
        if (call == .cancelOrder) {
            self.dismiss(animated: true, completion: nil)
            wrapperAlert.alpha = 1
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func successCancelOrder() {
        self.dismiss(animated: true, completion: nil)
        wrapperAlert.alpha = 1
    }
    
    func updateLocation(lat: Double, long: Double) {
        let position = CLLocationCoordinate2D(latitude: lat, longitude: long)
        let marker = GMSMarker(position: position)
        marker.icon = UIImage(named: "pin_user")
        marker.setIconSize(scaledToSize: .init(width: 40, height: 53))
        marker.map = self.mapView
        
        self.mapView.camera = GMSCameraPosition(
        target: position,
        zoom: 15,
        bearing: 0,
        viewingAngle: 0)
            
        
    }
    
    @IBAction func okMengertiAction(_ sender: Any) {
        wrapperAlert.alpha = 0
        refreshView()
    }
    
    func failureResponse(call: RequestCall, response: DataResponse<Any>) {
        self.dismiss(animated: true, completion: nil)
        self.showToast(message: "Gagal membatalkan orderan.")
    }
    
}

extension GMSMarker {
    func setIconSize(scaledToSize newSize: CGSize) {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        icon?.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        icon = newImage
    }
}
