//
//  ProfileModel.swift
//  octopus-ios
//
//  Created by Maven on 25/05/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import Foundation

class ProfileModel : Decodable {
    var name = ""
    var email = ""
    var photo = ""
    var phoneNumber = ""
    var province = ""
    var city = ""
}
