//
//  ProfileViewController.swift
//  octopus-ios
//
//  Created by Maven on 04/05/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import UIKit
import iOSDropDown
import Alamofire
import SwiftyJSON

class ProfileViewController: UIViewController, RestApiListener, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var txtFieldName: UITextField!
    @IBOutlet weak var txtFieldPhoneNumber: UITextField!
    @IBOutlet weak var txtFieldEmail: UITextField!
    @IBOutlet weak var provinceDropdown: DropDown!
    @IBOutlet weak var cityDropwosn: DropDown!
    
    @IBOutlet weak var labelEnterName: UILabel!
    @IBOutlet weak var labelEnterPhone: UILabel!
    @IBOutlet weak var labelEnterEmail: UILabel!
    @IBOutlet weak var labelSelectProvince: UILabel!
    @IBOutlet weak var labelSelectCity: UILabel!
    
    @IBOutlet weak var wrapperView: UIView!
    
    @IBOutlet weak var wrapperAlert: UIView!
    
    var arrProvince : [String] = []
    var province = ""
    var arrCity : [String] = []
    var city = ""
    var phoneValid = false
    var nameValid = false
    var emailValid = false
    var cityValid = false
    var provinceValid = false
    var deviceToken = ""
    var theImage: UIImage?
    var imageURL: NSURL?
    
    var window: UIWindow?
    let mainSB: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    var token = UserDefaults.standard.string(forKey: "token")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        closeKeyboard()
        
        txtFieldName.setUnderLine()
        txtFieldPhoneNumber.setUnderLine()
        txtFieldEmail.setUnderLine()
        provinceDropdown.setUnderLine()
        cityDropwosn.setUnderLine()
        
        cityDropwosn.isSearchEnable = false
        Service.getProvince(listener: self)
        Service.getProfile(token: token!, listener: self)
        
        txtFieldName.addTarget(self, action: #selector(textFieldDidChangeName(textField:)), for: .editingChanged)
        txtFieldEmail.addTarget(self, action: #selector(textFieldDidChangeEmail(textField:)), for: .editingChanged)
        txtFieldPhoneNumber.addTarget(self, action: #selector(textFieldDidChangePhone(textField:)), for: .editingChanged)
    }
    
    @objc func textFieldDidChangeName(textField: UITextField){
        if textField.text!.count == 0 {
            labelEnterName.isHidden = false
            nameValid = false
        } else {
            labelEnterName.isHidden = true
            nameValid = true
        }
    }
    
    @objc func textFieldDidChangeEmail(textField: UITextField){
        if !textField.text!.isValidEmail {
            labelEnterEmail.isHidden = false
            emailValid = false
            labelEnterEmail.text = "Email Kamu Salah."
        } else if textField.text?.count == 0 {
            labelEnterEmail.isHidden = false
            emailValid = false
            labelEnterEmail.text = "Masukkan Email Kamu."
        } else {
            labelEnterEmail.isHidden = true
            emailValid = true
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var maxLength = 100         // set your need
        
        if textField.tag == 1 {
            maxLength = 14
        }
        
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        
        return newString.length <= maxLength
    }
    
    @objc func textFieldDidChangePhone(textField: UITextField){
        checkPhone(phone: textField.text!)
    }
    
    func checkPhone(phone: String) {
        if phone.count <= 10 || phone.isContainsLetters {
            labelEnterPhone.isHidden = false
            labelEnterPhone.text = "Nomor Telepon Kamu Salah."
            phoneValid = false
        } else if phone.count == 0 {
            phoneValid = false
            labelEnterPhone.isHidden = false
            labelEnterEmail.text = "Masukkan Nomor Telepon Kamu."
        } else if phone.count >= 10 &&  phone[String.Index.init(utf16Offset: 0, in: phone)] != "0" {
            labelEnterPhone.text = "Nomor Telepon dimulai angka 0."
            labelEnterPhone.isHidden = false
            phoneValid = false
        } else {
            labelEnterPhone.isHidden = true
            phoneValid = true
        }
    }
    
    @IBAction func resignKeyboard(_ sender: Any) {
        closeKeyboard()
    }
    
    func closeKeyboard() {
        txtFieldName.resignFirstResponder()
        txtFieldPhoneNumber.resignFirstResponder()
        txtFieldEmail.resignFirstResponder()
        cityDropwosn.resignFirstResponder()
        provinceDropdown.resignFirstResponder()
    }

    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func cameraAction(_ sender: Any) {
        let actionSheetController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        // Create and add the Cancel action
        let cancelAction = UIAlertAction(title: "Batal", style: .cancel) { action -> Void in
            // Just dismiss the action sheet
        }
        actionSheetController.addAction(cancelAction)
        
        // Create and add first option action
        let cameraAction = UIAlertAction(title: "Ambil Foto", style: .default) { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = .camera;
                imagePicker.allowsEditing = false
                self.present(imagePicker, animated: true, completion: nil)
            }
        }
        actionSheetController.addAction(cameraAction)
        
        // Create and add a second option action
        let galleryAction = UIAlertAction(title: "Ambil dari galeri", style: .default) { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = .photoLibrary;
                imagePicker.allowsEditing = true
                self.present(imagePicker, animated: true, completion: nil)
            }
        }
        actionSheetController.addAction(galleryAction)
        
        // We need to provide a popover sourceView when using it on iPad
        actionSheetController.popoverPresentationController?.sourceView = sender as? UIView
        
        // Present the AlertController
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        self.imageView.image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage
        theImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        
        
            if (picker.sourceType == UIImagePickerController.SourceType.camera) {
                
                let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
                let imgName = UUID().uuidString + ".jpeg"

                let documentDirectory = NSTemporaryDirectory()
                let localPath = documentDirectory.appending(imgName)
                
                let data = image!.jpegData(compressionQuality: 0.3)! as NSData
                data.write(toFile: localPath, atomically: true)
                
                imageURL = URL.init(fileURLWithPath: localPath) as NSURL
                
            } else {
                imageURL = (info[UIImagePickerController.InfoKey.imageURL] as! NSURL)
                theImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage
            }
        
        
        dismiss(animated: true, completion: nil)
        
        let alert = UIAlertController(title: nil, message: "Mengupload Data", preferredStyle: .alert)

        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating();

        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
        
        uploadPhoto()
    }
    
    func uploadPhoto() {
        
        let url = "http://dev.api.octopus.co.id/api/user/image-upload"
        
        let filename = self.imageURL?.absoluteString
        
        let imageData = theImage!.jpegData(compressionQuality: 0.5)!
        print("image - ",imageData," - ",filename!)
        
        Alamofire.upload(multipartFormData: { (multipart) in
            multipart.append(imageData, withName: "image", fileName: filename!, mimeType: "image/jpeg")
        },
             usingThreshold: UInt64.init(),
             to: url,
             method: .post,
             headers: ["Authorization": "JWT "+token!],
      
             encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { (response) in
                        print(response)
                        print("success update photo")
                        Service.getProfile(token: self.token!, listener: self)
                        self.dismiss(animated: true, completion: nil)
                        
                        self.showToast(message: "Foto profil berhasil diperbaharui.")
                    }
                    
                case .failure(let encodingError):
                    print(encodingError)
                    self.dismiss(animated: true, completion: nil)
                    self.showToast(message: "Gagal mengupload foto, silahkan coba lagi.")
                }
        })
    }
    
    @IBAction func submitAction(_ sender: Any) {
        closeKeyboard()
        
        checkPhone(phone: txtFieldPhoneNumber.text!)
        
        if txtFieldName.text == "" || txtFieldPhoneNumber.text == "" || txtFieldEmail.text == "" || provinceDropdown.text == "" || cityDropwosn.text == "" || !phoneValid || !txtFieldEmail.text!.isValidEmail {
            if txtFieldName.text == "" {
                labelEnterName.isHidden = false
            }
            
            if txtFieldPhoneNumber.text == "" {
                labelEnterPhone.isHidden = false
            }
            
            if txtFieldEmail.text == "" {
                labelEnterName.isHidden = false
            }
            
            if provinceDropdown.text == "" {
                labelSelectProvince.isHidden = false
            }
            
            if cityDropwosn.text == "" {
                labelSelectCity.isHidden = false
            }
        
            if !phoneValid {
                labelEnterPhone.isHidden = false
            }
            
            if !emailValid {
                labelEnterEmail.isHidden = false
            }
            
        } else {
            labelSelectCity.isHidden = true
            labelSelectProvince.isHidden = true
            labelEnterEmail.isHidden = true
            labelEnterPhone.isHidden = true
            labelEnterName.isHidden = true
            
//            self.showToast(message: "Under Maintenance")
            let alert = UIAlertController(title: nil, message: "Mohon tunggu", preferredStyle: .alert)

            let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
            loadingIndicator.hidesWhenStopped = true
            loadingIndicator.style = UIActivityIndicatorView.Style.gray
            loadingIndicator.startAnimating();

            alert.view.addSubview(loadingIndicator)
            present(alert, animated: true, completion: nil)
            
            Service.updateProfile(token: token!, name: txtFieldName.text!, email: txtFieldEmail.text!, province: provinceDropdown.text!, city: cityDropwosn.text!, phone: txtFieldPhoneNumber.text!, listener: self)
        }
    }
    
    @IBAction func logoutAction(_ sender: Any) {
        wrapperView.alpha = 1
    }
    
    @IBAction func yesAction(_ sender: Any) {
        Service.logout(token: token!, listener: self)
    }
    
    @IBAction func noAction(_ sender: Any) {
        wrapperView.alpha = 0
    }
    
    func successResponse(call: RequestCall, response: DataResponse<Any>) {
        if (call == .profile) {
            let res = response.result.value
            if (res != nil) {
                let json = JSON(res!)
                txtFieldName.text = json["name"].stringValue
                txtFieldEmail.text = json["email"].stringValue
                txtFieldPhoneNumber.text = json["phone"].stringValue
                provinceDropdown.text = json["user_address"]["province"].stringValue
                self.province = json["user_address"]["province"].stringValue
                Service.getCity(name: json["user_address"]["province"].stringValue, listener: self)
                cityDropwosn.text = json["user_address"]["city"].stringValue
                
                let url = json["image"].stringValue
                imageView.kf.indicatorType = .activity
                imageView.kf.setImage(with: URL(string: url))
            }
        }
        
        if (call == .logout) {
            wrapperView.alpha = 0
            UserDefaults().removeObject(forKey: "token")
            let launchNav = mainSB.instantiateViewController(withIdentifier: "launchNav") as! UINavigationController
            UIApplication.shared.keyWindow?.rootViewController = launchNav
        }
        
        if (call == .getProvince) {
            let res = response.result.value
            if (res != nil) {
                let json = JSON(res!)
                arrProvince = []
                for (index, _) in json.enumerated() {
                    let name = json[index]["name"].stringValue
                    arrProvince.append(name)
                }
                DispatchQueue.main.async {
                    self.provinceDropdown.optionArray = self.arrProvince
                    self.provinceDropdown.isSearchEnable = false
                    self.provinceDropdown.didSelect { (selectedText, index, id) in
                        self.province = ""
                        self.getFilteredCity(selectedProvince: selectedText)
                    }
                }
            }
        }
        
        if (call == .getCity) {
            let res = response.result.value
            if (res != nil) {
                let json = JSON(res!)
                arrCity = []
                for (index, _) in json.enumerated() {
                    let name = json[index]["name"].stringValue
                    arrCity.append(name)
                }
                DispatchQueue.main.async {
                    self.cityDropwosn.optionArray = self.arrCity
                    self.cityDropwosn.isSearchEnable = false
                    if self.province == "" {
                        self.cityDropwosn.text = ""
                        self.cityDropwosn.placeholder = "Pilih Kota"
                    }
                    self.cityDropwosn.didSelect { (selectedText, index, id) in
                        self.city = selectedText
                    }
                }
            }
        }
        
        if (call == .updateProfile) {
            self.dismiss(animated: true, completion: nil)
            wrapperAlert.alpha = 1
        }
    }
    
    @IBAction func okUnderstandAction(_ sender: Any) {
        wrapperAlert.alpha = 0
    }
    
    func getFilteredCity(selectedProvince: String){
        Service.getCity(name: selectedProvince, listener: self)
    }
    
    func failureResponse(call: RequestCall, response: DataResponse<Any>) {
        self.dismiss(animated: true, completion: nil)
        
        if (call == .profile) {
            self.showToast(message: "Gagal mendapatkan profil.")
        }
        
        if (call == .getProvince) {
            self.showToast(message: "Gagal mendapatkan data provinsi.")
        }
        
        if (call == .getCity) {
            self.showToast(message: "Gagal mendapatkan data kota.")
        }
        
        if (call == .updateProfile) {
            self.showToast(message: "Gagal merubah data.")
        }
        
        if (call == .logout) {
            self.showToast(message: "Gagal logout.")
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UITextField {

    func setUnderLine() {
        let border = CALayer()
        let width = CGFloat(0.5)
        border.borderColor = UIColor.darkGray.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width - 10, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }

}
