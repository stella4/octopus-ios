//
//  HomeViewController.swift
//  octopus-ios
//
//  Created by Maven on 22/04/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Kingfisher
import SkeletonView
import Sentry

protocol homeviewcontrollerdelegate {
    func refreshViewChat()
}

protocol homeNotifdelegate {
    func showWrapper()
}

class HomeViewController: UIViewController, RestApiListener, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    static let sharedInstance = HomeViewController()
    
    @IBOutlet weak var callScavangerView: UIView!
    @IBOutlet weak var labelPoints: UILabel!
    @IBOutlet weak var newsCollectionView: UICollectionView!
    @IBOutlet weak var voucherCollectionView: UICollectionView!
    @IBOutlet weak var heighConstantVoucherCV: NSLayoutConstraint!
    @IBOutlet weak var callScavangerButton: UIButton!
    
    @IBOutlet weak var wrapperAlert: UIView!
    
    var delegate : homeviewcontrollerdelegate!
    var delegateNotif : homeNotifdelegate!
    
    var token = UserDefaults.standard.string(forKey: "token")
    let dtbmSB: UIStoryboard = UIStoryboard(name: "Dtbm", bundle: nil)
    let mainSB: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    let authorizationSB: UIStoryboard = UIStoryboard(name: "Authorization", bundle: nil)
    let scavangerSB: UIStoryboard = UIStoryboard(name: "Scavanger", bundle: nil)
    var arrHomeTiles : [HomeTilesModel] = []
    var arrNews : [NewsModel] = []
    var arrVouchers : [VoucherModel] = []
    var yourDTBM : String = ""
    var myDtbmInt = 0
    var callScavActive = false
    private var timer = Timer()
    
    var previousSelected : IndexPath?
    var currentSelected : Int?
    
    var userLocation = ""
    
    var open = true
    var openOrder = true

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        HomeViewController.sharedInstance.callScavActive = false
        open = true
        openOrder = true
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleNotification(notification:)), name: NSNotification.Name("PUSHACTION"), object: nil)
        
        labelPoints.isSkeletonable = true
        labelPoints.showAnimatedGradientSkeleton()
        
        Service.getProfile(token: token!, listener: self)
        Service.chackAccountType(token: token!, listener: self)
        Service.getNews(token: token!, listener: self)
        
        callScavangerButton.layer.cornerRadius = 4
        
        callScavangerView.layer.masksToBounds = false
        callScavangerView?.layer.shadowColor = UIColor.darkGray.cgColor
        callScavangerView?.layer.shadowOffset = CGSize.zero
        callScavangerView?.layer.shadowOpacity = 0.4
        callScavangerView?.layer.shadowRadius = 4
        callScavangerView.layer.cornerRadius = 8
    }
    
    @IBAction func okMengertiAction(_ sender: Any) {
        wrapperAlert.alpha = 0
    }
    
    @objc func handleNotification(notification:NSNotification) {
        let userinfo = notification.userInfo as! [String : AnyObject]
        print("user info home - ",userinfo)
        
        let dataType : String = userinfo["type"] as? String ?? ""
        
        if let name = userinfo["name"] {
            if openOrder {
                openOrder = false
                if name as! String == "find_scav" || name as! String == "scav_found" || name as! String == "order_finish" || name as! String == "order_canceled" || name as! String == "scav_arrived" || name as! String == "picking_up"  {
                    let tittle = userinfo["title"] as! String
                    let body = userinfo["message"] as! String
                    
                    let scavangerSB: UIStoryboard = UIStoryboard(name: "Scavanger", bundle: nil)
                    let orderVC = scavangerSB.instantiateViewController(withIdentifier: "orderVC") as! OrderViewController
                    orderVC.notif = name as! String
                    orderVC.titleAlert = tittle
                    orderVC.bodyAlert = body
                    
                    if let wd = UIApplication.shared.delegate?.window {
                        var vc = wd!.rootViewController
                        if(vc is UINavigationController){
                            vc = (vc as! UINavigationController).visibleViewController
                        }

                        if(vc is OrderViewController) {
                            //your code
                            print("hereOrder")
                        } else {
                            print("hereCallScav")
//                            if !HomeViewController.sharedInstance.callScavActive {
                                if name as! String == "find_scav" {
//                                    self.delegateNotif.showWrapper()
                                }
//                            }
                        }
                    }
                }
            }
        }
        
        if dataType == "chat" {
            if open {
                open = false
                let scavangerSB: UIStoryboard = UIStoryboard(name: "Scavanger", bundle: nil)
                let chatVC = scavangerSB.instantiateViewController(withIdentifier: "chatVC") as! ChatViewController
                if let notificationData = userinfo["data"] as? NSString {
                    var dictionary : NSDictionary?
                    if let data = notificationData.data(using: String.Encoding.utf8.rawValue) {
                        do {
                            dictionary = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary
                            if let dict = dictionary {
                                chatVC.order_id = dict["order_id"] as! String
                                
                                if let wd = UIApplication.shared.delegate?.window {
                                    var vc = wd!.rootViewController
                                    if(vc is UINavigationController){
                                        vc = (vc as! UINavigationController).visibleViewController
                                    }

                                    if(vc is ChatViewController){
                                        //your code
//                                        print("chat active")
                                    } else {
                                        self.navigationController?.pushViewController(chatVC, animated: true)
                                    }
                                }
                            }
                        } catch let error as NSError {
                            print("Error: \(error)")
                        }
                    }
                }
            }
        } else if dataType == "notification" {
            
        }
    }
    
    @IBAction func goToProfile(_ sender: Any) {
        let profileVC = mainSB.instantiateViewController(withIdentifier: "profileVC") as! ProfileViewController

        self.navigationController?.pushViewController(profileVC, animated: true)
    }
    
    @IBAction func callScavangerAction(_ sender: Any) {
        print("here1")
        let pickAndPackVC = scavangerSB.instantiateViewController(withIdentifier: "pickAndPackVC") as! PickAndPackViewController

        self.navigationController?.pushViewController(pickAndPackVC, animated: true)
    }
    
    @IBAction func touchDownButtonAction(_ sender: Any) {
//        callScavangerButton.tintColor =
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == newsCollectionView {
            return arrNews.count
        }
        return arrVouchers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == newsCollectionView {
            let cellIdentifier: String = "cell"
            let cell = self.newsCollectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath)

            let banner : UIImageView = cell.viewWithTag(1) as! UIImageView
            let labelTitle : UILabel = cell.viewWithTag(2) as! UILabel
            let labelContent : UILabel = cell.viewWithTag(3) as! UILabel
            
            let bannerModel : NewsModel = arrNews[indexPath.row]
            
            banner.clipsToBounds = true
            banner.contentMode = .scaleAspectFill
            banner.kf.indicatorType = .activity
            banner.kf.setImage(with: URL(string: bannerModel.news_image))
            
            labelTitle.text = bannerModel.news_name
            labelContent.text = bannerModel.news_description

            return cell
        }
        
        let cellIdentifier: String = "cell"
        let cell = self.voucherCollectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath)
        
        let imageView : UIImageView = cell.viewWithTag(2) as! UIImageView
        let labelTitle : UILabel = cell.viewWithTag(3) as! UILabel
        let labelDetail : UILabel = cell.viewWithTag(4) as! UILabel
        let labelAmount : UILabel = cell.viewWithTag(5) as! UILabel
        let imageLogo : UIImageView = cell.viewWithTag(6) as! UIImageView
        let labelRibbon : UILabel = cell.viewWithTag(7) as! UILabel
        let ribbon : UIView = cell.viewWithTag(9)!
        
        let bannerModel : VoucherModel = arrVouchers[indexPath.row]
        
        ribbon.backgroundColor = UIColor.init(hexString: bannerModel.ribbon_color)
        labelTitle.text = bannerModel.name
        labelDetail.text = bannerModel.merchant_name
        labelAmount.text = bannerModel.dtbm_cost
        labelRibbon.text = bannerModel.ribbon_text
        
        imageView.kf.indicatorType = .activity
        imageView.kf.setImage(with: URL(string: bannerModel.cover_image))

        imageLogo.kf.indicatorType = .activity
        imageLogo.kf.setImage(with: URL(string: bannerModel.merchant_logo))

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        if collectionView == voucherCollectionView {
            let cell = collectionView.cellForItem(at: indexPath)
            cell?.backgroundColor = UIColor.init(hexString: "F5F5F5")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        if collectionView == voucherCollectionView {
            let cell = collectionView.cellForItem(at: indexPath)
            cell?.backgroundColor = UIColor.white
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == voucherCollectionView {
            
            let detailVoucherVC = dtbmSB.instantiateViewController(withIdentifier: "detailVoucherVC") as! DetailVoucherViewController
            detailVoucherVC.idVoucher = arrVouchers[indexPath.row].id
            detailVoucherVC.myDTBM = yourDTBM
            detailVoucherVC.myDtbmInt = myDtbmInt
            self.navigationController?.pushViewController(detailVoucherVC, animated: true)
        }
        if collectionView == newsCollectionView {
            if arrNews[indexPath.row].news_url != "" {
                if let url = URL(string: arrNews[indexPath.row].news_url) {
                    UIApplication.shared.open(url)
                }
            } else {
                let termsVC = authorizationSB.instantiateViewController(withIdentifier: "termsVC") as! TermsConditionViewController
                self.navigationController?.pushViewController(termsVC, animated: true)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == newsCollectionView {
            let cellWidth = newsCollectionView.frame.size.width
            let cellHeight = newsCollectionView.frame.size.height
            let size : CGSize = CGSize(width: cellWidth, height: cellHeight)
            
            return size;
        }
        let screenRect : CGRect = UIScreen.main.bounds
        let screenWidth : CGFloat = screenRect.size.width
        let cellWidth : Double = Double(screenWidth * 0.42)
        let cellHeight : Double = Double(cellWidth * 1.27)
        
        let size : CGSize = CGSize(width: cellWidth, height: cellHeight)

        return size;
    }
    
    func setuptimer() {
        timer = Timer.scheduledTimer(timeInterval: 3, target: self , selector: #selector(startScrolling), userInfo: nil, repeats: true)
    }

    @objc func startScrolling() {

        DispatchQueue.main.async {
            let firstIndex = 0
            let lastIndex = self.arrNews.count - 1
            let visibleCellsIndexes = self.newsCollectionView.indexPathsForVisibleItems.sorted()
            
            if !visibleCellsIndexes.isEmpty {
                let nextIndex = visibleCellsIndexes[0].row + 1
                let nextIndexPath: IndexPath = IndexPath.init(item: nextIndex, section: 0)
                let firstIndexPath: IndexPath = IndexPath.init(item: firstIndex, section: 0)
                
                (nextIndex > lastIndex) ? (self.newsCollectionView.scrollToItem(at: firstIndexPath, at: .centeredHorizontally, animated: true)) : (self.newsCollectionView.scrollToItem(at: nextIndexPath, at: .centeredHorizontally, animated: true))
            }
        }
    }
    
    func successResponse(call: RequestCall, response: DataResponse<Any>) {
        
        if (call == .profile) {
            let res = response.result.value
            if (res != nil) {
                let json = JSON(res!)
                let points = json["wallet_user"]["balance"].intValue
                self.userLocation = json["user_address"]["city"].stringValue
                
                print("user location - ",self.userLocation)
                if self.userLocation != "Kota Makassar" {
                    wrapperAlert.alpha = 1
                    callScavangerButton.isUserInteractionEnabled = false
                    callScavangerButton.backgroundColor = UIColor.gray
                } else {
                    wrapperAlert.alpha = 0
                    callScavangerButton.isUserInteractionEnabled = true
                    callScavangerButton.backgroundColor = UIColor.init(hexString: "3889CC")
                }
                
                labelPoints.hideSkeleton()
                labelPoints.text = points.withCommas()
                yourDTBM = points.withCommas()
                myDtbmInt = points
            }
        }
        
        if (call == .accountType) {
            print("account type success")
            let res = response.result.value
            if (res != nil) {
                let json = JSON(res!)
                let type = json["data"]["account_type"].stringValue
            }
        }
        
        if (call == .news) {
            let res = response.result.value
            if (res != nil) {
                let json = JSON(res!)
                let arrayJSON = json["data"].arrayValue
                var resultNews: NewsModel
                var resultVoucher: VoucherModel
                arrHomeTiles = []
                arrVouchers = []
                arrNews = []
                for (index, _) in arrayJSON.enumerated() {
                    resultNews = NewsModel()
                    resultVoucher = VoucherModel()
                    let isCarousel = arrayJSON[index]["is_carousel"].intValue
                    
                    if isCarousel == 1 {
                        resultNews.news_name = arrayJSON[index]["name"].stringValue
                        resultNews.news_description = arrayJSON[index]["description"].stringValue
                        
//                        if arrayJSON[index]["article"].stringValue != "" {
                            resultNews.news_image = arrayJSON[index]["article"]["image"].stringValue
                            resultNews.news_url = arrayJSON[index]["article"]["url"].stringValue
                        resultNews.news_text = arrayJSON[index]["article"]["text"].stringValue
//                        } else {
//                            resultNews.news_image = arrayJSON[index]["voucher"]["cover_image"].stringValue
//                            resultNews.id = arrayJSON[index]["voucher"]["id"].intValue
//                        }
                        arrNews.append(resultNews)
                    } else {
                        resultVoucher.name = arrayJSON[index]["name"].stringValue
                        resultVoucher.merchant_name = arrayJSON[index]["voucher"]["merchant"]["name"].stringValue
                        resultVoucher.id = arrayJSON[index]["voucher"]["id"].intValue
                        resultVoucher.dtbm_cost = arrayJSON[index]["voucher"]["dtbm_cost"].stringValue
                        resultVoucher.merchant_logo = arrayJSON[index]["voucher"]["merchant"]["logo"].stringValue
                        resultVoucher.ribbon_text = arrayJSON[index]["voucher"]["ribbon_text"].stringValue
                        resultVoucher.ribbon_color = arrayJSON[index]["voucher"]["ribbon_color"].stringValue
                        resultVoucher.cover_image = arrayJSON[index]["voucher"]["cover_image"].stringValue
                        
                        
                        arrVouchers.append(resultVoucher)
                    }
                    
                }
               
                DispatchQueue.main.async {
                    if(self.arrNews.count == 0){
                        self.newsCollectionView.isHidden = true
                    }else{
                        self.newsCollectionView.isHidden = false
                    }
                    
                    if(self.arrVouchers.count == 0){
                        self.voucherCollectionView.isHidden = true
                    }else{
                        self.voucherCollectionView.isHidden = false
                    }
                    
                    self.voucherCollectionView.reloadData()
                    let height = self.voucherCollectionView.collectionViewLayout.collectionViewContentSize.height
                    self.heighConstantVoucherCV.constant = height
                    
                    self.setuptimer()
                    self.newsCollectionView.reloadData()
                }
            }
        }
    }
    
    func failureResponse(call: RequestCall, response: DataResponse<Any>) {
        print("failureResponse")
        
    }

    @IBAction func goToOrderAction(_ sender: Any) {
        let orderVC = scavangerSB.instantiateViewController(withIdentifier: "orderVC") as! OrderViewController
        self.navigationController?.pushViewController(orderVC, animated: true)
    }
    
    @IBAction func goToDtbm(_ sender: Any) {
        let yourDtbmVC = dtbmSB.instantiateViewController(withIdentifier: "yourDtbmVC") as! YourDtbmViewController
        yourDtbmVC.yourDTBM = yourDTBM
        yourDtbmVC.myDtbmInt = myDtbmInt
        self.navigationController?.pushViewController(yourDtbmVC, animated: true)
    }
    
    @IBAction func yourDtbmAction(_ sender: Any) {
        let yourDtbmVC = dtbmSB.instantiateViewController(withIdentifier: "yourDtbmVC") as! YourDtbmViewController
        yourDtbmVC.yourDTBM = yourDTBM
        yourDtbmVC.myDtbmInt = myDtbmInt
        yourDtbmVC.unwind = 0
        self.navigationController?.pushViewController(yourDtbmVC, animated: true)
    }
    
    @IBAction func unwindToHome(segue: UIStoryboardSegue) {
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension Int {
    func withCommas() -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        return numberFormatter.string(from: NSNumber(value:self))!
    }
}
