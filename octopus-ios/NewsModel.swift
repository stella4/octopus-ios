//
//  NewsModel.swift
//  octopus-ios
//
//  Created by Maven on 23/04/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import Foundation

class NewsModel: Decodable {
    var id = 0
    var news_name = ""
    var news_description = ""
    var news_image = ""
    var news_url = ""
    var news_text = ""
}
