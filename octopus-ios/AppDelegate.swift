//
//  AppDelegate.swift
//  octopus-ios
//
//  Created by Maven on 16/04/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import UserNotifications
import Firebase
import FirebaseMessaging
import FirebaseInstanceID
import GoogleMaps
import GooglePlaces
import Sentry

var sessionManager = SessionManager()

@available(iOS 13.0, *)
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {
    
    var window: UIWindow?
    let mainSB: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    var token = UserDefaults.standard.string(forKey: "token")

    func successResponse(call: RequestCall, response: DataResponse<Any>) {
    }
    
    func failureResponse(call: RequestCall, response: DataResponse<Any>) {
        
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
//         Override point for customization after application launch.
        
        GMSServices.provideAPIKey("AIzaSyDokHAhCeDEr4dlYXlwdWb4EPhrIzY7bJs")
        GMSPlacesClient.provideAPIKey("AIzaSyDokHAhCeDEr4dlYXlwdWb4EPhrIzY7bJs")
        
        // Added in 5.1.6
        SentrySDK.start { options in
            options.dsn = "https://77b135431e614b44960555a893476e78@o327274.ingest.sentry.io/1837148"
            options.debug = true // Enabled debug when first installing is always helpful
        }

//        SentrySDK.capture(message: "My first test message")
        
        let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : "Object does not exist"])
        SentrySDK.capture(error: error) { (scope) in
            // Changes in here will only be captured for this event
            // The scope in this callback is a clone of the current scope
            // It contains all data but mutations only influence the event being sent
            scope.setTag(value: "value", key: "myTag")
        }
        
        SentrySDK.start(options: [
            //...
            "integrations": Sentry.Options.defaultIntegrations().filter { (name) -> Bool in
                return name != "SentryAutoBreadcrumbTrackingIntegration" // This will disable  SentryAutoBreadcrumbTrackingIntegration
            }
            //...
        ])
        
        guard UserDefaults.standard.string(forKey: "token") != nil else {
            let launchNav = mainSB.instantiateViewController(withIdentifier: "launchNav") as! UINavigationController
            window!.rootViewController = launchNav
            
            FirebaseApp.configure()
            
            Messaging.messaging().delegate = self
            
            if #available(iOS 10.0, *) {
              // For iOS 10 display notification (sent via APNS)
              UNUserNotificationCenter.current().delegate = self

              let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
              UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            } else {
              let settings: UIUserNotificationSettings =
              UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
              application.registerUserNotificationSettings(settings)
            }

            application.registerForRemoteNotifications()

            InstanceID.instanceID().instanceID { (result, error) in
                if let error = error {
                    print("Error fetching remote instance ID: \(error)")
                } else if let result = result {
                    print("Remote instance ID token: \(result.token)")
                }
            }
            
            return true
        }
        
        let homeTab = mainSB.instantiateViewController(withIdentifier: "homeNav") as! UINavigationController
        window!.rootViewController = homeTab
        
        FirebaseApp.configure()
        
        Messaging.messaging().delegate = self
        
        if #available(iOS 10.0, *) {
          // For iOS 10 display notification (sent via APNS)
          UNUserNotificationCenter.current().delegate = self

          let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
          UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {_, _ in })
        } else {
          let settings: UIUserNotificationSettings =
          UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
          application.registerUserNotificationSettings(settings)
        }

        application.registerForRemoteNotifications()

        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instance ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
            }
        }
        
        
        
        return true
    }

    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        UserDefaults.standard.set(fcmToken, forKey: "dtoken")

        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error)");
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        completionHandler([.alert, .badge, .sound])
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        // Print full message.
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {

        // Print message ID.
        let dataAPS : [String:AnyObject] = (userInfo as! [String:AnyObject])["aps"] as! [String : AnyObject]
//        print("Message ID 2: \(userInfo)")
        let type : String = userInfo["type"] as! String
//        print("type notif - ",type)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PUSHACTION"), object: nil, userInfo: userInfo)
        
        if type == "chat" {
            
        }
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userinfo = response.notification.request.content.userInfo
        let type : String = userinfo["type"] as! String
        
        if let name = userinfo["name"] {
            if name as! String == "find_scav" || name as! String == "scav_found" || name as! String == "order_finish" || name as! String == "order_canceled" || name as! String == "scav_arrived" || name as! String == "picking_up"  {
                let tittle = userinfo["title"] as! String
                let body = userinfo["message"] as! String
                let homeTab = mainSB.instantiateViewController(withIdentifier: "homeNav") as! UINavigationController
                window!.rootViewController = homeTab
                
                let scavangerSB: UIStoryboard = UIStoryboard(name: "Scavanger", bundle: nil)
                let orderVC = scavangerSB.instantiateViewController(withIdentifier: "orderVC") as! OrderViewController
                orderVC.notif = name as! String
                orderVC.titleAlert = tittle
                orderVC.bodyAlert = body
                
                homeTab.pushViewController(orderVC, animated: true)
            }
        }
        
        if type == "chat" {
            let homeTab = mainSB.instantiateViewController(withIdentifier: "homeNav") as! UINavigationController
            window!.rootViewController = homeTab
            
            let scavangerSB: UIStoryboard = UIStoryboard(name: "Scavanger", bundle: nil)
            let chatVC = scavangerSB.instantiateViewController(withIdentifier: "chatVC") as! ChatViewController
            if let notificationData = userinfo["data"] as? NSString {
                var dictionary : NSDictionary?
                if let data = notificationData.data(using: String.Encoding.utf8.rawValue) {
                    do {
                        dictionary = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary
                        if let dict = dictionary {
                            chatVC.order_id = dict["order_id"] as! String
                            
                            homeTab.pushViewController(chatVC, animated: true)
                        }
                    } catch let error as NSError {
                        print("Error: \(error)")
                    }
                }
            }
        } else if type  == "notification" {
                
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        var token = ""
        for i in 0..<deviceToken.count {
            token = token + String(format: "%02.2hhx", arguments: [deviceToken[i]])
        }
        print("device token - ",token)
        
        Messaging.messaging().apnsToken = deviceToken as Data
    }

    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

}

