//
//  HomeTilesModel.swift
//  octopus-ios
//
//  Created by Maven on 01/05/20.
//  Copyright © 2020 Maven. All rights reserved.
//

import Foundation

class HomeTilesModel: Decodable {
    var article : NewsModel = NewsModel()
    var voucher : VoucherModel = VoucherModel()
    var description = ""
    var id = 0
    var image = ""
    var is_carousel = ""
    var name = ""
}
